# HOTCHPOTCH

this is a gathering of demos, tests and prototypes made at [polymorph.cool](http://polymorph.cool) using [Godot Engine](http://godotengine.org)

the interface allows to load "screens", meaning godot scenes with their own gameplay and configuration panels

this project is opened to collaboration: fork the repository, add your screen and make a pull request, we will certainly pack it in the next release!

see video logs here: https://peertube.mastodon.host/video-channels/hotchpotch/videos

## repository structure

- **design**: contain svg and graphical assets
- **godot**: godot project
 
## how to add a screen

- make a new folder in **godot/** containing all the scenes, objects, materials, scripts, sound and textures used in the screen
- open **godot/scripts/autoload/gobals.gd**

### credits

add credits info of the screen in **credtits** array; they will be added in the main panel credit

structure:


```python
{
	'type': '3d model', #[3d model, font, texture, tool, graphic, sound, etc.]
	'text': 'Fish Perch by holmen', # free text displayed in panel, make it short
	'url': 'https://www.blendswap.com/blend/8888', # where to find the asset online
	'license': 'CC 0' # license, quite clear
}
```

### screen

add a dscription of the screen in **screen_list** array; it will appear in main screens panel 

structure:

```python
{
	'ID': -1, # will be set by script, no need to set this up
	'name': 'ultra slow', # text displayed in the button in screens panel
	'scene': 'heightmap/ultra_slow.tscn', # relative path to the scene, res:// will be appended if not present
	'thumb': 'heightmap/ultra_slow.jpg', # relative path to a screenshot
	'type': 'heightmap', # describes the main technical component of the screen, can be a list comma separated
	'date': '2020.04.03', # date of the first release
	'authors': 'frankiezafe', # name of the authors, list comma spearated
	'interactive': false, # is there an interaction via keyboard, mouse or joypad?
	'description': 'a landscape that leaves trails in the sky while wandering above it', # free text describing the scene
	'configuration': {} # see below
}

```

### configuration

procedure to expose parameters and shader from your scene

#### in your scene

on the root node of your scene, add getters and setters to interact with your parameters (one getter/setter per parameter)

the suggested way is to create function that are both setter and getter; for instance

```python

func my_param( value = null ):
	if value == null:
		return my_param_value
	my_param_value = value
	# call wathever other method here
```

#### in globals.gd

configuration field in screens is a dictionary

configuration's keys are used as label in the menu

for each entry, you have to specify a **type**; see **HP_TYPE** enum at the top of globals.gd

HP_TYPEs:

- *_undefined*: will be ignored
- *_bool*: not yet implemented
- *_int* & *_real*: displayed as a slider and a text input
- *_color*: not yet implemented
- *_shader*: loads an text editor to edit te shader
- *_texture*: not yet implemented
- *_group*: generate a menu

mandatory keys for each type:

##### _int & _real

- type: HP_TYPE._int / HP_TYPE._real
- default: int / float; will be used to reset value
- range: [float, float]; min and max value of the slider
- range_auto: bool; indicates if range is updated if input value is off bound, if not, value will be constrained to initial range
- getter: str; function to call to get current value
- setter: str; function to call to set new value

##### _shader

- type: HP_TYPE._shader
- getter: str; function to call to get shader
- setter: str; function to call to set new shader

warning: there is no pre-compilation test when updating the shader

##### _group

- type: HP_TYPE._group
- data: a dictionary containing valid parameters

#### example

in scene.tscn, root node:

```python
export(float, -2, 2) var _speed setget speed

func speed( f = null ):
	if f == null:
		return _speed
	_speed = f
	$plane.speed = f
	$plane2.speed = f
```

in globals.gd

```python
'name': 'speedster',
'scene': 'myscene/scene.tscn',
'configuration': {
	'speed': { 
		'type': HP_TYPE._real, 
		'default': -0.15, 
		'range': [-2,2],
		'range_auto': true,
		'getter': 'speed',
		'setter': 'speed' }
}
```

when the scene is loaded, globals.gd will link the function **speed** with the slider by calling **connect_screen()**

```python
# set: new key in the configuration parameter
# glob.screen_active_scene > instance of myscene/scene.tscn
param.set = funcref( glob.screen_active_scene, param.setter )
``` 
