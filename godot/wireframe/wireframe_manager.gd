tool

extends Spatial

onready var glob = get_node("/root/glob")
onready var floor_y = $scenery/floor.translation.y

enum WF_STATUS {
	_idle,
	_collecting_faces,
	_merging_faces,
	_merging_edges,
	_meshing,
	_done
} 

export (bool) var _clear = false setget clear
export (bool) var _analyse = false setget analyse
export (bool) var _generate = false setget generate
export (bool) var _optimise = true setget optimise
export (bool) var _auto_optimise = true setget auto_optimise
export (float,1e-5,1) var _merge_tolerance = 1e-5 setget merge_tolerance
export (float,0,1) var _drop = 0 setget drop
export (float,-1,1) var _overflow = 0 setget overflow
export (float,0,1) var _gap = 0 setget gap
export (float,0,0.3) var _grow = 0 setget grow
export (float,0,0.3) var _stroke = 0 setget stroke
export (Vector3) var _stroke_multiply = Vector3(1,1,1) setget stroke_multiply
export (bool) var _solidify = true setget solidify
export (bool) var _caps = true setget caps

func show_floor( b = null ):
	if b == null:
		return $scenery/floor.visible
	$scenery/floor.visible = b
	update_spot()

func show_original( b = null ):
	if b == null:
		return $meshes/origin.visible
	$meshes/origin.visible = b
	update_spot()

func get_commands():
	return [ 'generate', 'analyse', 'clear' ]

func load_command( cmd ):
	match(cmd):
		'generate':
			generate()
		'analyse':
			analyse()
		'clear':
			clear()

func get_mesh_list():
	var l = []
	for c in $geometries.get_children():
		l.append( c.name )
	return l

func load_mesh_name( n ):
	if n == null:
		return
	for c in $geometries.get_children():
		if c.name == n:
			$meshes/origin.transform = c.transform
			$meshes/origin.mesh = c.mesh
			generate()
			return

func optimise( b = null ):
	if b == null:
		return _optimise
	_optimise = b
	
func auto_optimise( b = null ):
	if b == null:
		return _auto_optimise
	_auto_optimise = b

func merge_tolerance( f = null ):
	if f == null:
		return _merge_tolerance
	_merge_tolerance = f

func drop( f = null ):
	if f == null:
		return _drop
	_drop = f

func overflow( f = null ):
	if f == null:
		return _overflow
	_overflow = f

func gap( f = null ):
	if f == null:
		return _gap
	_gap = f

func grow( f = null ):
	if f == null:
		return _grow
	_grow = f
	$scenery/floor.translation.y = floor_y - _grow

func stroke( f = null ):
	if f == null:
		return _stroke
	_stroke = f

func stroke_multiply( v3 = null ):
	if v3 == null:
		return _stroke_multiply
	_stroke_multiply = v3

func solidify( b = null ):
	if b == null:
		return _solidify
	_solidify = b

func caps( b = null ):
	if b == null:
		return _caps
	_caps = b

var process_status = WF_STATUS._idle
var last_process_status = WF_STATUS._idle
var mutex = null
var thread = null
var edge_count = 0
var mouse_enabled = false

onready var analyse_notif = glob.new_notification()
onready var debug_notif = glob.new_notification()
onready var spot_position = $scenery/sun.translation

func face( v0, v1, v2 ):
	var norm = (((v2-v0).normalized()).cross((v1-v0).normalized())).normalized()
	return {
		'verts': [ v0, v1, v2 ],
		'edges': [ [v0,v1,norm], [v1,v2,norm], [v2,v0,norm] ],
		'center': (v0+v1+v2) / 3,
		'normal': norm,
		'merged': false
	}

func compare_edges( e0, e1 ):
	return ( e0[0] == e1[0] and e0[1] == e1[1] ) or ( e0[0] == e1[1] and e0[1] == e1[0] )

func merge_face( src, dst ):
	var unique_edges = []
	for se in src.edges:
		var rm = null
		for i in range( 0, len(dst.edges) ):
			var de = dst.edges[i]
			if compare_edges( se, de ):
				rm = i
				break
		if rm != null:
			dst.edges.remove( rm )
		else:
			unique_edges.append( se )
	for e in dst.edges:
		unique_edges.append( e )
	dst.edges = unique_edges
	dst.center = Vector3(0,0,0)
	for e in dst.edges:
		dst.center += e[0] + e[1]
	dst.center /= len( dst.edges ) * 2
	dst.normal = (dst.normal + src.normal).normalized()

func clear( b = true ):
	if b:
		$meshes/wire.mesh = null

func analyse( b = true ):
	if b:
		if mutex != null:
			mutex.lock()
		var txt = "source path: " + $meshes/origin.mesh.resource_path + ", "
		txt += "faces " + str( len( $meshes/origin.mesh.get_faces() ) ) + "\n"
		if $meshes/wire.mesh == null:
			txt += "no wireframe"
		else:
			if _stroke > 0:
				txt += "wireframe faces: " + str( len( $meshes/wire.mesh.get_faces() ) )
			else:
				txt += "wireframe edges: " + str( edge_count )
		analyse_notif.text = txt
		analyse_notif.ttl = glob.DEFAULT_NOTICATION_TTL
		glob.emit( "HP_notification", analyse_notif )
		if mutex != null:
			mutex.unlock()

func generate( b = true ):
	_generate = false
	if b:
		if thread != null:
			thread.wait_to_finish()
		if mutex == null:
			mutex = Mutex.new()
		mutex.lock()
		thread = Thread.new()
		thread.start(self, "_wireframe")
		mutex.unlock()
	
func update_debug():
	debug_notif.text = "status: "
	match ( process_status ):
		WF_STATUS._idle:
			debug_notif.text += "idle"
			debug_notif.ttl = glob.DEFAULT_NOTICATION_TTL
		WF_STATUS._collecting_faces:
			debug_notif.text += "collecting faces"
			debug_notif.ttl = null
		WF_STATUS._merging_faces:
			debug_notif.text += "merging faces"
			debug_notif.ttl = null
		WF_STATUS._merging_edges:
			debug_notif.text += "merging edges"
			debug_notif.ttl = null
		WF_STATUS._meshing:
			debug_notif.text += "meshing"
			debug_notif.ttl = null
		WF_STATUS._done:
			debug_notif.text += "done"
			debug_notif.ttl = glob.DEFAULT_NOTICATION_TTL
	glob.emit( "HP_notification", debug_notif )

func _ready():
	clear()
	# listening UI
	glob.connect( "HP_menu_open", self, "menu_open" )
	glob.connect( "HP_menu_close", self, "menu_close" )
	$cam_pivot.interaction_enabled = false
	$meshes.visible = true
	$geometries.visible = false

func menu_open():
	mouse_enabled = false
	$cam_pivot.interaction_enabled = mouse_enabled

func menu_close():
	mouse_enabled = true
	$cam_pivot.interaction_enabled = mouse_enabled
	$cam_pivot.start_drag()

# warning-ignore:unused_argument
func _process(delta):
	
	if thread != null:
		if last_process_status != process_status:
			update_debug()
			mutex.lock()
			last_process_status = process_status
			mutex.unlock()
		if last_process_status == WF_STATUS._done:
			thread.wait_to_finish()
			thread = null
			analyse()

func _exit_tree():
	# ensuring to remove the notification
	debug_notif.ttl = 0
	glob.emit( "HP_notification", debug_notif )
	# waiting for the thread to finish
	if thread != null:
		thread.wait_to_finish()

# warning-ignore:unused_argument
func _wireframe( userdata ):
	
	var src = $meshes/origin
	var dst = $meshes/wire
	var bfaces = []
	var unique_edges = []
	
	mutex.lock()
	var auto_opt = _auto_optimise
	var opt = _optimise
	var rand_drop = _drop
	var push = _grow
	var width = _stroke
	var wmult = _stroke_multiply
	var volume = _solidify
	var close__caps = _caps
	process_status = WF_STATUS._collecting_faces
	mutex.unlock()
	
	if rand_drop:
		randomize()
	
	# generate faces
	var fs = src.mesh.get_faces()
	# remove optimisation for large meshes
	if auto_opt and len( fs ) > 15000:
		opt = false
	
# warning-ignore:integer_division
	for i in range( 0, len( fs ) / 3 ):
		var f = face( fs[i*3], fs[i*3+1], fs[i*3+2] )
		if not opt:
			for e in f.edges:
				unique_edges.append( e )
		bfaces.append( f )
	
	if opt:
		
		mutex.lock()
		process_status = WF_STATUS._merging_faces
		mutex.unlock()
		
		# merge faces based on normal distance
		var faces = []
		var bfnum = len(bfaces)
		for i in range( 0, bfnum ):
			var bf = bfaces[i]
			if bf.merged:
				continue
			var found = false
			for f in faces:
				var dist = f.normal.dot(bf.normal)
				if dist > 1 - _merge_tolerance:
					merge_face( bf, f )
					bf.merged = true
					found = true
					break
			if not found:
				faces.append( bf.duplicate(true) )
		
		mutex.lock()
		process_status = WF_STATUS._merging_edges
		mutex.unlock()
		
		# removing all duplicated edges
		for f in faces:
			for e in f.edges:
				var found = null
				for ue in unique_edges:
					if compare_edges( e, ue ):
						found = ue
						break
				if found == null:
					unique_edges.append( e )
				else:
					# merging normals
					found[2] = (found[2] + e[2]).normalized()
	
	mutex.lock()
	process_status = WF_STATUS._meshing
	mutex.unlock()
	
	var _min = null
	var _max = null
	
	var all_verts = []
	for e in unique_edges:
		
		if rand_drop != 0 and randf() < rand_drop:
			continue
		
		# effects
		var dir = (e[1] - e[0])
		var length = dir.length()
		dir = dir.normalized()
		
		if push != 0:
			e[0] += e[2] * push
			e[1] += e[2] * push
		
		# _growing
		if _overflow != 0:
			if _overflow < 0 and length <= -_overflow:
				continue
			e[0] -= dir * _overflow * 0.5
			e[1] += dir * _overflow * 0.5
			length += _overflow
		
		# making a _gap between vertices
		if _gap > 0:
			if length <= _gap:
				continue
			var g = length - _gap
			all_verts.append( [dir, e[2], e[0]] )
			all_verts.append( [dir, e[2], e[0] + dir * g * 0.5] )
			all_verts.append( [dir, e[2], e[1] - dir * g * 0.5] )
			all_verts.append( [dir, e[2], e[1]] )
		else:
			all_verts.append( [dir, e[2], e[0]] )
			all_verts.append( [dir, e[2], e[1]] )
		
		if _min == null:
			_min = e[0]
			_max = e[0]
		# aabb limits
		for i in range( 0,2 ):
			if _min.x > e[i].x:
				_min.x = e[i].x
			if _min.y > e[i].y:
				_min.y = e[i].y
			if _min.z > e[i].z:
				_min.z = e[i].z
			if _max.x < e[i].x:
				_max.x = e[i].x
			if _max.y < e[i].y:
				_max.y = e[i].y
			if _max.z < e[i].z:
				_max.z = e[i].z
	
	
	var _mesh = Mesh.new()
	
	if len(all_verts) > 0: 
		
		var _surf = SurfaceTool.new()
		
		if width == 0:
			
			_surf.begin(Mesh.PRIMITIVE_LINES)
			for v in all_verts:
				_surf.add_normal(v[1])
				_surf.add_vertex(v[2])
			_surf.index()
			_surf.commit( _mesh )
			
		else:
			
			_surf.begin(Mesh.PRIMITIVE_TRIANGLES)
			
			width /= 2
			
			for i in range( 0, len( all_verts ), 2 ):
				
				if not volume:
					
					var v0 = all_verts[i]
					var left0 = (v0[0].cross( v0[1] )).normalized() * width * wmult.x
					var v1 = all_verts[i+1]
					var left1 = (v1[0].cross( v1[1] )).normalized() * width * wmult.x
					var pts = [ v0[2] + left0, v0[2] - left0, v1[2] - left1, v1[2] + left1 ]
					
					_surf.add_normal(v0[1])
					_surf.add_vertex(pts[0])
					_surf.add_normal(v0[1])
					_surf.add_vertex(pts[1])
					_surf.add_normal(v1[1])
					_surf.add_vertex(pts[2])
					
					_surf.add_normal(v1[1])
					_surf.add_vertex(pts[2])
					_surf.add_normal(v1[1])
					_surf.add_vertex(pts[3])
					_surf.add_normal(v0[1])
					_surf.add_vertex(pts[0])
				
				else:
					
					var v0 = all_verts[i]
					var v1 = all_verts[i+1]
					
					var normal = (v0[1]+v1[1]) * 0.5
					var normali = normal * -1
					var front = (v0[0]+v1[0]) * 0.5
					var fronti = front * -1
					var left = front.cross( normal )
					var lefti = left * -1
					
					var offsetx = left * width * wmult.x
					var offsety = front * (1 - wmult.y) 
					var offsetz = normal * width * wmult.z
					
					var vfront = [ 
						v0[2] + offsetx + offsety + offsetz, 
						v0[2] - offsetx + offsety + offsetz, 
						v1[2] - offsetx - offsety + offsetz, 
						v1[2] + offsetx - offsety + offsetz ]
					var vback = [ 
						v0[2] + offsetx + offsety - offsetz, 
						v0[2] - offsetx + offsety - offsetz, 
						v1[2] - offsetx - offsety - offsetz, 
						v1[2] + offsetx - offsety - offsetz ]
					
					# front
					_surf.add_normal(normal)
					_surf.add_vertex(vfront[0])
					_surf.add_normal(normal)
					_surf.add_vertex(vfront[1])
					_surf.add_normal(normal)
					_surf.add_vertex(vfront[2])

					_surf.add_normal(normal)
					_surf.add_vertex(vfront[2])
					_surf.add_normal(normal)
					_surf.add_vertex(vfront[3])
					_surf.add_normal(normal)
					_surf.add_vertex(vfront[0])

					# back
					_surf.add_normal(normali)
					_surf.add_vertex(vback[2])
					_surf.add_normal(normali)
					_surf.add_vertex(vback[1])
					_surf.add_normal(normali)
					_surf.add_vertex(vback[0])

					_surf.add_normal(normali)
					_surf.add_vertex(vback[0])
					_surf.add_normal(normali)
					_surf.add_vertex(vback[3])
					_surf.add_normal(normali)
					_surf.add_vertex(vback[2])
					
					# left
					_surf.add_normal(left)
					_surf.add_vertex(vback[0])
					_surf.add_normal(left)
					_surf.add_vertex(vfront[0])
					_surf.add_normal(left)
					_surf.add_vertex(vfront[3])

					_surf.add_normal(left)
					_surf.add_vertex(vfront[3])
					_surf.add_normal(left)
					_surf.add_vertex(vback[3])
					_surf.add_normal(left)
					_surf.add_vertex(vback[0])

					# right
					_surf.add_normal(lefti)
					_surf.add_vertex(vfront[1])
					_surf.add_normal(lefti)
					_surf.add_vertex(vback[1])
					_surf.add_normal(lefti)
					_surf.add_vertex(vback[2])

					_surf.add_normal(lefti)
					_surf.add_vertex(vback[2])
					_surf.add_normal(lefti)
					_surf.add_vertex(vfront[2])
					_surf.add_normal(lefti)
					_surf.add_vertex(vfront[1])
					
					if close__caps:
						
						# top
						_surf.add_normal(fronti)
						_surf.add_vertex(vfront[0])
						_surf.add_normal(fronti)
						_surf.add_vertex(vback[1])
						_surf.add_normal(fronti)
						_surf.add_vertex(vfront[1])
						
						_surf.add_normal(fronti)
						_surf.add_vertex(vback[1])
						_surf.add_normal(fronti)
						_surf.add_vertex(vfront[0])
						_surf.add_normal(fronti)
						_surf.add_vertex(vback[0])
						
						# bottom
						_surf.add_normal(front)
						_surf.add_vertex(vfront[2])
						_surf.add_normal(front)
						_surf.add_vertex(vback[2])
						_surf.add_normal(front)
						_surf.add_vertex(vback[3])
						
						_surf.add_normal(front)
						_surf.add_vertex(vback[3])
						_surf.add_normal(front)
						_surf.add_vertex(vfront[3])
						_surf.add_normal(front)
						_surf.add_vertex(vfront[2])
						
				
			_surf.index()
			_surf.commit( _mesh )
	
	var _aabb = AABB()
	if _min != null:
		_aabb = AABB( _min * 1.5, ( _max - _min ) * 1.5 )
	
	mutex.lock()
	dst.set_mesh( _mesh )
	dst.mesh.custom_aabb = _aabb
	dst.transform = src.transform
# warning-ignore:integer_division
	edge_count = len( unique_edges ) / 2
	process_status = WF_STATUS._done
	mutex.unlock()
	yield(get_tree(), "idle_frame")

func update_spot():
	var dist = spot_position.length()
	$scenery/sun.translation = spot_position.normalized() * rand_range( dist-0.01, dist+0.01 )
