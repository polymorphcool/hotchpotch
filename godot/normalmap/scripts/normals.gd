extends Node2D

enum CONTROL { 
	SIZE, STRENGTH, MULT, SIN, CONE, FADEOUT, EXPANDX, EXPANDY, ORIENTATION,
	METALLIC, ROUGHNESS }

export (int,1,1024) var render_size:int = 512

export (NodePath) var size_slider:NodePath = ""
export (NodePath) var size_display:NodePath = ""
export (NodePath) var strength_slider:NodePath = ""
export (NodePath) var strength_display:NodePath = ""
export (NodePath) var mult_slider:NodePath = ""
export (NodePath) var mult_display:NodePath = ""
export (NodePath) var sin_slider:NodePath = ""
export (NodePath) var sin_display:NodePath = ""
export (NodePath) var cone_slider:NodePath = ""
export (NodePath) var cone_display:NodePath = ""
export (NodePath) var fadeout_slider:NodePath = ""
export (NodePath) var fadeout_display:NodePath = ""
export (NodePath) var expandx_slider:NodePath = ""
export (NodePath) var expandx_display:NodePath = ""
export (NodePath) var expandy_slider:NodePath = ""
export (NodePath) var expandy_display:NodePath = ""
export (NodePath) var orientation_slider:NodePath = ""
export (NodePath) var orientation_display:NodePath = ""

export (NodePath) var metallic_slider:NodePath = ""
export (NodePath) var metallic_display:NodePath = ""
export (NodePath) var roughness_slider:NodePath = ""
export (NodePath) var roughness_display:NodePath = ""

onready var checkboard_tex:Texture = $root/pivot/rotor/plane.material_override.albedo_texture
var draw_enabled:bool = false
var controls = []

func register_control( usage:int, input:NodePath, display:NodePath ):
	var ctrl:Control = get_node(input)
	if ctrl != null:
		controls.append({
			'init': get_value(usage),
			'usage': usage,
			'control': ctrl,
			'display': get_node(display)
		})
		slider_changed( ctrl, get_value(usage) )
		if ctrl.has_signal( "slider_value" ):
			ctrl.connect( "slider_value", self, "slider_changed" )

func get_control( variant ):
	if variant is Control:
		for c in controls: 
			if c.control == variant:
				return c
	elif variant is int:
		for c in controls: 
			if c.usage == variant:
				return c
	return null

func push_value( usage:int, f:float ):
	match usage:
		CONTROL.SIZE:
			$render/mouse.border = f
			$preview/mouse.border = f
		CONTROL.STRENGTH:
			$render/mouse.material.set_shader_param('strength',f)
		CONTROL.MULT:
			$brush_vp/brush.material.set_shader_param('mult',f)
		CONTROL.SIN:
			$brush_vp/brush.material.set_shader_param('sin_force',f)
		CONTROL.CONE:
			$brush_vp/brush.material.set_shader_param('cone_force',f)
		CONTROL.FADEOUT:
			$brush_vp/brush.material.set_shader_param('fadeout',f)
		CONTROL.EXPANDX:
			$render/mouse.expand = Vector2(f,$render/mouse.expand.y)
			$preview/mouse.expand = $render/mouse.expand
		CONTROL.EXPANDY:
			$render/mouse.expand = Vector2($render/mouse.expand.x,f)
			$preview/mouse.expand = $render/mouse.expand
		CONTROL.ORIENTATION:
			$render/mouse.orientation = f
			$preview/mouse.orientation = $render/mouse.orientation
		CONTROL.METALLIC:
			$root/pivot/rotor/plane.material_override.metallic = f
		CONTROL.ROUGHNESS:
			$root/pivot/rotor/plane.material_override.roughness = f
		
func get_value( usage:int ):
	match usage:
		CONTROL.SIZE:
			return $render/mouse.border
		CONTROL.STRENGTH:
			return $render/mouse.material.get_shader_param('strength')
		CONTROL.MULT:
			return $brush_vp/brush.material.get_shader_param('mult')
		CONTROL.SIN:
			return $brush_vp/brush.material.get_shader_param('sin_force')
		CONTROL.CONE:
			return $brush_vp/brush.material.get_shader_param('cone_force')
		CONTROL.FADEOUT:
			return $brush_vp/brush.material.get_shader_param('fadeout')
		CONTROL.EXPANDX:
			return $render/mouse.expand.x
		CONTROL.EXPANDY:
			return $render/mouse.expand.y
		CONTROL.ORIENTATION:
			return $render/mouse.orientation
		CONTROL.METALLIC:
			return $root/pivot/rotor/plane.material_override.metallic
		CONTROL.ROUGHNESS:
			return $root/pivot/rotor/plane.material_override.roughness
	return 0

func slider_changed( ctrl:Control, f:float ):
	var c:Dictionary = get_control(ctrl)
	if c == null:
		return
	if c.control is Slider and c.control.value != f:
		c.control.value = f
	if c.display != null:
		c.display.text = str(int(f*1000)*0.001)
	push_value( c.usage, f )

func reset_values():
	for c in controls:
		if c.control is Slider:
			slider_changed( c.control, c.init ) 
		else:
			push_value( c.usage, c.init )

func clear():
	$render/clear.visible = true
	$render/mouse.visible = true
	$preview/mouse.visible = !$render/mouse.visible

func checkboard():
	if $root/pivot/rotor/plane.material_override.albedo_texture == null:
		$root/pivot/rotor/plane.material_override.albedo_texture = checkboard_tex
		$preview/ui/columns/zone5/checkboard.text = 'hide'
	else:
		$root/pivot/rotor/plane.material_override.albedo_texture = null
		$preview/ui/columns/zone5/checkboard.text = 'display'

func _ready():
	
	register_control( CONTROL.SIZE, 		size_slider, 			size_display )
	register_control( CONTROL.STRENGTH, 	strength_slider, 		strength_display )
	register_control( CONTROL.MULT, 		mult_slider, 			mult_display )
	register_control( CONTROL.SIN, 			sin_slider, 			sin_display )
	register_control( CONTROL.CONE, 		cone_slider, 			cone_display )
	register_control( CONTROL.FADEOUT, 		fadeout_slider, 		fadeout_display )
	register_control( CONTROL.EXPANDX, 		expandx_slider, 		expandx_display )
	register_control( CONTROL.EXPANDY, 		expandy_slider, 		expandy_display )
	register_control( CONTROL.ORIENTATION, 	orientation_slider, 	orientation_display )
	register_control( CONTROL.METALLIC, 	metallic_slider, 		metallic_display )
	register_control( CONTROL.ROUGHNESS, 	roughness_slider, 		roughness_display )
	$preview/ui/columns/zone4/reset.connect("pressed",self,"reset_values")
	$preview/ui/columns/zone4/clear.connect("pressed",self,"clear")
	$preview/ui/columns/zone5/checkboard.connect("pressed",self,"checkboard")
	checkboard()
	
	$render.size = Vector2.ONE * render_size
	$preview/draw_area.rect_size = $render.size
	$render/clear.scale = $render.size 
	$brush_vp.get_texture().flags = Texture.FLAG_FILTER
	$preview/mouse.visible = !$render/mouse.visible
	$preview/mouse.scale = $preview/texture.scale
	$preview/brush.scale = Vector2.ONE / $brush_vp.get_texture().get_size() * $preview/brush_bg.texture.get_size()
	
	$preview/draw_area.connect("mouse_entered",self,"draw_allow")
	$preview/draw_area.connect("mouse_exited",self,"draw_refuse")

func draw_allow():
	draw_enabled = true

func draw_refuse():
	draw_enabled = false

func draw_start():
	if draw_enabled:
		$render/mouse.visible = true
		$render/clear.visible = false
		$render.render_target_clear_mode = Viewport.CLEAR_MODE_NEVER

func _process(delta):
	$render/mouse.position = (get_viewport().get_mouse_position()-$preview/texture.position) / $preview/texture.scale
	$preview/mouse.position = get_viewport().get_mouse_position()

func _input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		if event.pressed:
			draw_start()
		else:
			$render/mouse.visible = false
		$preview/mouse.visible = !$render/mouse.visible
	if event is InputEventKey and event.pressed:
		if event.scancode == KEY_UP:
			pass
		elif event.scancode == KEY_DOWN:
			pass
		elif event.scancode == KEY_BACKSPACE:
			clear()
		elif event.scancode == KEY_ESCAPE:
			get_tree().quit()
