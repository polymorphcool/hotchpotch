tool

extends Node2D

export(int,2,50) var corners:int = 6
export(float,0,200) var radius:float = 50
export(float,1,30) var precision:float = 5

export(bool) var init_deviation:bool = false
export(float,0,1) var deviation:float = 0

export(bool) var debug:bool = false
export(bool) var generate:bool = false setget set_generate
export(bool) var clear:bool = false setget set_clear

export(float,-10,10) var origin_force:float = 1
export(float,-10,10) var inner_force:float = 1
export(float,-50,50) var random_force:float = 1
export(float,0,1) var damping:float = 0.5

var controls:Array = []
var curve:Curve2D = null
var display:Line2D = null

func set_clear(b:bool) -> void:
	if b:
		while get_child_count() > 0:
			remove_child( get_child(0) )
		controls = []
		curve = null
		display = null

func compute_anchors( i:int, delta:float = 0 ) -> void:
	if i == 0:
		curve.clear_points()
	var control:Dictionary = controls[i]
	var previous:Dictionary = controls[(i+(corners-1))%corners]
	var next:Dictionary = controls[(i+1)%corners]
	var prev_dir:Vector2 = (control.pos-previous.pos)
	var prev_dist:float = prev_dir.length()
	var curr_dir:Vector2 = (next.pos-control.pos)
	var curr_dist:float = prev_dir.length()
	var mix_dir = (prev_dir + curr_dir).normalized()
	if control.init.empty():
		for j in range(0,corners):
			if j != i:
				control.init.append( (controls[j].pos-control.pos).length() )
	else:
		var new_push:Vector2 = Vector2(0,0)
		var ci:int = 0
		for j in range(0,corners):
			if j != i:
				var p:Vector2 = controls[j].pos-control.pos
				var d:float = p.length()
				new_push += (p/d) * inner_force * (d-control.init[ci]) / (corners-1)
				ci += 1
		new_push += (control.init_pos-control.pos) * origin_force
		new_push += Vector2(rand_range(-1,1),rand_range(-1,1)).normalized() * random_force
		var pc:float = (1-damping) * delta
		control.push = control.push * (1-pc) + new_push * pc
	control.before = -(mix_dir*(prev_dist*0.33))
	control.after = (mix_dir*(curr_dist*0.33))
	curve.add_point( control.pos, control.before, control.after )
	if i == corners-1:
		curve.add_point( next.pos, next.before, next.after )
	if debug:
		# recompute scale
		var sc:float = max(prev_dist,curr_dist) * 0.5
		control.debug.scale = Vector2.ONE * sc
		control.debug.position = control.pos

func set_generate(b:bool) -> void:
	if b:
		
		while get_child_count() > 0:
			remove_child( get_child(0) )
		
		display = Line2D.new()
		display.width = 1
		display.default_color = Color(1,1,1)
		add_child(display)
		
		# creation of points
		var a:float = 0
		var agap:float = TAU / corners
		controls = []
# warning-ignore:unused_variable
		for i in range(0,corners):
			
			var initp:Vector2 = Vector2(cos(a),sin(a)) * radius
			var randp:Vector2 = initp * (1-rand_range(0,deviation))
			if init_deviation:
				initp = randp
			
			var c:Dictionary = { 
				# random push
				"push": Vector2(rand_range(-1,1),rand_range(-1,1)).normalized(),
				"init_pos": initp,
				"pos": randp,
				"init": [],
				"before": null,
				"after": null
			}
			
			if debug:
				c.debug = Line2D.new()
				c.debug.width = 0.01
				c.default_color = Color(1,1,1)
				for ci in range( 0, 17 ):
					var ca:float = ci / 16.0 * TAU
					c.debug.add_point( Vector2(cos(ca),sin(ca)) )
				add_child( c.debug )
			
			controls.append(c)
			a += agap
			
		# creation of anchors
		curve = Curve2D.new()
		curve.bake_interval = precision
		for i in range(0,corners):
			compute_anchors(i)
		display.points = curve.get_baked_points()
	
	if debug:
		var initl:Line2D = Line2D.new()
		initl.width = 0.5
		initl.default_color = Color(1,1,0)
		var startl:Line2D = Line2D.new()
		startl.width = 0.5
		startl.default_color = Color(1,0,1)
		for i in range(0,corners+1):
			var control:Dictionary = controls[ i % corners ]
			initl.add_point( control.init_pos )
			startl.add_point( control.pos )
		add_child( initl )
		add_child( startl )
		
func _process(delta) -> void:
	
	if controls.empty() or curve == null or display == null:
		return
	
	for control in controls:
		control.pos += control.push * delta
	
	for i in range(0,corners):
		compute_anchors(i,delta)
	
	display.points = curve.get_baked_points()
