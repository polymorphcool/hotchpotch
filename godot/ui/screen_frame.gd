extends Control

onready var glob = get_node("/root/glob")

export (int, -1, 500) var column0 = 200
export (int, -1, 500) var column1 = 250

onready var offset = Vector2( 0, $name.rect_size.y )

var current_id = null
var first_btn = null

var thumb_target_size = null

func to_top():
	if glob.screen_active_id != null:
		hover( glob.screen_active_id )

func image_fit( thumb, frame_size ):
	if not thumb is TextureRect:
		return
	var tsize = thumb.texture.get_size()
	var fratio = frame_size.y * 1.0 / frame_size.x
	var tratio = tsize.y * 1.0 / tsize.x
	var sc = 1
# warning-ignore:shadowed_variable
	var offset = Vector2(0,0)
	if tratio > fratio:
		sc = frame_size.x / tsize.x
		offset.y = ( frame_size.y - tsize.y * sc ) * 0.5
	else:
		sc = frame_size.y / tsize.y
		offset.x = ( frame_size.x - tsize.x * sc ) * 0.5
	thumb.rect_position = offset
	thumb.rect_size = tsize
	thumb.rect_scale = Vector2( sc, sc )

func set_dimension( v2 ):
	
	rect_position.y = -v2.y
	rect_size = v2
	$bg.rect_size = v2 - offset
	
	$name.rect_position.x = column0 - $name.rect_size.x
	$info.rect_position.x = column0
	$preview.rect_position.x = column0 + column1
	
	$frames.rect_position.y = $name.rect_size.y
	$frames.rect_size = $bg.rect_size
	
	$frames/btns.rect_min_size.x = column0
	$frames/desc.rect_min_size.x = column1
	var remainingx = rect_size.x - ( column0 + column1 )
	$frames/gallery.rect_size.x = remainingx
	$frames/gallery.rect_size.y = $bg.rect_size.y
	$frames/gallery.rect_min_size.y = $bg.rect_size.y
	
	image_fit( $frames/gallery/thumb, $frames/gallery.rect_size )
	thumb_target_size = $frames/gallery.rect_size

func load_screens():
	for l in glob.screen_list:
		if not l is Dictionary:
			return
		if not 'name' in l:
			return
		var btn = glob.tmpl_button.duplicate()
		$frames/btns/lyt.add_child( btn )
		btn.visible = true
		btn.text = l.name
		btn.connect( "hover", self, "update_hover" )
		btn.connect( "mouse_exited", self, "update_exited" )
		btn.connect( "pressed", self, "screen_select" )

func load_details():
	
	var lyt = $frames/desc/lyt
	if lyt.get_child_count() == 0:
# warning-ignore:unused_variable
		for i in range(0,7):
			var lbl = glob.tmpl_label.duplicate()
			lyt.add_child( lbl )
			lbl.visible = true
			lbl.autowrap = true
	var sc = glob.screen_list[current_id]
	if sc != null:
		var ci = 0
		lyt.get_child(ci).text = "interactive: "
		if sc.interactive:
			lyt.get_child(ci).text += 'yes'
		else:
			lyt.get_child(ci).text += 'no'
		ci += 1
		lyt.get_child(ci).text = "configurable: "
		if sc.configuration != null:
			lyt.get_child(ci).text += 'yes'
		else:
			lyt.get_child(ci).text += 'no'
		ci += 1
		lyt.get_child(ci).text = "type: " + sc.type
		ci += 1
		lyt.get_child(ci).text = "date: " + sc.date
		ci += 1
		lyt.get_child(ci).text = "authors: " + sc.authors
		ci += 1
		lyt.get_child(ci).text = "completion: "
		match (sc.level):
			glob.HP_LEVEL._undefined:
				lyt.get_child(ci).text += "not even draft"
			glob.HP_LEVEL._wip:
				lyt.get_child(ci).text += "wip"
			glob.HP_LEVEL._debug:
				lyt.get_child(ci).text += "fine-tuning"
			glob.HP_LEVEL._release:
				lyt.get_child(ci).text += "finalised"
		ci += 1
		lyt.get_child(ci).text = sc.description
		ci += 1
		$frames/gallery/thumb.texture = load( sc.thumb )
		image_fit( $frames/gallery/thumb, thumb_target_size )

func get_buttons():
	return $frames/btns/lyt.get_children()

func _ready():
	# cleanup placeholders
	while $frames/btns/lyt.get_child_count() > 0:
		$frames/btns/lyt.remove_child( $frames/btns/lyt.get_child(0) )
	while $frames/desc/lyt.get_child_count() > 0:
		$frames/desc/lyt.remove_child( $frames/desc/lyt.get_child(0) )

func screen_select():
	var lyt = $frames/btns/lyt
	var sel = lyt.get_child( current_id )
	for btn in lyt.get_children():
		if btn != sel:
			glob.button_unfocus( btn )
	glob.button_focus( lyt.get_child( current_id ) )
	glob.screen_active_id = current_id

func update_exited():
#	if glob.screen_active_id != null:
#		hover( glob.screen_active_id )
#	else:
#		first_time()
	pass

func update_hover( hovered ):
	var bi = 0
	for btn in $frames/btns/lyt.get_children():
		if btn == hovered:
			hover( bi )
			break
		bi += 1

func first_time():
	if $frames/btns/lyt.get_child(0) != null:
		hover(0)
		first_btn = 0
		glob.button_highlight( $frames/btns/lyt.get_child(first_btn) )

func hover( id ):
	if first_btn != null:
		glob.button_unfocus( $frames/btns/lyt.get_child(first_btn) )
		first_btn = null
	if current_id == id:
		return
	if current_id != glob.screen_active_id and current_id != null:
		glob.button_unfocus( $frames/btns/lyt.get_child(current_id) )
	current_id = id
	glob.button_highlight( $frames/btns/lyt.get_child(current_id) )
	load_details()
	$frames/gallery.rect_size.x = 0

func _process(delta):
	
	if thumb_target_size != null and thumb_target_size != $frames/gallery.rect_size:
		var d = delta
		if d > .05:
			d = .05
		var diff = glob.int_speed((thumb_target_size - $frames/gallery.rect_size) * d * 10)
		$frames/gallery.rect_size += diff
