extends Node2D

onready var glob = get_node("/root/glob")

var vps = null
var width = 230
var border_gap = Vector2(20,20)
var all_notications = []

func _ready():
# warning-ignore:return_value_discarded
	get_viewport().connect( "size_changed", self, "vp_size" )
# warning-ignore:return_value_discarded
	glob.connect( "HP_notification", self, "new_notif" )
	vp_size()

func vp_size( force = false ):
	if not force and vps == get_viewport().size:
		return
	vps = get_viewport().size
	position = Vector2( vps.x - ( width + border_gap.x ), vps.y )
	$bg.rect_size = Vector2( width, vps.y )
	$scroll.rect_size = Vector2( width, vps.y + border_gap.y )
	$scroll.rect_min_size = Vector2( width, vps.y + border_gap.y )

func update_notification( data ):
	for n in all_notications:
		if n.ID == data.ID:
			n.node.text = data.text
			n.ttl = data.ttl
			return true
	return false

func store_notification( lbl, data = null ):
	var n = {
		'node': lbl,
		'ID': null,
		'ttl': glob.DEFAULT_NOTICATION_TTL
	}
	if data != null:
		n.ID = data.ID
		n.ttl = data.ttl
	all_notications.append( n )

func new_notif( data = null ):
	if data == null:
		return
	if data is String:
		var lbl = glob.tmpl_notification.duplicate()
		lbl.text = data
		lbl.visible = true
		# storing notification in history
		store_notification( lbl )
		# updating display
		push( lbl )
		pop_up()
	elif data is Dictionary and 'ID' in data and not update_notification( data ):
		if data.ttl != 0:
			var lbl = glob.tmpl_notification.duplicate()
			lbl.text = data.text
			lbl.visible = true
			# storing notification in history
			store_notification( lbl, data )
			# updating display
			push( lbl )
			pop_up()
	$scroll.scroll_vertical = 0

func push( ctrl ):
	$scroll/lyt.add_child( ctrl )

func push_on_top( ctrl ):
	var cs = []
	for c in $scroll/lyt.get_children():
		$scroll/lyt.remove_child( c )
		cs.append(c)
	$scroll/lyt.add_child( ctrl )
	for c in cs:
		$scroll/lyt.add_child( c )

func pop_up():
	pass

func _process(delta):
	
	var py = vps - ( $scroll/lyt.rect_size + border_gap )
	if all_notications.size() == 0:
		py.y += $scroll/lyt.rect_size.y + border_gap.y
	if py.y < 0:
		py.y = 0
	if position != py and position.y > py.y:
		var diff = glob.int_speed(( py - position ) * 10 * delta )
		position += diff
	else:
		position = py
	
	var i = 0
	var zombies = []
	for n in all_notications:
		if n.ttl == null:
			# self controlled notification
			continue
		n.ttl -= delta
		if n.ttl <= 0:
			$scroll/lyt.remove_child( n.node )
			zombies.append( i )
		i += 1
	if not zombies.empty():
		pop_up()
	for z in range( 0, zombies.size() ):
		var id = zombies[ zombies.size() - (z+1) ]
		all_notications.remove( id )
		
