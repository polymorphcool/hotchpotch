extends Node2D

onready var glob = get_node("/root/glob")

signal screen_selected
signal open_splash

var width = null
var height = null
var vps = null

var init_position = null
var offset = Vector2(0,0)
var target_offset = Vector2(0,0)

var main_button = null
var screen_buttons = null
var close_notification = null
var close_notification_ttl = 6

onready var frames = [ $screen_frame, $edit_frame, $credit_frame ]

func open( b ):
	if b:
		visible = true
		# removal of close_notification 
		close_notification.ttl = 0
		glob.emit( "HP_notification", close_notification )
		# emiting open signal
		glob.emit( "HP_menu_open" )
	elif !b:
		visible = false
		glob.emit( "HP_menu_close" )

func load_screens():
	$screen_frame.load_screens()
	screen_buttons = $screen_frame.get_buttons()
	for btn in screen_buttons:
		btn.connect( "pressed", self, "screen_select" )
#	screen_pressed()
	$screen_frame.first_time()

# simple delegate
func load_configuration():
	$edit_frame.load_configuration()
	$main/edit.visible = glob.screen_list[ glob.screen_active_id ].configuration != null
	vp_size( true )

func _ready():
	
	close_notification = glob.new_notification()
	close_notification.text = "press [SPACE] to open menu" 
	close_notification.ttl = close_notification_ttl
	
	reset_all()
# warning-ignore:return_value_discarded
	get_viewport().connect( "size_changed", self, "vp_size" )
# warning-ignore:return_value_discarded
	$main/credits.connect( "pressed", self, "credits_pressed" )
# warning-ignore:return_value_discarded
	$main/edit.connect( "pressed", self, "edit_pressed" )
# warning-ignore:return_value_discarded
	$main/close.connect( "pressed", self, "close_pressed" )
# warning-ignore:return_value_discarded
	$main/quit.connect( "pressed", self, "bye_bye" )
# warning-ignore:return_value_discarded
	$main/screen.connect( "pressed", self, "screen_pressed" )
	# loading credits
	$credit_frame.load_credits()
	# hiding buttons
	$main/control.visible = false
	$main/edit.visible = false
	$main/close.visible = false
	vp_size()
	open( visible )
	screen_pressed()

func screen_select():
	emit_signal( "screen_selected", $screen_frame.current_id )
	if not $main/close.visible:
		$main/close.visible = true
		vp_size( true )

func bye_bye():
	get_tree().quit()

func reset_all():
	# hide all frames
	for f in frames:
		f.visible = false
	# reset all buttons
	for btn in $main.get_children():
		glob.button_unfocus( btn )
	target_offset = Vector2( 0,0 )

func close_pressed():
	close_notification.ttl = close_notification_ttl
	target_offset = Vector2( 0,0 )
	target_offset = Vector2( 0,0 )
	open( false )

func credits_pressed():
	emit_signal( "open_splash" )

func edit_pressed():
	var do_open = true
	reset_all()
	glob.button_focus( $main/edit )
	$edit_frame.visible = true
	if do_open:
		target_offset = Vector2( 0,-$main.rect_size.y )
		pass

func screen_pressed():
	var do_open = true
	reset_all()
	glob.button_focus( $main/screen )
	$screen_frame.to_top()
	$screen_frame.visible = true
	target_offset = Vector2( 0,0 )
	if do_open:
		pass

func vp_size( force = false ):
	
	if not force and vps == get_viewport().size:
		return
	
	vps = get_viewport().size
	width = int( vps.x / 3 )
	height = int( vps.y / 3 )
	var mw = 0
	for btn in $main.get_children():
		if btn.visible:
			mw += btn.rect_size.x
	var x = max( 10, width - mw )
	init_position = Vector2( x, height * 2 )
	self.position = init_position + offset
	
	$screen_frame.column0 = mw
	$edit_frame.column0 = mw
	
	$screen_frame.set_dimension( Vector2( vps.x - init_position.x, height ) )
	$edit_frame.set_dimension( Vector2( vps.x - init_position.x, height ) )
	$credit_frame.set_dimension( Vector2( mw * 2, height ) )
	
# warning-ignore:unused_argument
func _process(delta):
	if offset != target_offset:
		var diff = glob.int_speed( ( target_offset - offset ) * delta * 10 )
		offset += diff
		self.position = init_position + offset

func in_control( v2, ctrl ):
	if not ctrl.visible:
		return false
	return v2.x >= ctrl.rect_position.x and v2.y >= ctrl.rect_position.y and v2.x <= ctrl.rect_position.x + ctrl.rect_size.x and v2.y <= ctrl.rect_position.y + ctrl.rect_size.y

func has_point( v2 ):
	v2 = to_local( v2 )
	return in_control( v2, $main ) or in_control( v2, $credit_frame ) or in_control( v2, $edit_frame ) or in_control( v2, $screen_frame )

func _input(event):
	if glob.screen_active_id != null and visible and event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed:
		if not has_point( event.position ):
			close_pressed()
	if event.is_action( "ui_select" ) and event.is_pressed() and not self.visible:
		open( !self.visible )
		$screen_frame.to_top()
		$credit_frame.to_top()
	elif event.is_action( "ui_cancel" ):
		self.bye_bye()
