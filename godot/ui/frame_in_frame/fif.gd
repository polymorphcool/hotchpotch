#tool

extends Node2D

var height = null
var vps = null
var target_height = null

func _ready():
	get_viewport().connect( "size_changed", self, "vp_size" )
	vp_size()

func vp_size():
	if vps == get_viewport().size:
		return
	vps = get_viewport().size
	height = 0
	target_height = int( vps.y / 3 )
	$mainframe.rect_position = Vector2( 0, 0 )
	$mainframe.rect_size.x = vps.x
	$mainframe.rect_size.y = 0
	$mainframe/vp.size = Vector2( vps.x, target_height )
	print( $mainframe.rect_size )
	print( $mainframe/vp.size )

func _process(delta):
	if height != target_height:
		var s = ( target_height - height ) * delta * 0.5
		if s < 1:
			s = 1
		var h = height + s
		if abs( h - target_height ) < 1:
			h = target_height
		height = int( h )
		$mainframe.rect_position.y = height
		$mainframe.rect_size.y = height
		$mainframe/vp/root.position.y = target_height - height
