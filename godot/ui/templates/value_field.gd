extends Container

onready var glob = get_node("/root/glob")
onready var fx = get_node("/root/fx")

export (glob.HP_TYPE) var type = glob.HP_TYPE._undefined
export (fx.FX_TYPE) var _fx = fx.FX_TYPE._undefined setget set_fx

func set_fx( f ):
	_fx = f
	apply_fx()

func apply_fx():
	if valfx== null or _fx == fx.FX_TYPE._undefined:
		return
	fx.fx_add( valfx, _fx )

var valfx= null
var name_field = null
var slider_field = null
var value_field = null
var reset_button = null
var configuration = null
var current_value = null
var editing = false

var slider_fields = []
var value_fields = []

func connect_lists():
	if slider_fields != null:
		for sf in slider_fields:
			sf.connect("mouse_entered", self, "start_editing")
			sf.connect("mouse_exited", self, "stop_editing")
			sf.connect( "value_changed", self, "numeric_slider" )
	if value_fields != null:
		for vf in value_fields:
			vf.connect("mouse_entered", self, "start_editing")
			vf.connect("mouse_exited", self, "stop_editing")
			vf.connect( "text_changed", self, "numeric_text" )
			vf.connect( "focus_exited", self, "numeric_text_validate" )

func _ready():
	if type == glob.HP_TYPE._vec3:
		name_field = $display/name
		slider_fields = []
		slider_fields.append( $x/slider )
		slider_fields.append( $y/slider )
		slider_fields.append( $z/slider )
		value_fields = []
		value_fields.append( $x/value )
		value_fields.append( $y/value )
		value_fields.append( $z/value )
		reset_button = $display/reset
		connect_lists()
		reset_button.connect( "pressed", self, "reset" )
	elif is_numeric():
		name_field = $name
		value_field = $value
		slider_field = $slider
		reset_button = $reset
		slider_field.connect("mouse_entered", self, "start_editing")
		slider_field.connect("mouse_exited", self, "stop_editing")
		value_field.connect("mouse_entered", self, "start_editing")
		value_field.connect("mouse_exited", self, "stop_editing")
		slider_field.connect( "value_changed", self, "numeric_slider" )
		value_field.connect( "text_changed", self, "numeric_text" )
		value_field.connect( "focus_exited", self, "numeric_text_validate" )
		reset_button.connect( "pressed", self, "reset" )
	elif type == glob.HP_TYPE._bool:
		name_field = $name
		value_field = $value
		value_field.connect( "pressed", self, "bool_changed" )

func start_editing():
	editing = true
func stop_editing():
	editing = false
	if valfx != null:
		fx.fx_send( valfx, current_value )

func load_config( key, conf ):
	
	configuration = null
	name_field.text = key
	
	if type != conf.type:
		name_field.text += "WRONG TYPE "
		slider_field = null
		value_field = null
		print( "value field '" + key + "' is not the right type!" )
		return
	
	if not 'set' in conf or conf.set == null:
		name_field.text += "NO SETTER FOR "
		slider_field = null
		value_field = null
		print( "NO SET IN CONFIGURATION " + name_field.text + "? how do you want me to push, moron?" )
		return
	
	configuration = conf
	current_value = configuration.default
	if configuration.get != null:
		current_value = configuration.get.call_func()
		
	if type == glob.HP_TYPE._vec3:
		value_fields[0].text = str(current_value.x)
		value_fields[1].text = str(current_value.y)
		value_fields[2].text = str(current_value.z)
		numeric_text( current_value )
		if 'fx' in configuration:
			print( "No support yet for fx on complex types..." )
		
	elif is_numeric():
			value_field.text = str(current_value)
			numeric_text( current_value )
			if 'fx' in configuration:
				valfx = fx.fx_real( configuration.default )
				set_fx( configuration.fx )
				apply_fx()
	
	elif type == glob.HP_TYPE._bool:
			value_field.pressed = current_value

func is_numeric():
	return type == glob.HP_TYPE._real or type == glob.HP_TYPE._int

func numeric_slider( v ):
	
	if configuration == null:
		return
	
	if type == glob.HP_TYPE._vec3:
		
		current_value.x = slider_fields[0].value
		current_value.y = slider_fields[1].value
		current_value.z = slider_fields[2].value
		for i in range( 0, value_fields.size() ):
			value_fields[i].disconnect( "text_changed", self, "numeric_text" )
			value_fields[i].text = str(slider_fields[i].value)
			value_fields[i].connect( "text_changed", self, "numeric_text" )
			
	else:
		
		current_value = v
		if type == glob.HP_TYPE._int:
			current_value = int(v)
		value_field.disconnect( "text_changed", self, "numeric_text" )
		value_field.text = str(current_value)
		value_field.connect( "text_changed", self, "numeric_text" )
		
	update_value()

func numeric_text( t ):
	
	if configuration == null:
		return
	
	if type == glob.HP_TYPE._vec3:
		
		var vls = []
		for i in range( 0, value_fields.size() ):
			vls.append( float( value_fields[i].text ) )
			if not configuration.range_auto:
				if vls[i] < configuration.range[0]:
					vls[i] = configuration.range[0]
				elif vls[i] >configuration.range[1]:
					vls[i] = configuration.range[1]
		
		current_value.x = vls[0]
		current_value.y = vls[1]
		current_value.z = vls[2]
		numeric_text_validate()
		
		for i in range( 0, slider_fields.size() ):
			slider_fields[i].disconnect( "value_changed", self, "numeric_slider" )
		update_slider_range()
		for i in range( 0, slider_fields.size() ):
			slider_fields[i].value = vls[i]
			slider_fields[i].connect( "value_changed", self, "numeric_slider" )
		
		update_value()
	
	else:
		
		current_value = float( t )
		if type == glob.HP_TYPE._int:
			current_value = int( t )
		slider_field.disconnect( "value_changed", self, "numeric_slider" )
		if not configuration.range_auto:
			if current_value < configuration.range[0]:
				current_value = configuration.range[0]
			elif current_value > configuration.range[1]:
				current_value = configuration.range[1]
		numeric_text_validate()
		update_slider_range()
		slider_field.value = current_value
		slider_field.connect( "value_changed", self, "numeric_slider" )
		update_value()

func numeric_text_validate():
	
	if configuration == null:
		return
	
	if type == glob.HP_TYPE._vec3:
		var carets = []
		for vs in value_fields:
			vs.disconnect( "text_changed", self, "numeric_text" )
			carets.append( vs.caret_position )
		value_fields[0].text = str(float(current_value.x))
		value_fields[1].text = str(float(current_value.y))
		value_fields[2].text = str(float(current_value.z))
		for i in range(0, value_fields.size()):
			var vs = value_fields[i]
			vs.connect( "text_changed", self, "numeric_text" )
			vs.caret_position = carets[i]
		
	else:
		value_field.disconnect( "text_changed", self, "numeric_text" )
		match( configuration.type ):
			glob.HP_TYPE._real:
				value_field.text = str(float(current_value))
			glob.HP_TYPE._int:
				value_field.text = str(int(current_value))
		value_field.connect( "text_changed", self, "numeric_text" )

func bool_changed():
	if configuration == null or type != glob.HP_TYPE._bool:
		return
	update_value()

func update_slider_range():
	if configuration == null:
		return
	
	if type == glob.HP_TYPE._vec3:
		var vls = [ current_value.x, current_value.y, current_value.z ]
		for i in range( 0, slider_fields.size() ):
			if configuration.range_auto and vls[i] < configuration.range[0]:
				slider_fields[i].min_value = vls[i]
			else:
				slider_fields[i].min_value = configuration.range[0]
			if configuration.range_auto and vls[i] > configuration.range[1]:
				slider_fields[i].max_value = vls[i]
			else:
				slider_fields[i].max_value = configuration.range[1]
	else:
		if configuration.range_auto and current_value < configuration.range[0]:
			slider_field.min_value = current_value
		else:
			slider_field.min_value = configuration.range[0]
		if configuration.range_auto and current_value > configuration.range[1]:
			slider_field.max_value = current_value
		else:
			slider_field.max_value = configuration.range[1]

func update_value():
	if configuration == null:
		return
	if type == glob.HP_TYPE._vec3:
		configuration.set.call_func( current_value )
	elif is_numeric():
		configuration.set.call_func( float( value_field.text ) )
	elif type == glob.HP_TYPE._bool:
		configuration.set.call_func( value_field.pressed )

func reset():
	if configuration != null:
		if type == glob.HP_TYPE._vec3:
			for vs in value_fields:
				vs.disconnect( "text_changed", self, "numeric_text" )
			value_fields[0].text = str(configuration.default.x)
			value_fields[1].text = str(configuration.default.y)
			value_fields[2].text = str(configuration.default.z)
			numeric_text( "" )
			for vs in value_fields:
				vs.connect( "text_changed", self, "numeric_text" )
				vs.caret_position = len( vs.text )
		else:
			value_field.disconnect( "text_changed", self, "numeric_text" )
			value_field.text = str(configuration.default)
			numeric_text( value_field.text )
			value_field.connect( "text_changed", self, "numeric_text" )

func _process(delta):
	if not visible && editing:
		stop_editing()
	if editing or valfx == null:
		return
	if fx.update( valfx, delta ):
		value_field.text = str(valfx.current)
		numeric_text( valfx.current )
