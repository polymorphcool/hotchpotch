extends Button

onready var glob = get_node("/root/glob")

export (glob.HP_TYPE) var type = glob.HP_TYPE._undefined

var name_field = null
var configuration = null
var callback = null

func _ready():
	pass

func set_callback( function, parameter ):
	callback = { 'function': function, 'parameter': parameter }
	self.connect( "pressed", self, "call_function" )

func load_config( key, conf ):
	configuration = null
	if type != conf.type:
		text = "WRONG TYPE " + key
		print( "value field '" + key + "' is not the right type!" )
	if not 'set' in conf or conf.set == null:
		print( "NO SET IN CONFIGURATION " + key + "? how do you want me to push, moron?" )
		return
	configuration = conf
	text = key
	self.connect( "pressed", self, "call_function" )

func call_function():
	if configuration != null:
		configuration.set.call_func()
	elif callback != null:
		callback.function.call_func( callback.parameter )
