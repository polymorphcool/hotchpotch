extends VBoxContainer

onready var glob = get_node("/root/glob")

var path_field = null
var content_field = null
var update_button = null
var reset_button = null
var configuration = null

func _ready():
	path_field = $menu/path
	content_field = $content
	update_button = $menu/update
	reset_button = $menu/reset
	# auto size
	if get_parent() is Control:
		var fsize = get_parent().rect_size
		content_field.rect_size.y = fsize.y - update_button.rect_size.y
		content_field.rect_min_size.y = fsize.y - update_button.rect_size.y
	update_button.connect( "pressed", self, "update_shader" )
	reset_button.connect( "pressed", self, "reset_shader" )

func load_config( key, conf ):
	configuration = null
	if conf.type != glob.HP_TYPE._shader:
		print( "value field '" + key + "' is not the right type!" )
		return
	if not 'set' in conf or conf.set == null or not 'get' in conf or conf.get == null:
		print( "NO SET/GET IN CONFIGURATION? how do you want me to interact with shader, moron?" )
		return
	content_field.text = conf.get.call_func().code
	configuration = conf
	update_path()

func update_path():
	var p = configuration.get.call_func().resource_path
	if p == "":
		p = "tmp://shader"
	path_field.text = p

func update_shader():
	if configuration == null:
		return
	var s = Shader.new()
	s.code = content_field.text
	configuration.set.call_func( s )
	update_path()

func reset_shader():
	if configuration == null or not 'default' in configuration:
		return
	configuration.set.call_func( configuration.default )
	content_field.text = configuration.default.code
	update_path()


