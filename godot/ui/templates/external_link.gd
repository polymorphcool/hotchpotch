extends Label

export(String) var address = null

var hovered = false
var url = null

func _ready():
	self.connect( "mouse_entered", self, "activate" )
	self.connect( "mouse_exited", self, "deactivate" )
	if address != null:
		url = address

func activate():
	hovered = url != null
	set( "custom_styles/normal", glob.url_hover_stylebox )

func deactivate():
	hovered = false
	set( "custom_styles/normal", glob.url_normal_stylebox )

func _input(event):
	if url == null or not hovered:
		return
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.is_pressed():
		OS.shell_open(url)
		hovered = false
