extends VBoxContainer

onready var glob = get_node("/root/glob")

func load_config( key, conf ):
	
	$display/name.text = key
	$values.columns = conf.columns
	
	var tmpl = glob.fields[ glob.HP_TYPE._func ]
	for item in conf.default:
		var btn = tmpl.duplicate()
		btn.text = str( item )
		btn.set_callback( conf.set, item )
		$values.add_child( btn )
