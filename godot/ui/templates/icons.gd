extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	for c in $grid.get_children():
		c.render_target_update_mode = Viewport.UpdateMode.UPDATE_ONCE
