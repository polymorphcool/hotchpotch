extends Button

signal hover

func _ready():
	self.connect( "mouse_entered", self, "mentered" )

func mentered():
	emit_signal( "hover", self )
