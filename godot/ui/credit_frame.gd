extends Control

onready var glob = get_node("/root/glob")

func to_top():
	$scroll.scroll_vertical = 0

func load_credits():
	# cleanup
	while $scroll/lyt.get_child_count() > 0:
		$scroll/lyt.remove_child( $scroll/lyt.get_child(0) )
	# generation
	glob.load_credits( $scroll/lyt )
		
func set_dimension( v2 ):
	if rect_size == v2:
		return
	rect_size = v2
	$bg.rect_size = v2
	$scroll.rect_size = rect_size - Vector2( 0,10 )
	load_credits()
