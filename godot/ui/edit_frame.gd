extends Control

onready var glob = get_node("/root/glob")

var column0 = 200
var hover_var_id = null
var current_var_id = null
var current_conf = null
var button_tmpl = null

func _ready():
	# storing templates
	button_tmpl = $btns_frame/lyt/btn0.duplicate()
	add_child( button_tmpl )
	button_tmpl.visible = false
	# cleanup placeholders
	purge_all()
	# visibility adjustments
	$nothing.visible = true
	$edit.visible = false

func set_dimension( v2 ):
	rect_size = v2
	$bg.rect_size = v2
	$btns_frame.rect_size.x = column0
	$btns_frame/lyt.rect_size.x = column0
	edit_size()

func purge_all():
	while $btns_frame/lyt.get_child_count() > 0:
		$btns_frame/lyt.remove_child( $btns_frame/lyt.get_child(0) )
	purge_fields()

func purge_fields():
	while $edit/lyt.get_child_count() > 0:
		$edit/lyt.remove_child( $edit/lyt.get_child(0) )

func load_configuration():
	
	hover_var_id = null
	current_var_id = null
	
	purge_all()
	if glob.screen_active_id == null or glob.screen_active_scene == null:
		$nothing.visible = true
		return
	$nothing.visible = false
	$edit.visible = true
	current_conf = glob.screen_list[ glob.screen_active_id ].configuration
	if current_conf == null:
		return
	for k in current_conf:
		var btn = button_tmpl.duplicate()
		$btns_frame/lyt.add_child( btn )
		btn.visible = true
		btn.name = k
		btn.text = k
		btn.connect( "hover", self, "update_var_hover" )
		btn.connect( "mouse_exited", self, "update_var_exited" )
		btn.connect( "pressed", self, "var_select" )

func hover( id ):
	if hover_var_id == id or ( current_var_id != null and hover_var_id == current_var_id):
		return
	if hover_var_id != null:
		glob.button_unfocus( $btns_frame/lyt.get_child(hover_var_id) )
	hover_var_id = id
	glob.button_highlight( $btns_frame/lyt.get_child(hover_var_id) )

func update_var_hover( hovered ):
	var bi = 0
	for btn in $btns_frame/lyt.get_children():
		if btn == hovered:
			hover( bi )
			break
		bi += 1

func update_var_exited():
	if hover_var_id != current_var_id:
		glob.button_unfocus( $btns_frame/lyt.get_child(hover_var_id) )
	hover_var_id = null

func swap_lyt():
	var p = $edit/lyt.rect_position
	var s = $edit/lyt.rect_size
	if $edit/lyt is HBoxContainer:
		$edit.remove_child( $edit/lyt )
		var nl = VBoxContainer.new();
		nl.name = "lyt"
		$edit.add_child( nl )
	elif $edit/lyt is VBoxContainer:
		$edit.remove_child( $edit/lyt )
		var nl = HBoxContainer.new();
		nl.name = "lyt"
		$edit.add_child( nl )
	$edit/lyt.rect_position = p
	$edit/lyt.rect_size = s

func edit_size():
	$edit.rect_position.x = column0
	$edit.rect_size = Vector2( rect_size.x - ( column0 + 10 ), rect_size.y )
	$edit/bg.rect_size = $edit.rect_size
	$edit/lyt.rect_size = $edit.rect_size

func add_field( container, key, conf ):
	var lbl_field = null
	var edit_field = null
	
	# loading correspoding field if some
	if conf.type in glob.fields:
		edit_field = glob.fields[ conf.type ].duplicate()
	
	# special cases requiring manual intervention
	match( conf.type ):
		glob.HP_TYPE._label:
			lbl_field = glob.tmpl_label.duplicate()
		glob.HP_TYPE._group:
			for k in conf.data:
				add_field( container, k, conf.data[k] )
			return
	
	# pushing the fields in the layout
	if edit_field != null:
		container.add_child( edit_field )
		edit_field.load_config( key, conf )
	elif lbl_field != null:
		container.add_child( lbl_field )
		lbl_field.text = key
	
func load_group( params ):
	# is this group only contains groups?
	var only_groups = true
	var columns = 0
	for k in params.data:
		if params.data[k].type != glob.HP_TYPE._group:
			only_groups = false
			break
		columns += 1
	if only_groups:
		if $edit/lyt is VBoxContainer:
			swap_lyt()
		for k in params.data:
			var subl = VBoxContainer.new()
			subl.size_flags_horizontal += SIZE_EXPAND
			$edit/lyt.add_child( subl )
			var subtitle = glob.tmpl_label.duplicate()
			subtitle.text = k
			subl.add_child( subtitle )
			var g = params.data[k]
			for l in g.data:
				add_field( subl, l, g.data[l] )
	else:
		if $edit/lyt is HBoxContainer:
			swap_lyt()
		for k in params.data:
			add_field( $edit/lyt, k, params.data[k] )

func var_select():
	
	if current_var_id != null:
		glob.button_unfocus( $btns_frame/lyt.get_child(current_var_id) )
	current_var_id = hover_var_id
	glob.button_focus( $btns_frame/lyt.get_child(current_var_id) )
	
	purge_fields()
	
	var key = $btns_frame/lyt.get_child(current_var_id).name
	var c = current_conf[ key ]
	
	var field = null
	if c.type in glob.fields:
		field = glob.fields[ c.type ].duplicate()
	elif c.type == glob.HP_TYPE._group:
		load_group( c )
	
	if field != null:
		if $edit/lyt is HBoxContainer:
			swap_lyt()
		$edit/lyt.add_child( field )
		field.load_config( key, c )
