#tool

extends MultiMeshInstance

export(float, -2, 2) var speed
export(float, -2, 2) var rgb_speed
export(float, -2, 2) var height_speed
export(float,-2,2) var height_max setget set_height_max
export(bool) var control_cam = true
var height_mix = 0

onready var rgb_offset = Vector3(0,0,0)
onready var height_offset = Vector3(0,0,0)
onready var dir = Vector3( 0, 1, 0 )
onready var elapsed = 0
onready var diff = 0
onready var prev_roty = 0

func set_height_max(f):
	height_max = f
	upadet_heights()

func set_height_mix(f):
	height_mix = f
	upadet_heights()

func upadet_heights():
	material_override.set_shader_param( "height_0", height_max * (1-height_mix) )
	material_override.set_shader_param( "height_1", height_max * height_mix )

func _ready():
	upadet_heights()

func _physics_process(delta):
	elapsed += delta
	var s = sin( elapsed * 0.07 )
	var ms = 1
	if s < 0:
		ms *= -1
	s = pow( s, 10 ) * ms
	diff = 0.9 * s
	prev_roty = rotation.y
	rotation.y = -diff

func _process(delta):
	
	elapsed += delta
	height_mix = (1+sin( elapsed * 0.1 )) * 0.5
	upadet_heights()
	
	if control_cam:
#		var left = Quat( Vector3(0,0,1), ( rotation.y - prev_roty ) * -100 )
#		get_node( "../cam" ).transform.basis = Basis( left )
		get_node( "../cam" ).rotation.z = -rotation.y
	
	dir.y = cos( diff )
	dir.x = sin( diff )
	rgb_offset += dir * rgb_speed * speed * delta
	height_offset += dir * height_speed * speed * delta
	material_override.set_shader_param( "uv_rgb_offset", rgb_offset )
	material_override.set_shader_param( "uv_height_offset", height_offset )
	pass
