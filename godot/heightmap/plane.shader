shader_type spatial;
render_mode blend_mix,depth_draw_alpha_prepass,cull_back,diffuse_burley,specular_schlick_ggx;

uniform sampler2D texture_height_0 : hint_albedo;
uniform sampler2D texture_height_1 : hint_albedo;
uniform float height_0;
uniform float height_1;
uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform float point_size : hint_range(0,128);
uniform sampler2D texture_metallic : hint_white;
uniform vec4 metallic_texture_channel;
uniform sampler2D texture_roughness : hint_white;
uniform vec4 roughness_texture_channel;
uniform vec3 uv_rgb_scale;
uniform vec3 uv_rgb_offset;
uniform vec3 uv_height_scale;
uniform vec3 uv_height_offset;

uniform float height_shadow;
uniform float min_alpha;

void vertex() {
	vec2 huv = UV * uv_height_scale.xy - vec2( -0.5,-0.5 ) - uv_height_scale.xy * 0.5 + uv_height_offset.xy;
	vec4 hcolor = texture(texture_height_0,huv) * height_0 + texture(texture_height_1,huv) * height_1;
	VERTEX.y = hcolor.r;
}

void fragment() {
	vec2 base_uv = UV * uv_rgb_scale.xy - vec2( -0.5,-0.5 ) - uv_rgb_scale.xy * 0.5 + uv_rgb_offset.xy;
	vec2 huv = UV * uv_height_scale.xy - vec2( -0.5,-0.5 ) - uv_height_scale.xy * 0.5 + uv_height_offset.xy;
	vec4 albedo_tex = texture(texture_albedo,base_uv);
	vec4 height_tex = texture(texture_height_0,huv) * height_0 + texture(texture_height_1,huv) * height_1;
	float amult =  ( 1.0 - height_shadow ) +  ( 1.0 - pow( 1.0-height_tex.r, 4 ) * height_shadow );
	ALBEDO = albedo.rgb * albedo_tex.rgb * amult;
	float metallic_tex = dot(texture(texture_metallic,base_uv),metallic_texture_channel);
	METALLIC = metallic_tex * metallic;
	float roughness_tex = dot(texture(texture_roughness,base_uv),roughness_texture_channel);
	ROUGHNESS = roughness_tex * roughness;
	SPECULAR = specular;
	ALPHA = min_alpha + albedo.a * albedo_tex.a;
}
