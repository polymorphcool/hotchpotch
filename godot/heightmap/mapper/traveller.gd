#tool

extends Node2D

onready var mapglob = get_node("/root/mapglob")
onready var target_tmpl = $tmpls/target
onready var targets = [
	# just in front
	{ 'level': 0, 'node': null, 'dir': Vector2(0,0), 'pos': Vector2(0,0), 'color': null, 'distance': 0.005, 'angle': -1.2, 'weight': 0.75 }, # far left
	{ 'level': 0, 'node': null, 'dir': Vector2(0,0), 'pos': Vector2(0,0), 'color': null, 'distance': 0.005, 'angle': -0.6, 'weight': 0.9 }, # left
	{ 'level': 0, 'node': null, 'dir': Vector2(0,0), 'pos': Vector2(0,0), 'color': null, 'distance': 0.005, 'angle': 0.0, 'weight': 1 }, # in front
	{ 'level': 0, 'node': null, 'dir': Vector2(0,0), 'pos': Vector2(0,0), 'color': null, 'distance': 0.005, 'angle': 0.6, 'weight': 0.9 }, # right
	{ 'level': 0, 'node': null, 'dir': Vector2(0,0), 'pos': Vector2(0,0), 'color': null, 'distance': 0.005, 'angle': 1.2, 'weight': 0.75 }, # far right
	# near
	{ 'level': 1, 'node': null, 'dir': Vector2(0,0), 'pos': Vector2(0,0), 'color': null, 'distance': 0.015, 'angle': -1.0, 'weight': 0.4 }, # far left
	{ 'level': 1, 'node': null, 'dir': Vector2(0,0), 'pos': Vector2(0,0), 'color': null, 'distance': 0.015, 'angle': -0.5, 'weight': 0.45 }, # left
	{ 'level': 1, 'node': null, 'dir': Vector2(0,0), 'pos': Vector2(0,0), 'color': null, 'distance': 0.015, 'angle': 0, 'weight': 0.5 }, # in front
	{ 'level': 1, 'node': null, 'dir': Vector2(0,0), 'pos': Vector2(0,0), 'color': null, 'distance': 0.015, 'angle': 0.5, 'weight': 0.45 }, # right
	{ 'level': 1, 'node': null, 'dir': Vector2(0,0), 'pos': Vector2(0,0), 'color': null, 'distance': 0.015, 'angle': 1.0, 'weight': 0.4 }, # far right
	# far
	{ 'level': 2, 'node': null, 'dir': Vector2(0,0), 'pos': Vector2(0,0), 'color': null, 'distance': 0.05, 'angle': -0.6, 'weight': 0.3 }, # left
	{ 'level': 2, 'node': null, 'dir': Vector2(0,0), 'pos': Vector2(0,0), 'color': null, 'distance': 0.05, 'angle': 0.0, 'weight': 0.3 }, # in front
	{ 'level': 2, 'node': null, 'dir': Vector2(0,0), 'pos': Vector2(0,0), 'color': null, 'distance': 0.05, 'angle': 0.6, 'weight': 0.3 } # right
]

var initialised = false
var map_repeat = 0
var map_size = null
var map_ratio = null
var seek_position = Vector2(0,0)
var seek_absolute = Vector2(0,0)
var seek_dir = Vector2(0,1)
var seek_symmetry = Vector2(1,1)
var seek_speed = 0.02
var steer_speed = -1
var seek_color_previous = null
var seek_color = null
var slope = 0
var total_target_weight = 0

func randomise():
	seek_position = Vector2(rand_range(0,1),rand_range(0,1))
	seek_absolute = seek_position
	seek_dir = Vector2(rand_range(-1,1),rand_range(-1,1)).normalized()
	update_targets()

func get_positions():
	var out = [ seek_position ]
	for tc in targets:
		out.append( tc.pos )
	return out

func update_targets():
	for tc in targets:
		tc.dir = seek_dir.rotated( tc.angle ).normalized()
		tc.pos = mapglob.bound( seek_position + tc.dir * tc.distance, map_repeat ).pos

func update_colors( colors ):
	for i in range(0, colors.size()):
		if i == 0:
			seek_color_previous = seek_color
			seek_color = colors[i]
			if seek_color_previous != null:
				slope = seek_color_previous.x - seek_color.x
		else:
			targets[i-1].color = colors[i]

func initialise( size, repeat ):
	map_size = size
	map_ratio = Vector2( 1, map_size.x / map_size.y )
	map_repeat = repeat
	# cleaning previous targets
	while $targets.get_child_count() > 1:
		$targets.remove_child( $targets.get_child(1) )
	# creation of targets
	for tc in targets:
		total_target_weight += tc.weight
		var t = target_tmpl.duplicate()
		tc.node = t
		$targets.add_child( t )
	initialised = true
	update_targets()

func _ready():
	rotation = 0

func analyse_terrain( delta ):
	if seek_color == null:
		return
	# that's the heavy stuff: tarveller has to steer depending on the terrain analysis!
	var slopes = []
	var min_slope = null
	var max_slope = null
	for tc in targets:
		# slope computation for each target
		var tslope = tc.color.x - seek_color.x
		slopes.append( tslope )
		if min_slope == null or min_slope > tslope:
			min_slope = tslope
		if max_slope == null or max_slope < tslope:
			max_slope = tslope
	# avoid unnecessary computation
	if min_slope == max_slope:
		return
	# now we normalise slopes
	var new_dir = Vector2(0,0)
	for i in range(0,slopes.size()):
		# aiming to minimal slopes
		var pc = (1 - ( slopes[i] - min_slope ) / ( max_slope - min_slope )) * targets[i].weight
		new_dir += targets[i].dir * pc
	new_dir = new_dir.normalized()
	if steer_speed > 0:
		seek_dir += new_dir * delta * steer_speed
		seek_dir = seek_dir.normalized()
	else:
		seek_dir = new_dir

func move( delta ):
	# absolute position do NOT care about symmetry! 
	# => any symmetry must be cancelled to compute this position
	seek_absolute += seek_dir * map_ratio * seek_symmetry * seek_speed * delta
	# now, let's compute the local position of the traveller on the map
	var bounded = mapglob.bound( seek_position + seek_dir * map_ratio * seek_speed * delta, map_repeat )
	seek_position = bounded.pos
	# and change the direction if a symmetry is required
	if bounded.symmetry != null:
		seek_symmetry = bounded.symmetry
		seek_dir *= bounded.symmetry
	update_targets()
	if mapglob.update_display:
		$arrow.position = seek_position * map_size
		$arrow.rotation = seek_dir.angle()
		for tc in targets:
			tc.node.position = tc.pos * map_size

func _process(delta):
	if not initialised:
		return
	# making decision on where to go
	analyse_terrain( delta )
	# one step further
	move( delta )
