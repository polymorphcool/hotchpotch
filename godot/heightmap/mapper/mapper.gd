#tool

extends Node2D

export (int) var traveller_num = 1
export (bool) var update_display = false
export (bool) var debug = true

onready var mapglob = get_node("/root/mapglob")
var traveller_tmpl = load( "res://heightmap/mapper/traveller.tscn" )
var initialised = false
var map_repeat = 0
var map_size = null
var map_pixels = null
var world_info = null
var world_min = Vector2(0,0)
var world_max = Vector2(0,0)
var world_tiles = Vector2(0,0)
var tile_size = 0

func init_map():
	initialised = true
	if $map.texture == null:
		map_size = null
		map_pixels = null
		return
	map_repeat = $map.texture.flags
	if map_repeat & Texture.FLAG_MIRRORED_REPEAT != 0:
		map_repeat = Texture.FLAG_MIRRORED_REPEAT
	elif map_repeat & Texture.FLAG_REPEAT != 0:
		map_repeat = Texture.FLAG_REPEAT
	else:
		map_repeat = 0
	map_size = $map.texture.get_size()
	map_pixels = $map.texture.get_data()
	map_pixels.lock()
	# map display to top left corner
	$map.position = map_size * 0.5
	$world.position = Vector2(map_size.x,0)
	# initialise travellers
	for c in $travellers.get_children():
		c.initialise( map_size, map_repeat )
	if debug:
		while $debug.get_child_count() > 0:
			$debug.remove_child($debug.get_child(0))
		var dbg_offset = Vector2( 0, map_size.y )
		var dbg_color = $tmpls/color
		var dbg_slope = $tmpls/slope
		for c in $travellers.get_children():
			var pos = c.get_positions()
			var off = dbg_offset
			var x = 0
			for p in pos:
				var sq = dbg_color.duplicate()
				sq.position = off + Vector2(x,0)
				x += 21
				sq.visible = true
				$debug.add_child( sq )
			# slope rendering
			var slope = dbg_slope.duplicate()
			slope.position = off + Vector2(x + 10,10)
			$debug.add_child( slope )
			dbg_offset.y += 21
		# adding info label
		var info = $tmpls/info.duplicate()
		info.rect_position = dbg_offset
		info.text = "map size: " + str( map_size.x ) + "x" + str( map_size.y ) + "\n"
		info.text += "flag: "
		match map_repeat:
			Texture.FLAG_MIRRORED_REPEAT:
				info.text += "mirror"
			Texture.FLAG_REPEAT:
				info.text += "repeat"
			_:
				info.text += "none"
		$debug.add_child( info )
		dbg_offset.y += info.rect_size.y
		world_info = $tmpls/info.duplicate()
		world_info.rect_position = dbg_offset
		$debug.add_child( world_info )
		var title = $tmpls/info.duplicate()
		title.text = "texture space coordinates"
		add_child(title)
		title.rect_position = Vector2(5,5)

func update_world_info():
	if debug:
		world_info.text = "world box: " + str(world_min.x) + ", " + str(world_min.y)
		world_info.text += " > " + str(world_max.x) + ", " + str(world_max.y) + "\n"
		world_info.text += "world tiles: " + str(world_tiles.x*world_tiles.y)

func _ready():
	mapglob.update_display = update_display
	for i in range(0,traveller_num):
		var t = traveller_tmpl.instance()
		$travellers.add_child( t )
	init_map()
	randomize()
	for t in $travellers.get_children():
		t.randomise()

func get_color( pos ):
	
	# position MUST be bounded!
	var pixid = map_size * pos
	
	if pixid.x > map_size.x or pixid.y > map_size.y:
		print( "!!!!! ", pos )
		return Vector3(1,0,0)
	
	var weight = Vector2( pixid.x - floor(pixid.x), pixid.y - floor(pixid.y) )
	var kernel = [
		[floor(pixid.x), 1 - weight.x],
		[floor(pixid.y), 1 - weight.y],
		[int(ceil(pixid.x)) % int(map_size.x), weight.x],
		[int(ceil(pixid.y)) % int(map_size.y), weight.y]
	]
	var colors = [
		[mapglob.c2vec(map_pixels.get_pixel( kernel[0][0], kernel[1][0] )), kernel[0][1]+kernel[1][1]],
		[mapglob.c2vec(map_pixels.get_pixel( kernel[0][0], kernel[3][0] )), kernel[0][1]+kernel[3][1]],
		[mapglob.c2vec(map_pixels.get_pixel( kernel[2][0], kernel[1][0] )), kernel[2][1]+kernel[1][1]],
		[mapglob.c2vec(map_pixels.get_pixel( kernel[2][0], kernel[3][0] )), kernel[2][1]+kernel[3][1]]
	]
	var mixed = Vector3(0,0,0)
	var totalw = 0
	for mx in colors:
		mixed += mx[0] * mx[1]
		totalw += mx[1]
	mixed /= totalw
	return mixed

func update_colors():
	var sqi = 0
	for c in $travellers.get_children():
		var pos = c.get_positions()
		var colors = []
		for p in pos:
			colors.append( get_color(p) )
		c.update_colors( colors )
		if debug:
			for c in colors:
				$debug.get_child(sqi).color = mapglob.vec2c( c )
				sqi += 1
			# current slope boosted & smoothed
			var r0 = $debug.get_child(sqi).rotation
			var r1 = c.slope * PI * 5
			var r = r0 + ( r1 - r0 ) * 0.1
			$debug.get_child(sqi).rotation = r
			if r > 0.01:
				$debug.get_child(sqi).color = Color.green
			elif r < -0.01:
				$debug.get_child(sqi).color = Color.red
			else:
				$debug.get_child(sqi).color = Color.gray
			sqi += 1

func update_world():
	var update_required = false
	for c in $travellers.get_children():
		var apos = c.seek_absolute
		if world_min.x > apos.x:
			world_min.x -= 1
			update_required = true
		if world_min.y > apos.y:
			world_min.y -= 1
			update_required = true
		if world_max.x < apos.x:
			world_max.x += 1
			update_required = true
		if world_max.y < apos.y:
			world_max.y += 1
			update_required = true
	if update_required:
		world_tiles = world_max - world_min
		var divider = world_tiles.x
		if divider < world_tiles.y:
			divider = world_tiles.y
		# cleaning up world
		while $world.get_child_count() > 0:
			$world.remove_child( $world.get_child(0) )
		tile_size = map_size / divider
		var tile_scale = 1 / divider
		for y in range( 0, world_tiles.y ):
			for x in range( 0, world_tiles.x ):
				var tile = $map.duplicate()
				$world.add_child( tile )
				tile.scale *= tile_scale
				tile.position.x = tile_size.x * (x+0.5)
				tile.position.y = tile_size.y * (y+0.5)
				if map_repeat == Texture.FLAG_MIRRORED_REPEAT:
					if int(abs( world_min.x + x )) % 2 == 1:
						tile.scale.x *= -1
					if int(abs( world_min.y + y )) % 2 == 1:
						tile.scale.y *= -1
				
		# adding travellers
		for i in range(0,$travellers.get_child_count()):
			var trav = $tmpls/traveller.duplicate()
			$world.add_child( trav )
		update_world_info()
		# adding title
		var title = $tmpls/info.duplicate()
		title.text = "absolute coordinates"
		$world.add_child(title)
		title.rect_position = Vector2(5,5)
	# displaying absolute positions of travellers
	var offset = world_min * -1
	var start = (world_tiles.x*world_tiles.y)
	var stop = start + $travellers.get_child_count()
	for i in range(start,stop):
		var p = ( offset + $travellers.get_child(i-start).seek_absolute ) * tile_size
		var trav = $world.get_child(i)
		trav.position = p

func _process(delta):
	if not initialised:
		init_map()
	update_colors()
	update_world()
