extends Node

var update_display = true

func c2vec( c ):
	if c == null:
		return Vector3(0,0,0)
	return Vector3( c.r, c.g, c.b )

func vec2c( v, a = 1 ):
	if v == null:
		return Color(0,0,0,1)
	return Color( v.x, v.y, v.z, a )

func bound( v2, repeat ):
	
	var out = { 'pos': v2, 'symmetry': null }
	
	# already good! no need to go further
	if v2.x >= 0 and v2.x <= 1 and v2.y >= 0 and v2.y <= 1:
		return out
	
	# constraining x & y in range [-1,2]
	while out.pos.x < -1:
		out.pos.x +=1
	while out.pos.x > 2:
		out.pos.x -=1
	while out.pos.y < -1:
		out.pos.y +=1
	while out.pos.y > 2:
		out.pos.y -=1
	
	# now the real stuff
	match repeat:
		# simple, just sending the position to the other side
		Texture.FLAG_REPEAT:
			# simple repeat
			if out.pos.x < 0:
				out.pos.x +=1
			elif out.pos.x > 1:
				out.pos.x -=1
			if out.pos.y < 0:
				out.pos.y +=1
			elif out.pos.y > 1:
				out.pos.y -=1
		# mirror, lot more complex: requires a symmetry to be applied on direction
		Texture.FLAG_MIRRORED_REPEAT:
			if out.pos.x < 0:
				out.pos.x = -out.pos.x
				out.symmetry = Vector2(-1,1)
			elif out.pos.x > 1:
				out.pos.x = 1 - (out.pos.x-1)
				out.symmetry = Vector2(-1,1)
			if out.pos.y < 0:
				out.pos.y = -out.pos.y
				if out.symmetry == null:
					out.symmetry = Vector2(1,1)
				out.symmetry.y = -1
			elif out.pos.y > 1:
				out.pos.y = 1 - (out.pos.y-1)
				if out.symmetry == null:
					out.symmetry = Vector2(1,1)
				out.symmetry.y = -1
		# just block the value at border, no teleportation, no symmetry
		_:
			if out.pos.x < 0:
				out.pos.x = 0
			if out.pos.x > 1:
				out.pos.x = 1
			if out.pos.y < 0:
				out.pos.y = 0
			if out.pos.y > 1:
				out.pos.y = 1
	return out
