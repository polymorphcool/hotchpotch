#tool

extends MultiMeshInstance

export(float) var speed
export(Vector3) var rgb_speed
export(Vector3) var height_speed

export(float,-2,2) var height_max setget set_height_max
export(float,0,1) var height_mix setget set_height_mix
export(float,0,200) var rgb_scale setget set_rgb_scale

onready var rgb_offset = Vector3(0,0,0)
onready var height_offset = Vector3(0,0,0)
onready var elapsed = 0

func set_height_max(f):
	height_max = f
	upadet_heights()

func set_height_mix(f):
	height_mix = f
	upadet_heights()

func set_rgb_scale(f):
	rgb_scale = f
	material_override.set_shader_param( "uv_rgb_scale", Vector3(1,1,1) * rgb_scale )

func upadet_heights():
	material_override.set_shader_param( "height_0", height_max * (1-height_mix) )
	material_override.set_shader_param( "height_1", height_max * height_mix )

func _ready():
	upadet_heights()
	set_rgb_scale(rgb_scale)

func _process(delta):
	
	elapsed += delta
	get_node( "../cam" ).rotation.x = (PI/180) * ( -9.408 + sin( elapsed * speed ) * 3.4 )
	rotation.y -= delta * height_speed.y
	
	rgb_offset += rgb_speed * speed * delta
	height_offset += height_speed * speed * delta
	material_override.set_shader_param( "uv_rgb_offset", rgb_offset )
	material_override.set_shader_param( "uv_height_offset", height_offset )
