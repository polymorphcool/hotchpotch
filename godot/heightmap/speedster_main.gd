extends Spatial

export(float, -2, 2) var _speed setget speed
export(float, -2, 2) var _rgb_speed setget rgb_speed
export(float, -2, 2) var _height_speed setget height_speed
export(float, -1, 1) var _height_max0 setget height_max0
export(float, -1, 1) var _height_max1 setget height_max1
export(Shader) var height_shader setget shader

func speed( f = null ):
	if f == null:
		return _speed
	_speed = f
	$plane.speed = f
	$plane2.speed = f

func rgb_speed( f = null ):
	if f == null:
		return _rgb_speed
	_rgb_speed = f
	$plane.rgb_speed = f
	$plane2.rgb_speed = f
	
func height_speed( f = null ):
	if f == null:
		return _height_speed
	_height_speed = f
	$plane.height_speed = f
	$plane2.height_speed = f

func height_max0( f = null ):
	if f == null:
		return _height_max0
	_height_max0 = f
	$plane.height_max = f

func height_max1( f = null ):
	if f == null:
		return _height_max1
	_height_max1 = f
	$plane2.height_max = f

func shader( s = null ):
	if s == null:
		return $plane.material_override.shader
	$plane.material_override.shader = s

func _ready():
	_speed = $plane.speed
	_rgb_speed = $plane.rgb_speed
	_height_speed = $plane.height_speed
	_height_max0 = $plane.height_max
	_height_max1 = $plane2.height_max
