tool

extends MultiMeshInstance

export (bool) var _reset : bool = false setget reset
export (bool) var _pause : bool = false
export (bool) var _debug : bool = false setget debug
export (bool) var _debug_links : bool = true
export (bool) var _debug_axis : bool = true
export (bool) var _debug_sight : bool = true
export (bool) var _debug_force : bool = true
export (float,0,1000) var generation_radius : float = 3
export (float,0,1000) var expansion_radius : float = 3
export (float,0,1) var speed_min : float = 0.5
export (float,0,1) var speed_max : float = 2
export (float,0,100) var speed_mult : float = 1
export (float,0,100) var delta_mult : float = 1
export (Vector3) var deviation : Vector3 = Vector3(1,0,0)
export (float,0,10) var awarness : float = 0.2
export (float,-1,1) var vision : float = 0
export (float,1,100) var repulsion : float = 5
export (float,0,10) var follow : float = 1

var up : Vector3 = Vector3(0,1,0)
var forward : Vector3 = Vector3(0,0,-1)
var initialised : bool = false
var mm_num : int = -1
var mm : MultiMesh = null
var mm_confs : Array = []

func reset( b : bool ):
	_reset = false
	if b:
		initialised = false

func debug( b : bool ):
	_debug = b
	if $links != null:
		$links.visible = _debug && _debug_links
		$axis.visible = _debug && _debug_axis
		$sight.visible = _debug && _debug_sight
		$force.visible = _debug && _debug_force

func _ready():
	debug(_debug)

func new_config():
	return {
		"transform" : null,
		"force" : Vector3(0,0,0),
		"dir" : null,
		"up" : null,
		"force_decay" : 2,
		"force_sensitivity" : 2,
		"speed" : 1
	}

func random_config():
	var b : Basis = Basis( Vector3( rand_range(-PI,PI),rand_range(-PI,PI),rand_range(-PI,PI) ) )
	var p : Vector3 = Vector3( 
		rand_range(-generation_radius,generation_radius),
		rand_range(-generation_radius,generation_radius),
		rand_range(-generation_radius,generation_radius) )
	var conf : Dictionary = new_config()
	conf.transform = Transform( b, p )
	conf.dir = conf.transform.basis.xform( forward )
	conf.up = conf.transform.basis.xform( up )
	conf.speed = rand_range(speed_min,speed_max)
	return conf

func debug_sight( bid : int ):
	if not _debug:
		return
	var offset : Vector3 = mm_confs[bid].transform.origin
	var fwd : Vector3 = mm_confs[bid].dir
	if vision > 0.999:
		$sight.add_vertex( offset )
		$sight.add_vertex( offset + fwd * awarness )
		return
	var right : Vector3 = mm_confs[bid].up.cross(mm_confs[bid].dir)
	var maxa : float = acos(vision)
	var h : float = -maxa
	var ch : float = cos( h )
	var sh : float = sin( h )
	var stepcount : int = ceil( maxa * 16 / PI )
	var step : float = maxa * 2.0 / stepcount
	if vision > -1:
		$sight.add_vertex( offset )
		$sight.add_vertex( offset + (fwd*ch+right*sh).normalized() * awarness )
	for i in range( 1, stepcount+1 ):
		var a : float = -maxa + step * i
		var ci : float = cos( a )
		var si : float = sin( a )
		$sight.add_vertex( offset + (fwd*ch+right*sh).normalized() * awarness )
		$sight.add_vertex( offset + (fwd*ci+right*si).normalized() * awarness )
		ch = ci
		sh = si
	if vision > -1:
		$sight.add_vertex( offset )
		$sight.add_vertex( offset + (fwd*ch+right*sh).normalized() * awarness )

func border_push( bid : int ):
	var conf : Dictionary = mm_confs[bid]
	# limit repulsion
	var bpush : float = 0
	bpush = expansion_radius - conf.transform.origin.x
	if bpush < 0.3:
		conf.force += Vector3(-1,0,0) / bpush
	bpush = expansion_radius + conf.transform.origin.x
	if bpush < 0.3:
		conf.force += Vector3(1,0,0) / bpush
	bpush = expansion_radius - conf.transform.origin.y
	if bpush < 0.3:
		conf.force += Vector3(0,-1,0) / bpush
	bpush = expansion_radius + conf.transform.origin.y
	if bpush < 0.3:
		conf.force += Vector3(0,1,0) / bpush
	bpush = expansion_radius - conf.transform.origin.z
	if bpush < 0.3:
		conf.force += Vector3(0,0,-1) / bpush
	bpush = expansion_radius + conf.transform.origin.z
	if bpush < 0.3:
		conf.force += Vector3(0,0,1) / bpush

func _process(delta):
	
	if not initialised or ( mm != null and mm_num != mm.instance_count ):
		# checking values
		forward = forward.normalized()
		if speed_max < speed_min:
			var f : float = speed_max
			speed_max = speed_min
			speed_min = f
		mm = self.get_multimesh()
		if mm == null:
			return
		mm_num = mm.instance_count
		mm_confs.resize(mm_num)
		randomize()
		for i in mm_num:
			mm_confs[i] = random_config()
			mm.set_instance_transform( i, mm_confs[i].transform )
			var r : float = rand_range(0,1)
			mm.set_instance_color( i, Color( r, 1-r, rand_range(0.1,0.5) ) )
		initialised = true
	
	if _pause:
		return
	
	if initialised:
		
		var now : int = OS.get_ticks_msec()
		var mdelta : float = delta * delta_mult
		
		if _debug:
			$links.clear()
			$links.begin( Mesh.PRIMITIVE_LINES )
			$axis.clear()
			$axis.begin( Mesh.PRIMITIVE_LINES )
			$sight.clear()
			$sight.begin( Mesh.PRIMITIVE_LINES )
			$force.clear()
			$force.begin( Mesh.PRIMITIVE_LINES )
		
		for i in mm_num:
			var conf : Dictionary = mm_confs[i]
			conf.force += conf.dir * deviation * mdelta * rand_range(-1,1)
#			conf.force += Vector3( 0,-9.81, 0 )
			border_push( i )
			var fl : float = conf.force.length_squared()
			if fl <= 1e-5:
				conf.force *= 0
			elif fl > 0:
				# create a look_at point:
				var la : Vector3 = conf.transform.origin + conf.force
				var nt : Transform = conf.transform.looking_at( la, conf.dir )
				conf.transform.basis = conf.transform.basis.slerp( nt.basis.orthonormalized(), min(1.0,conf.force_sensitivity*mdelta) )
				conf.force -= conf.force * conf.force_decay * mdelta
				debug_sight( i )
				
			conf.dir = conf.transform.basis.xform( forward )
			conf.up = conf.transform.basis.xform( up )
			var translation : Vector3 = conf.dir * conf.speed * speed_mult * mdelta
			conf.transform.origin += translation
			
			if _debug:
				var r : Vector3 = conf.transform.basis.xform(Vector3.RIGHT)
				$axis.add_vertex( conf.transform.origin )
				$axis.add_vertex( conf.transform.origin + r * 0.2 )
				$axis.add_vertex( conf.transform.origin )
				$axis.add_vertex( conf.transform.origin + conf.transform.basis.xform(Vector3.UP) * 0.2 )
				$axis.add_vertex( conf.transform.origin )
				$axis.add_vertex( conf.transform.origin + conf.transform.basis.xform(Vector3.FORWARD) * 0.2 )
				$axis.add_vertex( conf.transform.origin + conf.dir * 0.3 + r * 0.05 )
				$axis.add_vertex( conf.transform.origin + conf.dir * 0.3 - r * 0.05 )
				$axis.add_vertex( conf.transform.origin + conf.dir * 0.3 - r * 0.05 )
				$axis.add_vertex( conf.transform.origin + conf.dir * 0.4 )
				$axis.add_vertex( conf.transform.origin + conf.dir * 0.3 + r * 0.05 )
				$axis.add_vertex( conf.transform.origin + conf.dir * 0.4 )
				$force.add_vertex( conf.transform.origin )
				$force.add_vertex( conf.transform.origin + conf.force )
				debug_sight( i )
			
			var p : Vector3 = conf.transform.origin
			while p.x > expansion_radius:
				p.x -= expansion_radius*2
			while p.x < -expansion_radius:
				p.x += expansion_radius*2
			while p.y > expansion_radius:
				p.y -= expansion_radius*2
			while p.y < -expansion_radius:
				p.y += expansion_radius*2
			while p.z > expansion_radius:
				p.z -= expansion_radius*2
			while p.z < -expansion_radius:
				p.z += expansion_radius*2
			conf.transform.origin = p
			mm.set_instance_transform( i, mm_confs[i].transform )
		
		# this is the brute force approach: test of all boids on boids, so pow(n,2)
		# for 300 boids on a i7 4790k, 4ghz:
		# - average process time is 0.045 ms of this method
		# - average fps is 22 fps
#		for i in mm_num:
#			# avoidance test
#			var conf : Dictionary = mm_confs[i]
#			var p : Vector3 = conf.transform.origin
#			for j in mm_num:
#				if i == j:
#					continue
#				var other : Dictionary = mm_confs[j]
#				var op : Vector3 = other.transform.origin
#				var d : float = op.distance_to( p )
#				if d > awarness:
#					continue
#				# in sight?
#				var dot : float = conf.dir.dot( op )
#				if dot > vision:
#					conf.dir += (op-p).normalized() * (d/awarness) * 0.3
#			conf.dir = conf.dir.normalized()
		for i in range(0,mm_num):
			# avoidance test
			var conf : Dictionary = mm_confs[i]
			var p : Vector3 = conf.transform.origin
			for j in range(i,mm_num):
				var other : Dictionary = mm_confs[j]
				var op : Vector3 = other.transform.origin
				var diff = op - p
				var d : float = op.distance_to( p )
				# is j in sight of i?
				var doti : float = conf.dir.dot( (op-p).normalized() )
				if d <= awarness:
					if mdelta == 0:
						continue
					if _debug:
						$links.add_vertex( p )
						$links.add_vertex( op )
					if doti > vision:
						# pushing on i, so in -diff direction
						# force decrease with distance
						conf.force -= diff * (d/awarness)*repulsion * mdelta
					# is i in sight of j?
					var dotj : float = other.dir.dot( (p-op).normalized() )
					if dotj > vision:
						# pushing on j, so in diff direction
						# force decrease with distance
						other.force += diff * (d/awarness)*repulsion * mdelta
				elif d <= follow and doti > vision:
					# pulling towards the boid in front
					var pc : float = d/follow
					conf.force += diff * pc * mdelta
					$links.add_vertex( p )
					$links.add_vertex( op )
		if _debug:
			$links.end()
			$axis.end()
			$sight.end()
			$force.end()
		var then : int = OS.get_ticks_msec()
		get_node("../ui/swarm_info").text = "swarm processing time: " + str( (then-now)*1.0/1000 )
