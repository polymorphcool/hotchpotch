tool

extends MeshInstance

export (bool) var lock_translation : bool = true

var initialised : bool = false
var step : Vector3 = Vector3()

func _process(delta):
	
	if not initialised or step != self.global_transform.basis.get_scale():
		step = self.global_transform.basis.get_scale()
		initialised = true
	
	if lock_translation:
		var o : Vector3 = self.global_transform.origin
		var vertex_offset : Vector3 = Vector3()
		vertex_offset.x = (int(o.x/step.x) * step.x) - o.x
		vertex_offset.z = (int(o.z/step.z) * step.z) - o.z
		var mat : ShaderMaterial = self.material_override
		mat.set_shader_param( "vertex_offset", vertex_offset / step )
