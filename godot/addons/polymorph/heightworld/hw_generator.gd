tool

extends Spatial

export (int,1,1000) var _mesh_radius : int = 20
export (float,0,10) var _mesh_radius_decay : float = 1
export (int,1,10) var _mesh_LOD : int = 5 setget mesh_LOD
export (bool) var _mesh_quadrans : bool = true
export (float,0,100) var _height : float = 5
export (bool) var _mesh_generate : bool = false setget mesh_generate

export (bool) var _repeat_border : bool = false
export (bool) var _repeat_texture : bool = false
export (bool) var _normal_generate : bool = false setget normal_generate

export (Texture) var _input_height : Texture = null
export (Shader) var _height_shader : Shader = null
export (Texture) var _normal_map : Texture = null

###########################
##### MESH GENERATION #####
###########################

var sqrt2 : float = sqrt(2)
var terrain_script = load( "res://addons/polymorph/heightworld/terrain_constraint.gd" )

func mesh_LOD( i : int ):
	_mesh_LOD = i

func mesh_face( verts : Array, uvs: Array, lvl : int = 0 ):
	var c = (verts[0]+verts[1]+verts[2]+verts[3]) * 0.25
	return {
		"column": -1,
		"row": -1,
		"verts": verts,
		"uvs": uvs,
		"center": c,
		"radius": c.length(),
		"level": lvl,
		"depth": 0,
		"split": [false,false,false,false],
		"subs": null
	}

func mesh_tri( verts : Array, uvs : Array, uv2s : Array, lvl : int, surf : SurfaceTool ):
	for i in range(0,3):
		surf.add_uv( uvs[i] )
		surf.add_uv2( uv2s[i] )
		surf.add_color( Color(float(lvl)/_mesh_LOD,0,0,0) )
		surf.add_vertex( verts[i] )

func mesh_add( f : Dictionary, surf : SurfaceTool, min_lvl : int = -1, max_lvl : int = -1 ):
	if f.subs != null:
		var total = 0
		for s in f.subs:
			total += mesh_add( s, surf, min_lvl, max_lvl )
		return total
	else:
		if min_lvl != -1 and f.level < min_lvl:
			return 0
		if max_lvl != -1 and f.level > max_lvl:
			return 0
		var tang : Plane = Plane(Vector3.UP,0.0)
		var count : int = 0
		if f.split[0] or f.split[1] or f.split[2] or f.split[3]:
			if f.split[0] and not ( f.split[1] or f.split[3] ):
				var midv = f.verts[0] + (f.verts[1]-f.verts[0]) * 0.5
				var miduv = f.uvs[0] + (f.uvs[1]-f.uvs[0]) * 0.5
				mesh_tri( [f.verts[0],midv,f.verts[3]], [f.uvs[0],miduv,f.uvs[3]], [Vector2(0,0),Vector2(0.5,0),Vector2(0,1)], f.level, surf )
				count += 1
				mesh_tri( [midv,f.verts[1],f.verts[2]], [miduv,f.uvs[1],f.uvs[2]], [Vector2(0.5,0),Vector2(1,0),Vector2(1,1)], f.level, surf )
				count += 1
				mesh_tri( [midv,f.verts[2],f.verts[3]], [miduv,f.uvs[2],f.uvs[3]], [Vector2(0.5,0),Vector2(1,1),Vector2(0,1)], f.level, surf )
				count += 1
			elif f.split[2] and not ( f.split[1] or f.split[3] ):
				var midv = f.verts[3] + (f.verts[2]-f.verts[3]) * 0.5
				var miduv = f.uvs[3] + (f.uvs[2]-f.uvs[3]) * 0.5
				mesh_tri( [f.verts[0],midv,f.verts[3]], [f.uvs[0],miduv,f.uvs[3]], [Vector2(0,0),Vector2(0.5,1),Vector2(0,1)], f.level, surf )
				count += 1
				mesh_tri( [midv,f.verts[1],f.verts[2]], [miduv,f.uvs[1],f.uvs[2]], [Vector2(0.5,1),Vector2(1,0),Vector2(1,1)], f.level, surf )
				count += 1
				mesh_tri( [f.verts[0],f.verts[1],midv], [f.uvs[0],f.uvs[1],miduv], [Vector2(0,0),Vector2(1,0),Vector2(0.5,1)], f.level, surf )
				count += 1
			elif f.split[1] and not ( f.split[0] or f.split[2] ):
				var midv = f.verts[1] + (f.verts[2]-f.verts[1]) * 0.5
				var miduv = f.uvs[1] + (f.uvs[2]-f.uvs[1]) * 0.5
				mesh_tri( [f.verts[0],f.verts[1],midv], [f.uvs[0],f.uvs[1],miduv], [Vector2(0,0),Vector2(1,0),Vector2(1,0.5)], f.level, surf )
				count += 1
				mesh_tri( [midv,f.verts[2],f.verts[3]], [miduv,f.uvs[2],f.uvs[3]], [Vector2(1,0.5),Vector2(1,1),Vector2(0,1)], f.level, surf )
				count += 1
				mesh_tri( [f.verts[0],midv,f.verts[3]], [f.uvs[0],miduv,f.uvs[3]], [Vector2(0,0),Vector2(1,0.5),Vector2(0,1)], f.level, surf )
				count += 1
			elif f.split[3] and not ( f.split[0] or f.split[2] ):
				var midv = f.verts[0] + (f.verts[3]-f.verts[0]) * 0.5
				var miduv = f.uvs[0] + (f.uvs[3]-f.uvs[0]) * 0.5
				mesh_tri( [f.verts[0],f.verts[1],midv], [f.uvs[0],f.uvs[1],miduv], [Vector2(0,0),Vector2(1,0),Vector2(0,0.5)], f.level, surf )
				count += 1
				mesh_tri( [midv,f.verts[2],f.verts[3]], [miduv,f.uvs[2],f.uvs[3]], [Vector2(0,0.5),Vector2(1,1),Vector2(0,1)], f.level, surf )
				count += 1
				mesh_tri( [f.verts[1],f.verts[2],midv], [f.uvs[1],f.uvs[2],miduv], [Vector2(1,0),Vector2(1,1),Vector2(0,0.5)], f.level, surf )
				count += 1
			elif f.split[0] and f.split[1]:
				var m0 = f.verts[0] + (f.verts[1]-f.verts[0]) * 0.5
				var u0 = f.uvs[0] + (f.uvs[1]-f.uvs[0]) * 0.5
				var m1 = f.verts[1] + (f.verts[2]-f.verts[1]) * 0.5
				var u1 = f.uvs[1] + (f.uvs[2]-f.uvs[1]) * 0.5
				mesh_tri( [f.verts[0],m0,f.verts[3]], [f.uvs[0],u0,f.uvs[3]], [Vector2(0,0),Vector2(0.5,0),Vector2(0,1)], f.level, surf )
				count += 1
				mesh_tri( [f.verts[3],m1,f.verts[2]], [f.uvs[3],u1,f.uvs[2]], [Vector2(0,1),Vector2(1,0.5),Vector2(1,1)], f.level, surf )
				count += 1
				mesh_tri( [f.verts[3],m0,m1], [f.uvs[3],u0,u1], [Vector2(0,1),Vector2(0.5,0),Vector2(1,0.5)], f.level, surf )
				count += 1
				mesh_tri( [f.verts[1],m1,m0], [f.uvs[1],u1,u0], [Vector2(1,0),Vector2(1,0.5),Vector2(0.5,0)], f.level, surf )
				count += 1
			elif f.split[1] and f.split[2]:
				var m0 = f.verts[1] + (f.verts[2]-f.verts[1]) * 0.5
				var u0 = f.uvs[1] + (f.uvs[2]-f.uvs[1]) * 0.5
				var m1 = f.verts[3] + (f.verts[2]-f.verts[3]) * 0.5
				var u1 = f.uvs[3] + (f.uvs[2]-f.uvs[3]) * 0.5
				mesh_tri( [f.verts[0],f.verts[1],m0], [f.uvs[0],f.uvs[1],u0], [Vector2(0,0),Vector2(1,0),Vector2(1,0.5)], f.level, surf )
				count += 1
				mesh_tri( [f.verts[0],m1,f.verts[3]], [f.uvs[0],u1,f.uvs[3]], [Vector2(0,0),Vector2(0.5,1),Vector2(0,1)], f.level, surf )
				count += 1
				mesh_tri( [f.verts[0],m0,m1], [f.uvs[0],u0,u1], [Vector2(0,0),Vector2(1,0.5),Vector2(0.5,1)], f.level, surf )
				count += 1
				mesh_tri( [f.verts[2],m1,m0], [f.uvs[2],u1,u0], [Vector2(1,1),Vector2(0.5,1),Vector2(1,0.5)], f.level, surf )
				count += 1
			elif f.split[2] and f.split[3]:
				var m0 = f.verts[3] + (f.verts[2]-f.verts[3]) * 0.5
				var u0 = f.uvs[3] + (f.uvs[2]-f.uvs[3]) * 0.5
				var m1 = f.verts[0] + (f.verts[3]-f.verts[0]) * 0.5
				var u1 = f.uvs[0] + (f.uvs[3]-f.uvs[0]) * 0.5
				mesh_tri( [f.verts[0],f.verts[1],m1], [f.uvs[0],f.uvs[1],u1], [Vector2(0,0),Vector2(1,0),Vector2(0,0.5)], f.level, surf )
				count += 1
				mesh_tri( [f.verts[1],f.verts[2],m0], [f.uvs[1],f.uvs[2],u0], [Vector2(1,0),Vector2(1,1),Vector2(0.5,1)], f.level, surf )
				count += 1
				mesh_tri( [f.verts[1],m0,m1], [f.uvs[1],u0,u1], [Vector2(1,0),Vector2(0.5,1),Vector2(0,0.5)], f.level, surf )
				count += 1
				mesh_tri( [f.verts[3],m1,m0], [f.uvs[3],u1,u0], [Vector2(0,1),Vector2(0,0.5),Vector2(0.5,1)], f.level, surf )
				count += 1
			elif f.split[0] and f.split[3]:
				var m0 = f.verts[0] + (f.verts[1]-f.verts[0]) * 0.5
				var u0 = f.uvs[0] + (f.uvs[1]-f.uvs[0]) * 0.5
				var m1 = f.verts[0] + (f.verts[3]-f.verts[0]) * 0.5
				var u1 = f.uvs[0] + (f.uvs[3]-f.uvs[0]) * 0.5
				mesh_tri( [f.verts[1],f.verts[2],m0], [f.uvs[1],f.uvs[2],u0], [Vector2(1,0),Vector2(1,1),Vector2(0.5,0)], f.level, surf )
				count += 1
				mesh_tri( [f.verts[3],m1,f.verts[2]], [f.uvs[3],u1,f.uvs[2]], [Vector2(0,1),Vector2(0,0.5),Vector2(1,1)], f.level, surf )
				count += 1
				mesh_tri( [f.verts[2],m1,m0], [f.uvs[2],u1,u0], [Vector2(1,1),Vector2(0,.5),Vector2(0.5,0)], f.level, surf )
				count += 1
				mesh_tri( [f.verts[0],m0,m1], [f.uvs[0],u0,u1], [Vector2(0,0),Vector2(0.5,0),Vector2(0,.5)], f.level, surf )
				count += 1
		else:
			# part 0
			mesh_tri( [f.verts[0],f.verts[1],f.verts[2]], [f.uvs[0],f.uvs[1],f.uvs[2]], [Vector2(0,0),Vector2(1,0),Vector2(1,1)], f.level, surf )
			count += 1
			# part 1
			mesh_tri( [f.verts[2],f.verts[3],f.verts[0]], [f.uvs[2],f.uvs[3],f.uvs[0]], [Vector2(1,1),Vector2(0,1),Vector2(0,0)], f.level, surf )
			count += 1
#			if (f.center.x > 0 and f.center.z > 0) or (f.center.x < 0 and f.center.z < 0):
#			else:
#				# part 0
#				mesh_tri( [f.verts[0],f.verts[1],f.verts[3]], [f.uvs[0],f.uvs[1],f.uvs[3]], [Vector2(0,0),Vector2(1,0),Vector2(0,1)], f.level, surf )
#				count += 1
#				# part 1
#				mesh_tri( [f.verts[3],f.verts[1],f.verts[2]], [f.uvs[3],f.uvs[1],f.uvs[2]], [Vector2(0,1),Vector2(1,0),Vector2(1,1)], f.level, surf )
#				count += 1
		return count

func mesh_divide( f : Dictionary, radius : float ):
	
	if f.subs != null:
		var add_depth = false
		for s in f.subs:
			if mesh_divide( s, radius ):
				add_depth = true
		if add_depth:
			f.depth += 1
		return add_depth
	elif f.radius <= radius:
		# vertex
		var mid0 = f.verts[0] + ( f.verts[1]-f.verts[0] ) * 0.5
		var mid1 = f.verts[1] + ( f.verts[2]-f.verts[1] ) * 0.5
		var mid2 = f.verts[2] + ( f.verts[3]-f.verts[2] ) * 0.5
		var mid3 = f.verts[3] + ( f.verts[0]-f.verts[3] ) * 0.5
		var c = f.verts[0] + ( f.verts[2]-f.verts[0] ) * 0.5
		# uvs
		var uvm0 = f.uvs[0] + ( f.uvs[1]-f.uvs[0] ) * 0.5
		var uvm1 = f.uvs[1] + ( f.uvs[2]-f.uvs[1] ) * 0.5
		var uvm2 = f.uvs[2] + ( f.uvs[3]-f.uvs[2] ) * 0.5
		var uvm3 = f.uvs[3] + ( f.uvs[0]-f.uvs[3] ) * 0.5
		var uvc = f.uvs[0] + ( f.uvs[2]-f.uvs[0] ) * 0.5
		f.subs = []
		f.subs.resize(4)
		f.subs[0] = mesh_face([ f.verts[0], mid0, c, mid3 ],[ f.uvs[0], uvm0, uvc, uvm3 ], f.level+1)
		f.subs[1] = mesh_face([ mid0, f.verts[1], mid1, c ],[ uvm0, f.uvs[1], uvm1, uvc ], f.level+1)
		f.subs[2] = mesh_face([ c, mid1, f.verts[2], mid2 ],[ uvc, uvm1, f.uvs[2], uvm2 ], f.level+1)
		f.subs[3] = mesh_face([ mid3, c, mid2, f.verts[3] ],[ uvm3, uvc, uvm2, f.uvs[3] ], f.level+1)
		f.depth += 1
		return true
	return false

func mesh_sew_internal( f : Dictionary ):
	if f.subs != null:
		mesh_sew_external(f.subs[0],f.subs[1],1)
		mesh_sew_external(f.subs[0],f.subs[3],2)
		mesh_sew_external(f.subs[1],f.subs[0],3)
		mesh_sew_external(f.subs[1],f.subs[2],2)
		mesh_sew_external(f.subs[2],f.subs[1],0)
		mesh_sew_external(f.subs[2],f.subs[3],3)
		mesh_sew_external(f.subs[3],f.subs[0],0)
		mesh_sew_external(f.subs[3],f.subs[2],1)
		for s in f.subs:
			mesh_sew_internal(s)

func mesh_sew_external( f : Dictionary, other : Dictionary, pos : int ):
	if f.depth == 0 and other.depth > 0:
		f.split[pos] = true
	elif f.depth > 0 and other.depth >= f.depth:
		# we need to go one step lower in the faces
		match pos:
			0:
				mesh_sew_external( f.subs[0], other.subs[3], pos )
				mesh_sew_external( f.subs[1], other.subs[2], pos )
			1:
				mesh_sew_external( f.subs[1], other.subs[0], pos )
				mesh_sew_external( f.subs[2], other.subs[3], pos )
			2:
				mesh_sew_external( f.subs[2], other.subs[1], pos )
				mesh_sew_external( f.subs[3], other.subs[0], pos )
			_:
				mesh_sew_external( f.subs[0], other.subs[1], pos )
				mesh_sew_external( f.subs[3], other.subs[2], pos )

func mesh_generate( b: bool ):
	_mesh_generate = false
	if b:
		# clean previous terrain
		for c in self.get_children():
			if c.name == "terrain":
				remove_child( $terrain )
				break
		
		# mesh generation
		var def : int = (_mesh_radius * 2)
		var offset : Vector3 = Vector3( -_mesh_radius,0,-_mesh_radius )
		var faces : Array = []
		var grid : Array = []
		grid.resize( def * def )
		var fnum: int = 0
		for z in range(0,def):
			var zz : int = z+1
			for x in range(0,def):
				
				var xx : int = x+1
				var pts : Array = [
					Vector3( x,0,z ) + offset,
					Vector3( xx,0,z ) + offset,
					Vector3( xx,0,zz ) + offset,
					Vector3( x,0,zz ) + offset
				]
				var uvs : Array = [
					Vector2( x,z ) / def,
					Vector2( xx,z ) / def,
					Vector2( xx,zz ) / def,
					Vector2( x,zz ) / def
				]
				
				var keepon = true
				for pt in pts:
					if pt.length() > _mesh_radius + sqrt2:
						keepon = false
						break
				
				if !keepon:
					grid[x+z*def] = -1
					continue
				
				var c = (pts[0]+pts[1]+pts[2]+pts[3]) * 0.25
				var f = mesh_face(pts,uvs)
				f.column = x
				f.row = z
				grid[x+z*def] = faces.size()
				faces.append( f )
		
		# tesselation:
		for i in range(1,_mesh_LOD):
			var rad = _mesh_radius / pow(pow(2,i),_mesh_radius_decay)
			for f in faces:
				mesh_divide( f, rad )
		
		# fix gaps
		var id : int = 0
		for f in faces:
			# linking sub-faces together
			mesh_sew_internal( f )
			if f.row > 0:
				id = int( f.column + (f.row-1) * def )
				var above = grid[id]
				if above == -1:
					continue
				var other = faces[above]
				mesh_sew_external(f,other,0)
			if f.column < def-1:
				id = int( (f.column+1) + f.row * def )
				var right = grid[id]
				if right == -1:
					continue
				var other = faces[right]
				mesh_sew_external(f,other,1)
			if f.row < def-1:
				id = int( f.column + (f.row+1) * def )
				var below = grid[id]
				if below == -1:
					continue
				var other = faces[below]
				mesh_sew_external(f,other,2)
			if f.column > 0:
				id = int( (f.column-1) + f.row * def )
				var left = grid[id]
				if left == -1:
					continue
				var other = faces[left]
				mesh_sew_external(f,other,3)
		
		if not _mesh_quadrans:
			# one mesh only
			# generation of meshinstance
			var terrain : MeshInstance = MeshInstance.new()
			terrain.name = "terrain"
			if terrain_script != null:
				terrain.set_script( terrain_script )
			self.add_child( terrain )
			terrain.set_owner(get_tree().edited_scene_root)
			# mesh generation
			var surf : SurfaceTool = SurfaceTool.new()
			surf.begin(Mesh.PRIMITIVE_TRIANGLES)
			for f in faces:
				fnum += mesh_add( f, surf )
			surf.index()
			surf.generate_normals()
			surf.generate_tangents()
			var mesh : ArrayMesh = ArrayMesh.new()
			surf.commit( mesh )
			mesh.custom_aabb = AABB( Vector3(), Vector3(_mesh_radius*2,_height,_mesh_radius*2) )
			terrain.mesh = mesh
		else:
			# generation of 4 meshes
			var terrain : Spatial = Spatial.new()
			terrain.name = "terrain"
			self.add_child( terrain )
			terrain.set_owner(get_tree().edited_scene_root)
			for i in range(0,5):
				var quadran : MeshInstance = MeshInstance.new()
				var bbox : AABB = AABB(Vector3(_mesh_radius,0,_mesh_radius)*0.5,Vector3(_mesh_radius,_height,_mesh_radius))
				match i:
					0:
						quadran.name = "terrain_SE"
					1:
						quadran.name = "terrain_SW"
						bbox.position *= Vector3(-1,0,1)
					2:
						quadran.name = "terrain_NW"
						bbox.position *= Vector3(-1,0,-1)
					3:
						quadran.name = "terrain_NE"
						bbox.position *= Vector3(1,0,-1)
					_:
						quadran.name = "terrain_CENTER"
						bbox.position = Vector3()
				var surf : SurfaceTool = SurfaceTool.new()
				surf.begin(Mesh.PRIMITIVE_TRIANGLES)
				for f in faces:
					match i:
						0:
							if f.center.x > 0 and f.center.z > 0:
								fnum += mesh_add( f, surf, -1, _mesh_LOD - 2 )
						1:
							if f.center.x < 0 and f.center.z > 0:
								fnum += mesh_add( f, surf, -1, _mesh_LOD - 2 )
						2:
							if f.center.x < 0 and f.center.z < 0:
								fnum += mesh_add( f, surf, -1, _mesh_LOD - 2 )
						3:
							if f.center.x > 0 and f.center.z < 0:
								fnum += mesh_add( f, surf, -1, _mesh_LOD - 2 )
						_:
							fnum += mesh_add( f, surf, _mesh_LOD - 1 )
				surf.index()
				surf.generate_normals()
				surf.generate_tangents()
				var mesh : ArrayMesh = ArrayMesh.new()
				surf.commit( mesh )
#				mesh.custom_aabb = bbox
				quadran.mesh = mesh
				if terrain_script != null:
					quadran.set_script( terrain_script )
				terrain.add_child( quadran )
				quadran.set_owner(get_tree().edited_scene_root)
		
		print( "mesh generation success, faces: ", fnum )
		
		if _height_shader != null:
			var mat : ShaderMaterial = ShaderMaterial.new()
			mat.shader = _height_shader
			mat.set_shader_param( "roughness", 1.0 )
			mat.set_shader_param( "albedo", Color(1,1,1,1) )
			mat.set_shader_param( "uv1_scale", Vector3(1,1,1) )
			mat.set_shader_param( "uv2_scale", Vector3(1,1,1) )
			mat.set_shader_param( "world_uv_scale", Vector3(0.02,1,0.02) )
			mat.set_shader_param( "height", _height )
			if _input_height != null:
				mat.set_shader_param( "texture_height", _input_height )
			var terrain = get_node( "terrain" )
			if not _mesh_quadrans:
				terrain.mesh.surface_set_material(0,mat)
				terrain.material_override = mat
			else:
				for c in terrain.get_children():
					c.mesh.surface_set_material(0,mat)
					c.material_override = mat
			terrain.editor_description = "faces: " + str( fnum )
		
			link_normal()

#############################
##### NORMAL GENERATION #####
#############################

var normal_im : Image = null
var normal_ims : Vector2 = Vector2()
var normal_vertices : Array = []

func link_normal():
	var terrain = get_node( "terrain" )
	var tmat : ShaderMaterial = null
	if not _mesh_quadrans:
		tmat = terrain.material_override
	else:
		tmat = terrain.get_child(0).material_override
	tmat.set_shader_param("texture_normal", _normal_map)
	if _repeat_texture:
		tmat.set_shader_param("normal_scale", Vector3(.5,1,.5))
	else:
		tmat.set_shader_param("normal_scale", Vector3(1,1,1))

func c2v( var c : Color ):
	return Vector3( c.r, c.g, c.b )

func v2c( var v : Vector3 ):
	return Color(v.x,v.y,v.z)

func normal_generate( b : bool ):
	
	_normal_generate = false
	
	if b and _input_height != null:
		
		var now = OS.get_ticks_msec()
		
		normal_im = _input_height.get_data()
		if normal_im.is_compressed():
# warning-ignore:return_value_discarded
			normal_im.decompress()
		normal_im.lock()
		normal_ims = normal_im.get_size()
		# adding a border to the grid
		var gridx : int = int(normal_ims.x+2)
		var gridy : int = int(normal_ims.y+2)
		normal_vertices.clear()
		normal_vertices.resize( gridx*gridy )
		print( "normal_vertices: ", normal_vertices.size() )
		# computing vertices
		for y in range(0,normal_ims.y):
			for x in range(0,normal_ims.x):
				var v = Vector3( (x/normal_ims.x)-0.5, c2v(normal_im.get_pixel( int(x), int(y))).x*_height, (y/normal_ims.y)-0.5 )
				# generating ids in the normal grid, not in image
				var id = (x+1)+(y+1)*gridx
				normal_vertices[id] = v
		normal_im.unlock()
		# filling the gaps
		for y in range(0,gridy):
			for x in range(0,gridx):
				var id = x+y*gridx
				if normal_vertices[id] != null:
					continue
				## top left corner
				if x == 0 and y == 0:
					# position:
					var ref : Vector3 = normal_vertices[1+1*gridx]
					if _repeat_border:
						# bottom right pîxel
						ref.y += normal_vertices[(gridx-2)+(gridy-2)*gridx].y
					normal_vertices[id] = ref + Vector3( -1/normal_ims.x, 0, -1/normal_ims.y )
				## top right corner
				elif x == gridx-1 and y == 0:
					var ref : Vector3 = normal_vertices[(gridx-2)+1*gridx]
					if _repeat_border:
						# bottom left pîxel
						ref.y = normal_vertices[1+(gridy-2)*gridx].y
					normal_vertices[id] = ref + Vector3( 1/normal_ims.x, 0, -1/normal_ims.y )
				## bottom right corner
				elif x == gridx-1 and y == gridy-1:
					var ref : Vector3 = normal_vertices[(gridx-2)+(gridy-2)*gridx]
					if _repeat_border:
						# top left pîxel
						ref.y = normal_vertices[1+gridx].y
					normal_vertices[id] = ref + Vector3( 1/normal_ims.x, 0, 1/normal_ims.y )
				## bottom left corner
				elif x == 0 and y == gridy-1:
					var ref : Vector3 = normal_vertices[1+(gridy-2)*gridx]
					if _repeat_border:
						# top right pîxel
						ref.y = normal_vertices[(gridx-2)+gridx].y
					normal_vertices[id] = ref + Vector3( -1/normal_ims.x, 0, 1/normal_ims.y )
				# all corners are solved, we have to process the edges now
				## top edge
				elif y == 0:
					var ref : Vector3 = normal_vertices[x+gridx]
					if _repeat_border:
						# bottom edge
						ref.y = normal_vertices[x+(gridy-2)*gridx].y
					normal_vertices[id] = ref + Vector3( 0, 0, -1/normal_ims.y )
				## bottom edge
				elif y == gridy-1:
					var ref : Vector3 = normal_vertices[x+(gridy-2)*gridx]
					if _repeat_border:
						# top edge
						ref.y = normal_vertices[x+gridx].y
					normal_vertices[id] = ref + Vector3( 0, 0, 1/normal_ims.y )
				## left edge
				elif x == 0:
					var ref : Vector3 = normal_vertices[1+y*gridx]
					if _repeat_border:
						# right edge
						ref.y = normal_vertices[(gridx-2)+y*gridx].y
					normal_vertices[id] = ref + Vector3( -1/normal_ims.x, 0, 0 )
				## right edge
				elif x == gridx-1:
					var ref : Vector3 = normal_vertices[(gridx-2)+y*gridx]
					if _repeat_border:
						# left edge
						ref.y = normal_vertices[1+y*gridx].y
					normal_vertices[id] = ref + Vector3( 1/normal_ims.x, 0, 0 )
				## should NEVER comes here
				else:
					print( "inconsistency in normal_generate, await messy results" )
					normal_vertices[id] = Vector3()
		
		# adding standard vertices
		var surf : SurfaceTool = SurfaceTool.new()
		surf.begin(Mesh.PRIMITIVE_TRIANGLES)
		surf.add_smooth_group(true)
		for y in range(0,gridy):
			for x in range(0,gridx):
				var id : int = int(x+y*gridx)
				surf.add_uv( Vector2(normal_vertices[id].x,normal_vertices[id].z) + Vector2(0.5,0.5) )
				surf.add_vertex( normal_vertices[id] * Vector3(10,1,10) )
		
		# registering standard faces
		for y in range(0,gridy-1):
			for x in range(0,gridx-1):
				var id0 : int = int(x+y*gridx)
				var id1 : int = int((x+1)+y*gridx)
				var id2 : int = int((x+1)+(y+1)*gridx)
				var id3 : int = int(x+(y+1)*gridx)
				surf.add_index(id0)
				surf.add_index(id1)
				surf.add_index(id2)
				surf.add_index(id2)
				surf.add_index(id3)
				surf.add_index(id0)
		
		surf.generate_normals()
		surf.generate_tangents()
		var mesh : Mesh = Mesh.new()
# warning-ignore:return_value_discarded
		surf.commit( mesh )
		surf.clear()
		surf.unreference()
		var arr : Array = mesh.surface_get_arrays(0)
		var norms : PoolVector3Array = arr[Mesh.ARRAY_NORMAL]
		print( "Mesh.ARRAY_NORMAL: ", norms.size() )
		# encoding normals in a map
		var normal : Image = Image.new()
		if _repeat_texture:
			# we will create a 2x2 version of the map with flipped normals
			normal.create( int(normal_ims.x*2), int(normal_ims.y*2), false, Image.FORMAT_RGBF )
			normal.lock()
			for y in range(0,normal_ims.y):
				for x in range(0,normal_ims.x):
					var id : int = int((x+1)+(y+2)*gridx)
					var norm : Vector3 = norms[id]
					var n : Vector3 = Vector3( (1+norm.x)*.5, 1 - (1+norm.z)*0.5, norm.y )
					# standard normal
					normal.set_pixel( int(x), int(y), Color(n.x,n.y,n.z) )
					# x flipped
					normal.set_pixel( int((normal_ims.x*2-1)-x), int(y), Color(1.0-n.x,n.y,n.z) )
					# y flipped
					normal.set_pixel( int(x), int((normal_ims.y*2-1)-y), Color(n.x,1.0-n.y,n.z) )
					# x & y flipped
					normal.set_pixel( int((normal_ims.x*2-1)-x), int((normal_ims.y*2-1)-y), Color(1.0-n.x,1.0-n.y,n.z) )
			normal.unlock()
		else:
			normal.create( int(normal_ims.x), int(normal_ims.y), false, Image.FORMAT_RGBF )
			normal.lock()
			for y in range(0,normal_ims.y):
				for x in range(0,normal_ims.x):
					var id : int = int((x+1)+(y+1)*gridx)
					var norm : Vector3 = norms[id]
					var n : Vector3 = Vector3( (1+norm.x)*.5, 1 - (1+norm.z)*0.5, norm.y )
					normal.set_pixel( x, y, Color(n.x,n.y,n.z) )
			normal.unlock()
		
#		$plane.mesh = mesh
		var tex : ImageTexture = ImageTexture.new()
		tex.create_from_image( normal )
		_normal_map = tex
		
		link_normal()
