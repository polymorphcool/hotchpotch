extends Camera

export (NodePath) var _terrain : NodePath = ""
export (float,0,20) var _speed : float = 2

var dir : Vector3 = Vector3(0,0,-1)
var roty : float = 0
var terrain : Spatial = null
var rot_keys : Array = [false,false]
var alt_keys : Array = [false,false]

var height : float = 0
var new_height : float = 0

func _ready():
	if not _terrain.is_empty():
		terrain = get_node( _terrain )
		height = terrain.get_child(0).material_override.get_shader_param("height")
		new_height = height
	roty = self.rotation.y
	dir = dir.rotated( Vector3.FORWARD, roty )

func _input(event):
	
	if event is InputEventKey and event.scancode == KEY_RIGHT:
		rot_keys[1] = event.is_pressed()
	if event is InputEventKey and event.scancode == KEY_LEFT:
		rot_keys[0] = event.is_pressed()
	if event is InputEventKey and event.scancode == KEY_UP:
		alt_keys[0] = event.is_pressed()
	if event is InputEventKey and event.scancode == KEY_DOWN:
		alt_keys[1] = event.is_pressed()
	if event is InputEventKey and event.scancode == KEY_H and event.is_pressed():
		randomize()
		new_height = rand_range( 0, 10 )
	
func _process(delta):
	if rot_keys[0]:
		self.rotation.y += 2 * delta
	if rot_keys[1]:
		self.rotation.y -= 2 * delta
	if alt_keys[0]:
		self.translation.y += 10 * delta
	if alt_keys[1]:
		self.translation.y -= 10 * delta
	dir = Vector3.FORWARD.rotated( Vector3.UP, self.rotation.y )
	self.translation += dir * _speed
	height += ( new_height - height ) * 10 * delta
	terrain.get_child(0).material_override.set_shader_param("height",height)
	if not terrain == null:
		terrain.global_transform.origin.x = self.global_transform.origin.x
		terrain.global_transform.origin.z = self.global_transform.origin.z
