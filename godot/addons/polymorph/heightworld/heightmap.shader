shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;
uniform vec4 albedo : hint_color;

uniform sampler2D texture_albedo : hint_albedo;
uniform sampler2D texture_normal : hint_normal;

uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform float point_size : hint_range(0,128);

uniform vec3 uv1_scale = vec3(1.0);
uniform vec3 uv1_offset = vec3(0.0);
uniform vec3 uv2_scale = vec3(1.0);
uniform vec3 uv2_offset = vec3(0.0);
uniform vec3 normal_scale = vec3(1.0);

uniform sampler2D texture_height : hint_albedo;
uniform vec3 world_uv_scale = vec3(1.0);
uniform vec3 world_uv_offset = vec3(0.0);
uniform float height = 1.0;

uniform vec3 vertex_offset = vec3(0.0);

varying vec3 world_uv;

float get_depth( vec2 uv ) {
	return texture(texture_height,uv).x * height;
}

void vertex() {
	UV=UV*uv1_scale.xy+uv1_offset.xy;
	UV2=UV2*uv2_scale.xy+uv2_offset.xy;
	VERTEX += vertex_offset;
	world_uv = ( WORLD_MATRIX * vec4(VERTEX.xyz,1.0)).xyz * world_uv_scale + world_uv_offset;
	VERTEX.y += get_depth( world_uv.xz );
}

void fragment() {
	vec2 base_uv = UV;
	vec4 albedo_tex = texture(texture_albedo,world_uv.xz);
//	vec4 albedo_tex = texture(texture_height,UV);
	ALBEDO = albedo.rgb * albedo_tex.rgb;
	METALLIC = metallic;
	ROUGHNESS = roughness;
	SPECULAR = specular;
	NORMALMAP = texture(texture_normal,(world_uv*normal_scale).xz).rgb;
	NORMALMAP_DEPTH = 1.0;
}
