tool

extends SpotLight

export (bool) var _vibrate : bool = true setget vibrate
export (NodePath) var _target : NodePath  = "" setget target

var initialised : bool = false
var fixed_translation : Vector3 = Vector3()
var target_node : Spatial = null

func vibrate( b : bool ):
	_vibrate = b
	initialised = false

func target( np : NodePath ):
	_target = np
	target_node = null

func _process(delta):
	if _vibrate:
		if not initialised:
			fixed_translation = translation
			initialised = true
		translation = fixed_translation + Vector3(rand_range(-0.001,0.001),0,0)
	if _target != "":
		if target_node == null:
			target_node = get_node( _target )
		if target_node != null:
			self.look_at( target_node.global_transform.origin, Vector3.UP )
