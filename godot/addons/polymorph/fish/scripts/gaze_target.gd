tool

extends MeshInstance

export (float,0,1) var radius : float = 0.8
export (float,0,10) var oscillation : float = 0.4
export (float,0,10) var rot_speed : float = 1
export (float,0,10) var osc_speed : float = 0.3

var rot_timer : float = 0
var osc_timer : float = 0

func _ready():
	pass # Replace with function body.

func _process(delta):
	rot_timer += rot_speed * delta
	osc_timer += osc_speed * delta
	self.translation = Vector3( cos(rot_timer)*radius,sin(osc_timer)*oscillation,sin(rot_timer)*radius )
	self.look_at( self.translation * 2, Vector3.UP )
