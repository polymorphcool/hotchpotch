extends Spatial

export (NodePath) var reference : NodePath = ""

export (float,-1,1000) var inside : float = 1.0
export (float,0,1000) var near : float = 2.0
export (float,-1,1000) var far : float = 5.0
export (bool) var info : bool = true
export (Vector2) var info_offset : Vector2 = Vector2(0.55,0.1)

var obj : Spatial = null
var use_shadermat : bool = false
var use_spatialmat : bool = false
var on_screen : bool = true
var albedo : Color = Color()
var floorp_init : bool = true
var floorp : Vector2 = Vector2()
var panel2d_alpha : float = 0

func _ready():
	if reference == "" or get_node( reference ) == null:
		return
	obj = get_node( reference )
	# making material unique
	var m = $panel3d.material_override.duplicate()
	$panel3d.material_override = m
	if m is ShaderMaterial:
		albedo = m.get_shader_param( "albedo_color" )
		use_shadermat = true
	elif m is SpatialMaterial:
		albedo = m.albedo_color
		use_spatialmat = true
	on_screen = $vnotifier.is_on_screen()
	$info.visible = false

func _process(delta):
	
	if obj == null:
		return
	
	# moving the landmark to tracked object world position
	global_transform.origin = obj.global_transform.origin
	
	if get_viewport().get_camera() != null:
		
		var c : Camera = get_viewport().get_camera()
		# getting distance to camera
		var d : float = ( global_transform.origin - c.global_transform.origin ).length()
		var vps : Vector2 = get_viewport().size
		
		if on_screen:
			var alpha : float = 0
			if d < inside or ( far >= 0 and d > far ):
				alpha = 0.0
			elif d >= near and far < 0:
				alpha = 1.0
			else:
				if d < near:
					alpha = pow((d-inside)/(near-inside),2)
				else:
					alpha = pow(1.0-(d-near)/(far-near),2)
			if alpha <= 0:
				$panel3d.visible = false
			else:
				$panel3d.visible = true
				albedo.a = pow(alpha,2)
				if use_shadermat:
					$panel3d.material_override.set_shader_param( "albedo_color", albedo )
				if use_spatialmat:
					$panel3d.material_override.albedo_color = albedo
			if info:
				var off : Vector3 = c.global_transform.basis.xform(Vector3(info_offset.x,info_offset.y,0))
				var scp : Vector2 = c.unproject_position(global_transform.origin+off)
				$info.rect_position = scp
				$info.visible = true
				$info.align = Label.ALIGN_LEFT
				$info.text = self.name + '\n'
				$info.text += 'inside: ' + str(inside) + '\n'
				$info.text += 'near: ' + str(near) + '\n'
				$info.text += 'far: ' + str(far) + '\n'
				$info.text += 'distance: ' + str(d) + '\n'
				if $info.rect_position.x + $info.rect_size.x >= vps.x:
					scp = c.unproject_position(global_transform.origin-off)
					$info.rect_position.x = scp.x - ($info.rect_size.x + info_offset.x)
					$info.align = Label.ALIGN_RIGHT
				albedo.a = pow(alpha,0.5)
				$info.set( "custom_colors/font_color", albedo )

		if not on_screen:
			if info:
				$info.visible = false
			panel2d_alpha = 1
			var proj : Vector3 = c.to_local( global_transform.origin )
			var new_fp = Vector2(proj.x, -proj.y)
			if floorp_init:
				floorp = new_fp
				floorp_init = false
			else:
				floorp += ( new_fp - floorp ) * min(1,(4*delta))
			var a = floorp.angle()
			$panel2d.position = vps*.5 + Vector2(
				max(vps.x*-.5,min(vps.x*.5,cos(a)*(vps.y/vps.x)*vps.x)),
				max(vps.y*-.5,min(vps.y*.5,sin(a)*vps.y)))
			$panel2d.rotation = a + PI*.5
		else:
			panel2d_alpha = 0
			
	
	if panel2d_alpha != $panel2d.modulate.a:
		$panel2d.modulate.a += ( panel2d_alpha - $panel2d.modulate.a ) * 5 * delta
		if abs( panel2d_alpha - $panel2d.modulate.a ) < 1e-4:
			$panel2d.modulate.a = panel2d_alpha
		if $panel2d.modulate.a == 0:
			$panel2d.visible = false
			floorp_init = true
		else:
			$panel2d.visible = true

func _on_cam_entered(camera):
	on_screen = true

func _on_cam_exited(camera):
	on_screen = false
