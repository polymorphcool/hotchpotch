extends MeshInstance

export (float,0,1000) var revolution_time : float = 200
export (float,-3.16,3.16) var tilt : float = 0.1

var radius : float = 0
var plane : Basis = Basis()
var time : float = 0

func _ready():
	radius = translation.length()
	plane = Basis( Vector3.RIGHT, tilt )

func _process(delta):
	time += delta
	var a = ( time / revolution_time ) * TAU
	translation = plane.xform( Vector3( cos(a),0,sin(a) ) * radius )
