shader_type canvas_item;
render_mode blend_mix;

uniform float alpha : hint_range(0.0,1.0);

void fragment() {
	COLOR = texture( TEXTURE, UV ) * vec4(1.0,1.0,1.0,alpha);
}
