tool

extends MeshInstance

export (bool) var _randomise_time : bool = false setget randomise_time
export (float) var _frequency : float = 5.0 setget frequency
export (float) var _amplitude : float = 0.082 setget amplitude
export (float) var _zmult : float = 2.968 setget zmult
export (float) var _x_phase_shift : float = 1.622 setget x_phase_shift
export (float) var _xmin : float = 0.0 setget xmin
export (float) var _xmax : float = 0.29 setget xmax
export (float) var _zmin : float = -0.198 setget zmin
export (float) var _zmax : float = 0.689 setget zmax

func randomise_time( b : bool ):
	_randomise_time = false
	if b:
		randomize()
		self.material_override.set_shader_param( "time_offset", rand_range(0,TAU) )

func frequency( f : float ):
	_frequency = f
	self.material_override.set_shader_param( "frequency", _frequency )
func amplitude( f : float ):
	_amplitude = f
	self.material_override.set_shader_param( "amplitude", _amplitude )
func zmult( f : float ):
	_zmult = f
	self.material_override.set_shader_param( "zmult", _zmult )
func x_phase_shift( f : float ):
	_x_phase_shift = f
	self.material_override.set_shader_param( "x_phase_shift", _x_phase_shift )
func xmin( f : float ):
	_xmin = f
	self.material_override.set_shader_param( "xmin", _xmin )
func xmax( f : float ):
	_xmax = f
	self.material_override.set_shader_param( "xmax", _xmax )
func zmin( f : float ):
	_zmin = f
	self.material_override.set_shader_param( "zmin", _zmin )
func zmax( f : float ):
	_zmax = f
	self.material_override.set_shader_param( "zmax", _zmax )
