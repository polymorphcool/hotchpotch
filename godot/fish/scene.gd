extends Spatial

var arrows = [false,false,false,false]

func _ready():
	pass

func _input(event):
	
	if event is InputEventKey and event.scancode == KEY_RIGHT:
		arrows[1] = event.pressed
	elif event is InputEventKey and event.scancode == KEY_LEFT:
		arrows[3] = event.pressed
	elif event is InputEventKey and event.scancode == KEY_UP:
		arrows[0] = event.pressed
	elif event is InputEventKey and event.scancode == KEY_DOWN:
		arrows[2] = event.pressed
		

func _process(delta):
	
	if arrows[1]:
		$fish.yaw( $fish.yaw() - delta * 2 )
	if arrows[3]:
		$fish.yaw( $fish.yaw() + delta * 2 )
	if arrows[0]:
		$fish.pitch( $fish.pitch() + delta )
	if arrows[2]:
		$fish.pitch( $fish.pitch() - delta )
	
	$fish.translation += $fish.direction * $fish.speed * 5 * delta
