shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_front,diffuse_burley,specular_schlick_ggx;

// whale params
uniform float frequency;
uniform float amplitude;
uniform float xmult;
uniform float xpow;
uniform float zmult;
uniform vec2 uvstretch_min;
uniform vec2 uvstretch_max;

// standard params
uniform float alpha_scissor_threshold;
uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform float point_size : hint_range(0,128);
uniform sampler2D texture_metallic : hint_white;
uniform vec4 metallic_texture_channel;
uniform sampler2D texture_roughness : hint_white;
uniform vec4 roughness_texture_channel;
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform vec3 uv2_scale;
uniform vec3 uv2_offset;

void vertex() {
	
	vec3 new_pos = VERTEX;
	float vx = pow( abs(VERTEX.x), xpow ) * xmult;
	float vz = VERTEX.z * zmult;
	float ystretch = sin( ( TIME * frequency - ( vx + vz ) ) );
	new_pos.y += ystretch * amplitude;
	
	// UV
	UV=vec2( VERTEX.y, VERTEX.z ) * uvstretch_min + (1.0 + sin( TIME * frequency - vz ) )  * 0.5 * ( uvstretch_max - uvstretch_min );
	UV=UV*uv1_scale.xy+uv1_offset.xy;
	
	VERTEX = new_pos;
	
}

void fragment() {
	vec2 base_uv = UV;
	vec4 albedo_tex = texture(texture_albedo,base_uv);
	ALBEDO = albedo.rgb * albedo_tex.rgb;
	float metallic_tex = dot(texture(texture_metallic,base_uv),metallic_texture_channel);
	METALLIC = metallic_tex * metallic;
	float roughness_tex = dot(texture(texture_roughness,base_uv),roughness_texture_channel);
	ROUGHNESS = roughness_tex * roughness;
	SPECULAR = specular;
	ALPHA = albedo.a * albedo_tex.a;
	ALPHA_SCISSOR=alpha_scissor_threshold;
}
