shader_type canvas_item;
render_mode blend_mix;

uniform sampler2D texture_mix0 : hint_white;
uniform sampler2D texture_mix1 : hint_black;
uniform sampler2D depth_map : hint_white;
uniform float mixer = 1.0;

void fragment() {
//	vec2 t = vec2(TIME*0.01);
//	while(t.x > 1.0) { t.x-= 1.0; }
//	while(t.y > 1.0) { t.y-= 1.0; }
	vec4 c = texture( TEXTURE, SCREEN_UV );
	vec4 d = texture( depth_map, SCREEN_UV );
//	COLOR = vec4( (c.xyz * d.xyz), d.w * max(0.6,c.w));
	vec4 c0 = texture( texture_mix0, SCREEN_UV );
	float m = max( mixer, c0.a );
	COLOR = mix( c0, texture( texture_mix1, SCREEN_UV ), m );
}