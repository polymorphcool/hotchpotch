tool

extends Sprite

var initialised : bool = false
var angle : float = 0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if !initialised:
		var tex = $vp0.get_texture()
		self.material.set_shader_param( "texture_mix0", tex )
		tex = $vp1.get_texture()
		self.material.set_shader_param( "texture_mix1", tex )
		initialised = true
	self.material.set_shader_param( "mixer", (1.0+sin(angle))*0.5 )
	angle += delta
