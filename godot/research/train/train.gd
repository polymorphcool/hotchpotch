extends Spatial

export (float,0,20) var speed = 4

var wagons = []
var turning = [false,false]

func _ready():
	for c in get_children():
		if c.name.begins_with( "wagon" ):
			var w = {
				'node': c,
				'front': c.get_node("anchor_front").translation,
				'back': c.get_node("anchor_back").translation 
			}
	pass

func _input(event):
	if event is InputEventKey:
		if event.scancode == KEY_LEFT:
			turning[0] = event.pressed
		if event.scancode == KEY_RIGHT:
			turning[1] = event.pressed

func _process(delta):
	if turning[0] or turning[1]:
		if turning[0]:
			if $tractor.rotation.y < 0.2:
				$tractor.rotation.y += (0.2 - $tractor.rotation.y) * delta * 5
		if turning[1]:
			if $tractor.rotation.y > -0.2:
				$tractor.rotation.y += (-0.2 - $tractor.rotation.y) * delta * 5
	else:
		$tractor.rotation.y -= $tractor.rotation.y * delta * 5
