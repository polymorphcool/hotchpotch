tool

extends Spatial

export (Array) var configurations = []
export (bool) var store = false setget do_store
export (bool) var previous = false setget do_previous
export (bool) var next = false setget do_next

func do_store(b):
	if not b:
		return
	var dict = {
			"curvature" : $shaded.material_override.get_shader_param( "curvature" ),
			"curve_size" : $shaded.material_override.get_shader_param( "curve_size" ),
			"curve_offset" : $shaded.material_override.get_shader_param( "curve_offset" ),
			"curve_accel" : $shaded.material_override.get_shader_param( "curve_accel" ),
			"inflate" : $shaded.material_override.get_shader_param( "inflate" )
		}
	configurations.append(dict)

func do_previous( b ):
	if not b:
		return
	target_conf += ( len(configurations) - 1 )
	target_conf %= len(configurations)
	pc = 0

func do_next( b ):
	if not b:
		return
	target_conf += 1
	target_conf %= len(configurations)
	pc = 0

onready var current_conf = 0
onready var target_conf = 0
onready var pc = 0

func mix_configuration():
	var c0 = configurations[ current_conf ]
	var c1 = configurations[ target_conf ]
	var m = (1+sin(-PI*0.5 + pc*PI)) / 2
	var mi = 1 - m
	var mat = $shaded.material_override
	for k in c0:
		mat.set_shader_param( k, c0[k] * mi + c1[k] * m )
	$spot.translation.x = -3.914 + rand_range(-0.001,0.001)

func _process(delta):
	if current_conf != target_conf:
		pc += delta * 0.7
		if pc >= 1:
			pc = 1
			current_conf = target_conf
		mix_configuration()
