tool

extends Line2D

export (float,1,200) var size = 10 setget set_size
export (int,1,100) var definition = 1 setget set_defnition
export (float,-3,3) var curve_x = 0 setget set_curve_x
export (float,-3,3) var curve_y = 0 setget set_curve_y

func set_curve_x( f ):
	curve_x = f

func set_curve_y( f ):
	curve_y = f

func set_size( s ):
	size = s

func set_defnition( i ):
	definition = i

func _process(delta):
	generate()

func generate():
	
	var pts = PoolVector2Array()
	var gap = ( 1.0 / definition ) * size
	var start = Vector2( -0.5, -0.5 ) * size

	for y in range(0,definition+1):
		for x in range(0,definition+1):
			pts.append( start + Vector2( x * gap, y * gap ) )
	
	var pi = 0
	var offset = $center.position
	
	if curve_x != 0:
		pi = 0
		var radius = size / curve_x
		for p in pts:
			var angl = ( p.y + offset.y ) / radius
			var r = radius + p.x
			p.x = radius - ( cos( angl ) * r )
			p.y = ( sin( angl ) * r )
			pts.set( pi, p )
			pi += 1
	
	if curve_y != 0:
		pi = 0
		var radius = size / curve_y
		for p in pts:
			var angl = ( p.x + offset.x ) / radius
			var r = radius + p.y
			p.y = radius - ( cos( angl ) * r )
			p.x = ( sin( angl ) * r )
			pts.set( pi, p )
			pi += 1
	
	self.points = pts
