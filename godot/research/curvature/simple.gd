extends Spatial

onready var fx = get_node("/root/fx")

export (float) var _cam_rot = 0 setget cam_rot
export (float) var _cam_rotor = 0 setget cam_rotor
export (float) var _cam_fov = 25 setget cam_fov
export (float) var _cam_dist = 0 setget cam_dist

export (float) var _cx = 0 setget curvature_x
export (float) var _cy = 0 setget curvature_y
export (float) var _cz = 0 setget curvature_z
export (float) var _ax = 0 setget accel_x
export (float) var _ay = 0 setget accel_y
export (float) var _az = 0 setget accel_z
export (float) var _inf = 0 setget inflate
export (float) var _scratches = 0.035 setget scratches
export(Shader) var _fish_shader setget fish_shader

func cam_rot( f = null ):
	if f == null:
		if rot_y != null:
			return rot_y.target
		return _cam_rot
	_cam_rot = f
	if rot_y != null:
		rot_y.target = _cam_rot

func cam_rotor( f = null ):
	if f == null:
		return _cam_rotor
	_cam_rotor = f

func cam_fov( f = null ):
	if f == null:
		return _cam_fov
	_cam_fov = f
	$cam_pivot/cam.fov = f

func cam_dist( f = null ):
	if f == null:
		return _cam_dist
	_cam_dist = f
	$cam_pivot/cam.translation = $cam_pivot/cam.translation.normalized() * _cam_dist

func curvature_x( f = null ):
	if f == null:
		return _cx
	_cx = f
	update_curvature()
func curvature_y( f = null ):
	if f == null:
		return _cy
	_cy = f
	update_curvature()
func curvature_z( f = null ):
	if f == null:
		return _cz
	_cz = f
	update_curvature()

func accel_x( f = null ):
	if f == null:
		return _ax
	_ax = f
	update_accel()
func accel_y( f = null ):
	if f == null:
		return _ay
	_ay = f
	update_accel()
func accel_z( f = null ):
	if f == null:
		return _az
	_az = f
	update_accel()

func inflate( f = null ):
	if f == null:
		return _inf
	_inf = f
	if fxinf != null:
		fxinf.target = f

func scratches( f= null ):
	if f  == null:
		return $fish.material_override.get_shader_param( "uv2_scale" ).z
	_scratches = f
	var uv2sc = $fish.material_override.get_shader_param( "uv2_scale" )
	uv2sc.z = _scratches
	$fish.material_override.set_shader_param( "uv2_scale", uv2sc )

func fish_shader( s = null ):
	if s == null:
		return _fish_shader
	_fish_shader = s
	$fish.material_override.shader = s

func update_curvature():
	$fish.material_override.set_shader_param( "curvature", Vector3( _cx,_cy,_cz ) )
func update_accel():
	$fish.material_override.set_shader_param( "curve_accel", Vector3( _ax,_ay,_az ) )
func update_inflate():
	if fxinf != null:
		$fish.material_override.set_shader_param( "inflate", Vector3( fxinf.current,fxinf.current,fxinf.current ) )

var curvature = null
var accel = null
var rot_y = null
var rot_accum = null
var fxinf = null

func _ready():
	_fish_shader = $fish.material_override.shader
	curvature = $fish.material_override.get_shader_param( "curvature" )
	_cx = curvature.x
	_cy = curvature.y
	_cz = curvature.z
	accel = $fish.material_override.get_shader_param( "curve_accel" )
	_ax = accel.x
	_ay = accel.y
	_az = accel.z
	_inf = $fish.material_override.get_shader_param( "inflate" ).x
	_cam_dist = $cam_pivot/cam.translation.length()
	_cam_rotor = 0
	# fx
	rot_y = fx.fx_real($cam_pivot.rotation.y)
	fx.fx_add(rot_y, fx.FX_TYPE._real_slingshot)
	rot_accum = fx.fx_real(0,30)
	rot_accum.delta = 1e-5
	fx.fx_add(rot_accum, fx.FX_TYPE._real_linear)
	fxinf = fx.fx_real(0, 20)
	fxinf.target = _inf
	fx.fx_add(fxinf, fx.FX_TYPE._real_slingshot)

func _process(delta):
	
	$spot.shadow_enabled = false
	rot_accum.target += _cam_rotor * delta
	fx.update( rot_accum, delta )
	fx.update( rot_y, delta )
	$cam_pivot.rotation.y = rot_y.current + rot_accum.current
	if fx.update( fxinf, delta ):
		update_inflate()
	$spot.shadow_enabled = true
