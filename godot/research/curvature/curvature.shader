shader_type spatial;
render_mode blend_mix,depth_draw_alpha_prepass,cull_back,diffuse_burley,specular_schlick_ggx;//,unshaded;

uniform vec3 curvature;
uniform vec3 curve_size;
uniform vec3 curve_offset;
uniform vec3 curve_accel;
uniform vec3 inflate;
uniform sampler2D inflate_x : hint_white;
uniform sampler2D inflate_y : hint_white;
uniform sampler2D inflate_z : hint_white;
uniform vec3 vertex_offset;
uniform vec3 vertex_scale;

uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;
uniform sampler2D texture_normal : hint_normal;
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform float point_size : hint_range(0,128);
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform vec3 uv2_scale;
uniform vec3 uv2_offset;

void vertex() {
	vec3 vtrx = VERTEX * vertex_scale + vertex_offset;
	mat3 normrot = mat3( vec3(1.0,0.0,0.0), vec3(0.0,1.0,0.0), vec3(0.0,0.0,1.0) );
	// avoiding divisions by 0
	if ( curvature.x != 0.0 && curve_size.x != 0.0 ) {
		// radius proportional to curvature
		float radius = curve_size.x / curvature.x;
		// size of the arc / radius => angle
		float angl = ( VERTEX.z - curve_offset.x ) / radius;
		float r = curve_accel.x + radius - VERTEX.x;
		float ac = cos( angl );
		float as = sin( angl );
		VERTEX.x = radius - ( ac * r ) + curve_accel.x;
		VERTEX.z = ( as * r ) + curve_offset.x;
		normrot *= mat3(
			vec3(ac,	0.0,	as), 
			vec3(0.0,	1.0,	0.0), 
			vec3(-as,	0.0,	ac) );
	}
	// avoiding divisions by 0
	if ( curvature.y != 0.0 && curve_size.y != 0.0 ) {
		// radius proportional to curvature
		float radius = curve_size.y / curvature.y;
		// size of the arc / radius => angle
		float angl = ( VERTEX.z - curve_offset.y ) / radius;
		float r = curve_accel.y + radius - VERTEX.y;
		float ac = cos( angl );
		float as = sin( angl );
		VERTEX.y = radius - ( ac * r ) + curve_accel.y;
		VERTEX.z = ( as * r ) + curve_offset.y;
		normrot *= mat3(
			vec3(1.0,	0.0,		0.0), 
			vec3(0.0,	ac,		as), 
			vec3(0.0,	-as,	ac) );
	}
	// avoiding divisions by 0
	if ( curvature.z != 0.0 && curve_size.z != 0.0 ) {
		// radius proportional to curvature
		float radius = curve_size.z / curvature.z;
		float angl = ( VERTEX.x + curve_offset.z ) / radius;
		// size of the arc / radius => angle
		float r = curve_accel.z + radius - VERTEX.z;
		float ac = cos( angl );
		float as = sin( angl );
		VERTEX.x = ( as * r ) + curve_offset.z;
		VERTEX.z = radius - ( ac * r ) + curve_accel.z;
		normrot *= mat3(
			vec3(ac,	0.0,	as), 
			vec3(0.0,	1.0,	0.0), 
			vec3(-as,	0.0,	ac) );
	}
	NORMAL *= normrot;
	UV=UV*uv1_scale.xy+uv1_offset.xy;
	UV2=UV*uv2_scale.xy+uv2_offset.xy;
	vec3 inflate_mult = vec3(
		texture(inflate_x,vec2(vtrx.z,0.5)).r,
		texture(inflate_y,vec2(vtrx.z,0.5)).r,
		texture(inflate_z,vec2(vtrx.z,0.5)).r
	);
	VERTEX += normalize(VERTEX) * inflate_mult * inflate;
	NORMAL += (VERTEX*normrot) * inflate_mult * inflate * dot(NORMAL,VERTEX);
}

void fragment() {
	vec2 base_uv = UV;
	vec4 albedo_tex = texture(texture_albedo,base_uv);
	ALBEDO = albedo.rgb * albedo_tex.rgb;
	METALLIC = metallic;
	ROUGHNESS = roughness;
	SPECULAR = specular;
	NORMALMAP = texture(texture_normal,UV2).rgb;
	NORMALMAP_DEPTH = uv2_scale.z;
}
