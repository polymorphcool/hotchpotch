tool

extends ImmediateGeometry

enum DISPLAY_MODE {
	POINT,
	QUAD
}

export (DISPLAY_MODE) var display:int = 0 setget set_display
export (float,0,1) var quad_size:float = 0.3 setget set_quad_size

func set_display(i:int):
	display = i
	initialised = false

func set_quad_size(f:float):
	quad_size = f
	initialised = false

var initialised:bool = false
var new_cloud:bool = true
var cloud:Array = []

func _process(delta):
	if !initialised:
		initialised = true
		if new_cloud:
			cloud=[]
			for i in range(0,1000):
				randomize()
				cloud.append( [ 
					Vector3(rand_range(-10,10),rand_range(-10,10),rand_range(-10,10)),
					Color(rand_range(0.0,1.0),rand_range(0.0,1.0),rand_range(0.0,0.5))
					] )
			new_cloud = false
		clear()
		if display == DISPLAY_MODE.QUAD:
			begin(Mesh.PRIMITIVE_TRIANGLES)
			for c in cloud:
				var co:Color = c[1]
				var po:Vector3 = c[0]
				set_color( co )
				add_vertex( po - (Vector3.UP+Vector3.LEFT) * quad_size * 0.5 ) 
				set_color( co )
				add_vertex( po - (Vector3.UP+Vector3.RIGHT) * quad_size * 0.5 ) 
				set_color( co )
				add_vertex( po - (Vector3.DOWN+Vector3.RIGHT) * quad_size * 0.5 )
				set_color( co )
				add_vertex( po - (Vector3.DOWN+Vector3.RIGHT) * quad_size * 0.5 )
				set_color( co )
				add_vertex( po - (Vector3.DOWN+Vector3.LEFT) * quad_size * 0.5 )
				set_color( co )
				add_vertex( po - (Vector3.UP+Vector3.LEFT) * quad_size * 0.5 )
			end()
		else:
			begin(Mesh.PRIMITIVE_POINTS)
			for c in cloud:
				set_color( c[1] )
				add_vertex( c[0] )
			end()
