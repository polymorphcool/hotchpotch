tool

extends Camera

export (float,-20,20) var min_z = 8
export (float,-20,20) var max_z = 8
export (float,-20,5) var speed = 1

var accum = 0

func _process(delta):
	accum += delta
	translation.z = min_z + (1+sin(accum))*0.5 * (max_z-min_z)
