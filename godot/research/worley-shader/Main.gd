extends Spatial

var angle = 0

func raymarch( s = null ):
	if s == null:
		return $TestCube.material_override.shader
	$TestCube.material_override.shader = s

func _process(delta):
	angle += delta;

func _ready():
	$max_distance.value = $Worley_texture.max_distance * 100.0
	$cloud_density.value = $TestCube.material_override.get_shader_param("cloud_density_factor")
	$light_density.value = $TestCube.material_override.get_shader_param("light_density_factor")

func _on_max_distance_value_changed(value):
	$Worley_texture.max_distance = value / 100.01

func _on_cloud_density_value_changed(value):
	$TestCube.material_override.set_shader_param("cloud_density_factor", value)

func _on_light_density_value_changed(value):
	$TestCube.material_override.set_shader_param("light_density_factor", value)
