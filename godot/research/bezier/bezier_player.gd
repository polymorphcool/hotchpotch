tool

extends Spatial

export (Array) var data = null
export (Array) var points = null
export (Array) var segments = null
export (float) var length = 0

func position_at_ratio( pc ):
	return position_at_distance( pc*length )

func position_at_distance( d ):
	if points == null or segments == null:
		return null
	while d > length:
		d -= length
	while d < 0:
		d += length
	var crrtd = 0
	var snum = segments.size()
	for i in range(0,snum):
		var seg = segments[i]
		if crrtd + seg.length > d:
			var start = points[seg.start]
			var end = points[seg.end]
			#let's merge!
			var pc = ( d - seg.distance ) / seg.length
			var pci = 1-pc
			var b0 = Basis( start.left, start.up, start.front )
			var b1 = Basis( end.left, end.up, end.front )
			var out = {
				'pos': start.pos * pci + end.pos * pc,
				'mat': b0.slerp(b1, pc)
			}
			return out
		crrtd += seg.length
	return null
