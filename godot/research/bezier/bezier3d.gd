tool

extends MeshInstance

export (int,1,200) var _sampling = 10 setget sampling
export (float,0,3) var _width = 1
export (bool) var _close = true setget close
export (bool) var _smart_uv = true setget smart_uv
export (Vector2) var _uv_scale = Vector2(1,1) setget uv_scale
export (bool) var _generate = false setget generate

export (Array) var data = null
export (Array) var points = null
export (Array) var segments = null
export (float) var length = 0

func sampling( i ):
	_sampling = i
	compute()

func close( b ):
	_close = b
	compute()

func smart_uv( b ):
	_smart_uv = b
	compute()

func uv_scale( v2 ):
	_uv_scale = v2
	compute()

func generate( b ):
	_generate = b
	compute()

func sample( start, end, percent ):
	var pci = (1-percent)
	var mid00 = start.pos + (start.handler0-start.pos) * percent
	var mid01 = end.pos + (end.handler1-end.pos)* pci
	var mid02 = start.handler0 + (end.handler1-start.handler0) * percent
	var mid10 = mid00 + ( mid02 - mid00 ) * percent
	var mid11 = mid01 + ( mid02 - mid01 ) * pci
	var mid12 = mid10 + ( mid11 - mid10 ) * percent
	return to_local(mid12)

func add_point( v3, tilt, rad, dataid ):
	var dnum = data.size()
	if not points.empty():
		var add = true
		var swapid = false
		if (points[ points.size()-1 ].pos-v3).length() < 1e-5:
			add = false
			swapid = true
		elif points.size() == dnum * _sampling:
			add = false
			swapid = true
		if add:
			points.append( { 
				'pos': v3, 
				'tilt': tilt,
				'radius': rad,
				'left': null, # will be generated in segments
				'up': null, # will be generated in segments 
				'front': null, # will be generated in segments
				'dataid' : dataid } )
		elif swapid:
			points[ points.size()-1 ].dataid = dataid
	else:
		points.append( { 
			'pos': v3, 
			'tilt': tilt,
			'radius': rad,
			'left': null, # will be generated in segments
			'up': null, # will be generated in segments
			'front': null, # will be generated in segments
			'dataid' : dataid } )

func add_segment( id0, id1 ):
	var pt0 = points[id0]
	var pt1 = points[id1]
	var diff = pt1.pos - pt0.pos
	var seg = {
		'start': id0,
		'end': id1,
		'vec': diff,
		'length': diff.length(),
		'distance': length
	}
	pt0.front = diff.normalized()
	var basis = Basis(Vector3(0,1,0).cross(pt0.front).normalized(),Vector3(0,1,0),pt0.front).rotated( pt0.front, pt0.tilt )
	pt0.left = basis.xform( Vector3(1,0,0) ).normalized()
	pt0.up = pt0.front.cross(pt0.left).normalized()
	length += seg.length
	segments.append( seg )

func add_tri( st, pts, norms, uvs, part ):
	match part:
		0:
			st.add_uv( uvs[0] )
			st.add_normal( norms[0] )
			st.add_vertex( pts[0] )
			st.add_uv( uvs[1] )
			st.add_normal( norms[1] )
			st.add_vertex( pts[1] )
			st.add_uv( uvs[2] )
			st.add_normal( norms[2] )
			st.add_vertex( pts[2] )
		_:
			st.add_uv( uvs[2] )
			st.add_normal( norms[2] )
			st.add_vertex( pts[2] )
			st.add_uv( uvs[3] )
			st.add_normal( norms[3] )
			st.add_vertex( pts[3] )
			st.add_uv( uvs[0] )
			st.add_normal( norms[0] )
			st.add_vertex( pts[0] )

func add_quad( st, pts, norms, uvs ):
	add_tri( st, pts, norms, uvs, 0 )
	add_tri( st, pts, norms, uvs, 1 )

func compute():
	if get_node( "controls" ) == null:
		return
	
	# collecting data
	data = []
	for c in $controls.get_children():
		if c.has_method("get_data") and c.visible:
			data.append( c.get_data() )
	
	# points generation
	points = []
	var dnum = data.size()
	for di in range(0,dnum):
		if di == dnum -1 and not _close:
			break
		var start = data[di]
		var end = data[(di+1)%dnum]
		var gap = 1.0 / _sampling
		for i in range(0,_sampling+1):
			var pc = gap * i
			var pci = (1-pc)
			var pos = sample( start, end, pc )
			var s = (1+sin(-PI*0.5+pc*PI)) * 0.5
			var tilt = ( start.tilt * (1-s) + end.tilt * s )
			var rad = ( start.radius * (1-s) + end.radius * s )
			add_point( pos, tilt, rad, di )
	var pnum = points.size()
	# reseting length
	length = 0
	# segments generation
	segments = []
	for i in range(1,pnum):
		add_segment( i-1, i )
	if _close:
		add_segment( pnum-1, 0 )
	else:
		# synching last point with previous one
		points[pnum-1].left = points[pnum-2].left
		points[pnum-1].up = points[pnum-2].up
		points[pnum-1].front = points[pnum-2].front
	var snum = segments.size()
	
	# mesh generation
	if _generate:
		var st = SurfaceTool.new()
		st.begin( Mesh.PRIMITIVE_TRIANGLES )
		for i in range(0,snum):
			
			var seg = segments[i]
			var nextseg = segments[(i+1)%snum]
			var p0 = points[seg.start]
			var p1 = points[seg.end]
			
			var pt0 = p0.pos - p0.left * _width * 0.5 * p0.radius
			var uv0 = Vector2(1,1)
			var pt1 = p0.pos + p0.left * _width * 0.5 * p0.radius
			var uv1 = Vector2(0,1)
			var pt2 = p1.pos + p1.left * _width * 0.5 * p1.radius
			var uv2 = Vector2(0,0)
			var pt3 = p1.pos - p1.left * _width * 0.5 * p1.radius
			var uv3 = Vector2(1,0)
			
			if _smart_uv:
				var diff = 1 - p0.radius
				uv0.x -= diff * 0.5
				uv1.x = diff * 0.5
				diff = 1 - p1.radius
				uv3.x -= diff * 0.5
				uv2.x = diff * 0.5
				var d0 = seg.distance
				var d1 = nextseg.distance
				if d1 == 0:
					d1 = length
				uv0.y = length - d0
				uv1.y = length - d0
				uv2.y = length - d1
				uv3.y = length - d1
			
			uv0 *= _uv_scale
			uv1 *= _uv_scale
			uv2 *= _uv_scale
			uv3 *= _uv_scale
			
			add_quad( 
				st, 
				[pt0,pt1,pt2,pt3], 
				[p0.up,p0.up,p1.up,p1.up], 
				[uv0,uv1,uv2,uv3] )
		
		st.generate_tangents()
		mesh = st.commit()
	else:
		mesh = null
	
	# updating bezier players
	for c in get_children():
		if c.has_method( "position_at_distance" ):
			c.data = data.duplicate(true)
			c.points = points.duplicate(true)
			c.segments = segments.duplicate(true)
			c.length = length
	
	# updating debugs
	$debug/path.clear()
	$debug/path.begin( Mesh.PRIMITIVE_LINES )
	for i in range(1,pnum):
		$debug/path.add_vertex( points[i-1].pos )
		$debug/path.add_vertex( points[i].pos )
		if _close and i == pnum-1:
			$debug/path.add_vertex( points[i].pos )
			$debug/path.add_vertex( points[0].pos )
	$debug/path.end()

	$debug/lefts.clear()
	$debug/lefts.begin( Mesh.PRIMITIVE_LINES )
	for p in points:
		$debug/lefts.add_vertex( p.pos )
		$debug/lefts.add_vertex( p.pos + p.left )
	$debug/lefts.end()
	
	$debug/ups.clear()
	$debug/ups.begin( Mesh.PRIMITIVE_LINES )
	for p in points:
		$debug/ups.add_vertex( p.pos )
		$debug/ups.add_vertex( p.pos + p.up )
	$debug/ups.end()
	
	$debug/fronts.clear()
	$debug/fronts.begin( Mesh.PRIMITIVE_LINES )
	for p in points:
		$debug/fronts.add_vertex( p.pos )
		$debug/fronts.add_vertex( p.pos + p.front )
	$debug/fronts.end()
	
	print( "bezier curve updated" )

func _process(delta):
	for c in $controls.get_children():
		if c.has_method("get_data"):
			if c.refresh:
				compute()
				break
