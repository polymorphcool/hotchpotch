tool

extends MeshInstance

export (float,0,50) var speed
export (bool) var _play = true setget play
export (Vector3) var translation_offset = Vector3()

var distance = 0
var bplayer = null

func play(b):
	_play = b
	if not b:
		transform.basis = Basis()

func _ready():
	pass # Replace with function body.

func _process(delta):
	if not _play:
		return
	if bplayer == null:
		bplayer = get_node("../bezier_player")
	distance += speed * delta
	if distance > bplayer.length:
		distance -= bplayer.length
	var newp = bplayer.position_at_distance( distance )
	if newp != null:
		transform.basis = newp.mat
		translation = newp.pos + newp.mat.xform(translation_offset)
	var wagon_gap = -0.6
	var current_gap = wagon_gap
	for c in get_children():
		if c.name.begins_with( "wagon" ):
			newp = bplayer.position_at_distance( distance + current_gap )
			if newp != null:
				c.global_transform.basis = newp.mat
				c.global_transform.origin = newp.pos + newp.mat.xform(translation_offset)
			current_gap += wagon_gap
