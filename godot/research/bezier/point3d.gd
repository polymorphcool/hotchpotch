tool

extends MeshInstance

export (bool) var aligned = true
export (bool) var _reset = false setget reset
export (float,-10,10) var tilt = 0
export (float,0,10) var radius = 1
export (float,1,10) var axis_size = 2

var initialised = false
var prevvis = null
var prevtilt = null
var prevradius = null
var prevpos = null
var left = null
var up = null
var front = null
var refresh = false

func get_data():
	return {
		'pos': global_transform.origin,
		'handler0': $handler0.global_transform.origin,
		'handler1': $handler1.global_transform.origin,
		'tilt': tilt,
		'radius': radius
	}

func reset( b ):
	_reset = false
	if b:
		rotation = Vector3(0,0,0)
		$handler0.translation = Vector3(1,0,0)
		$handler1.translation = Vector3(-1,0,0)

func update_axis():
	up = Vector3( 0,1,0 )
	front = $handler0.translation.normalized()
	left = up.cross(front).normalized()
	up = front.cross( left ).normalized()

func update_debug():
	
	$line.clear()
	$line.begin( Mesh.PRIMITIVE_LINES )
	$line.add_vertex( $handler0.translation )
	$line.add_vertex( Vector3(0,0,0) )
	$line.add_vertex( Vector3(0,0,0) )
	$line.add_vertex( $handler1.translation )
	$line.end()
	
	$axis/x.clear()
	$axis/x.begin( Mesh.PRIMITIVE_LINES )
	$axis/x.add_vertex( Vector3(0,0,0) )
	$axis/x.add_vertex( left * axis_size )
	$axis/x.add_vertex( left * axis_size )
	$axis/x.add_vertex( (left * 0.85 - front * 0.1) * axis_size )
	$axis/x.add_vertex( left * axis_size )
	$axis/x.add_vertex( (left * 0.85 + front * 0.1) * axis_size )
	$axis/x.end()
	
	$axis/y.clear()
	$axis/y.begin( Mesh.PRIMITIVE_LINES )
	$axis/y.add_vertex( Vector3(0,0,0) )
	$axis/y.add_vertex( up * axis_size )
	$axis/y.add_vertex( up * axis_size )
	$axis/y.add_vertex( (up * 0.85 - front * 0.1) * axis_size )
	$axis/y.add_vertex( up * axis_size )
	$axis/y.add_vertex( (up * 0.85 + front * 0.1) * axis_size )
	$axis/y.end()
	
	$axis/z.clear()
	$axis/z.begin( Mesh.PRIMITIVE_LINES )
	$axis/z.add_vertex( Vector3(0,0,0) )
	$axis/z.add_vertex( front * axis_size )
	$axis/z.add_vertex( front * axis_size )
	$axis/z.add_vertex( (front * 0.85 - left * 0.1) * axis_size )
	$axis/z.add_vertex( front * axis_size )
	$axis/z.add_vertex( (front * 0.85 + left * 0.1) * axis_size )
	$axis/z.end()

func _process(delta):
	
	if not initialised:
		prevpos = []
		prevpos.append( global_transform )
		prevpos.append( $handler0.global_transform.origin )
		prevpos.append( $handler1.global_transform.origin )
		emit_signal("bezier_update")
		update_axis()
		update_debug()
		initialised = true
		refresh = true
	else:
		refresh = false
		if prevvis != visible:
			refresh = true
		if prevtilt != tilt:
			refresh = true
		if prevradius != radius:
			refresh = true
		if prevpos[1] != $handler0.global_transform.origin:
			refresh = true
			if aligned:
				var d = $handler1.translation.length()
				$handler1.translation = $handler0.translation.normalized() * -d
		elif prevpos[2] != $handler1.global_transform.origin:
			refresh = true
			if aligned:
				var d = $handler0.translation.length()
				$handler0.translation = $handler1.translation.normalized() * -d
		elif prevpos[0] != global_transform:
			refresh = true
		if refresh:
			prevvis = visible
			prevtilt = tilt
			prevradius = radius
			prevpos[0] = global_transform
			prevpos[1] = $handler0.global_transform.origin
			prevpos[2] = $handler1.global_transform.origin
			update_axis()
			update_debug()
