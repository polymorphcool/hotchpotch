tool

extends Polygon2D

export (bool) var continuous = true setget set_continuous

var anchor0 = null
var anchor1 = null
var prev_position = null

func set_continuous( b ):
	if continuous != b:
		var pts = []
		for i in range(0,polygon.size()):
			pts.append( polygon[i].rotated( PI * 0.25 ) )
		polygon = pts
	continuous = b

func update():
	if anchor0 == null and anchor1 == null:
		return
	$line.clear_points()
	if anchor0 != null:
		$line.add_point( to_local(anchor0.global_position) )
	$line.add_point( Vector2(0,0) )
	if anchor1 != null:
		$line.add_point( to_local(anchor1.global_position) )

func get_data():
	var out = {
		'back': null,
		'pos': global_position,
		'front': null,
		'continuous': continuous
	}
	if anchor0 != null:
		out.back = anchor0.global_position
	else:
		out.back = out.pos
	if anchor1 != null:
		out.front = anchor1.global_position
	else:
		out.front = out.pos
	return out

func _process(delta):
	if prev_position != position:
		if prev_position != null:
			var diff =  position - prev_position
			if anchor0 != null:
				anchor0.position += diff
			if anchor1 != null:
				anchor1.position += diff
		prev_position = position
