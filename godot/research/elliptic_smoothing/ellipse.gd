tool

extends Line2D

export (float,0,100) var vertical_radius = 30
export (float,0,100) var horizontal_radius = 30
export (int,2,200) var sampling = 30
export (bool) var _generate = false setget generate

func generate(b):
	if b:
		clear_points()
		var gap = TAU / sampling
		var a = 0
		for i in range( 0, sampling + 1 ):
			add_point( Vector2( 
				cos(a) * vertical_radius, 
				sin(a) * horizontal_radius 
				) )
			a += gap
