extends Polygon2D

export (float,0,100) var speed = 10
export (float,0,1000) var distance = 0

func _process(delta):
	distance += delta * speed
	var bezier = get_parent()
	while( distance > bezier.length ):
		distance -= bezier.length
	var d = bezier.position_ratio( distance / bezier.length )
	position = d.pos
	rotation = d.rot
	
