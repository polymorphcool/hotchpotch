# sampling:
# * https://en.wikipedia.org/wiki/De_Casteljau%27s_algorithm#Geometric_interpretation
# computation of length:
# * https://malczak.linuxpl.com/blog/quadratic-bezier-curve-length/

extends Node2D

export (int,1,200) var sampling = 5
export (bool) var close_curve = true
export (float,0,200) var traveller_speed = 20

var control_points = []
var points = []
var segments = []
var length = 0
var aabb = { 'min': null, 'max': null, 'size': null, 'center': null }

func _ready():
	for c in get_children():
		if c.has_method( "get_data" ):
			control_points.append( {'node': c, 'data': null} )
			c.connect("point_moved", self, "update")
	update()

func get_point( start, end, percent ):
	var pci = (1-percent)
	var far0 = start.pos + start.front
	var far1 = end.pos + end.back
	var mid00 = start.pos + start.front * percent
	var mid01 = end.pos + end.back * pci
	var mid02 = far0 + (far1-far0) * percent
	var mid10 = mid00 + ( mid02 - mid00 ) * percent
	var mid11 = mid01 + ( mid02 - mid01 ) * pci
	var mid12 = mid10 + ( mid11 - mid10 ) * percent
	return mid12

func add_point( v2, continuous, pid = -1 ):
	if pid == -1:
		points.append( { 'pos': v2, 'continuous': continuous } )
	elif pid < points.size():
		points[pid].pos = v2
		points[pid].continuous = continuous

func sample( pt0, pt1 ):
	
	var gap = 1.0 / sampling
	var pc = 0
	var previous_p = null
	var continuous = pt0.data.continuous
# warning-ignore:unused_variable
	for i in range( 0, sampling + 1 ):
		var p = get_point( pt0.data, pt1.data, pc ) - global_position
		if aabb.min == null:
			aabb.min = p
		else:
			if aabb.min.x > p.x:
				aabb.min.x = p.x
			if aabb.min.y > p.y:
				aabb.min.y = p.y
		if aabb.max == null:
			aabb.max = p
		else:
			if aabb.max.x < p.x:
				aabb.max.x = p.x
			if aabb.max.y < p.y:
				aabb.max.y = p.y
		# avoiding duplicates
		var ptnum = points.size()
		if ptnum > 0:
			var prev = points[ ptnum-1 ]
			if (prev.pos-p).length() > 1e-5:
				add_point( p, continuous )
			else:
				add_point( p, continuous, ptnum-1 )
		else:
			add_point( p, continuous )
		pc += gap
		continuous = true
		
func update():
	points.clear()
	#$line.clear_points()
	aabb.min = null
	aabb.max = null
	var prev_cpt = null
	for cpt in control_points:
		cpt.data = cpt.node.get_data()
		# sample segment
		if prev_cpt != null:
			sample( prev_cpt, cpt )
		prev_cpt = cpt
	if close_curve and control_points.size() > 1:
		sample( control_points[ control_points.size()-1 ], control_points[0] )
	# updating display:
	$line.clear_points()
	for p in points:
		$line.add_point( p.pos )
	# generating segments
	segments.clear()
	length = 0
	var ptnum = points.size()
	for i in range(0,ptnum):
		var pt0 = points[ i ]
		var pt1 = points[ (i+1)%ptnum ]
		var diff = (pt1.pos-pt0.pos)
		if diff.length() > 1e-5:
			var seg = {
				'pos': pt0.pos,
				'continuous': pt0.continuous,
				'vec': diff,
				'dir': diff.normalized(),
				'length': diff.length(),
				'distance': length
			}
			segments.append( seg )
			length += seg.length
	aabb.size = aabb.max - aabb.min
	aabb.center = aabb.min + aabb.size * 0.5
	if $bbox.visible:
		$bbox.clear_points()
		$bbox.add_point( aabb.min )
		$bbox.add_point( aabb.min + aabb.size * Vector2(1,0) )
		$bbox.add_point( aabb.max )
		$bbox.add_point( aabb.min + aabb.size * Vector2(0,1) )
		$bbox.add_point( aabb.min )
	if $info.visible:
		$info.rect_position = Vector2( aabb.max.x + 5, aabb.min.y )
		$info.text = "aabb:\n"
		$info.text += "\tpos: "+str(aabb.min.x)+", "+str(aabb.min.y)+"\n"
		$info.text += "\tsize: "+str(aabb.size.x)+", "+str(aabb.size.y)+"\n"
		$info.text += "segments: "+str(segments.size())+"\n"
		$info.text += "length: "+str(length)+"\n"

func position_ratio( pc ):
	var tgrt = pc * length
	var crt = 0
	var segi = 0
	var segnum = segments.size()
	for seg in segments:
		if crt + seg.length > tgrt:
			var subpc = ( tgrt - seg.distance ) / seg.length
			var nexts = segments[ (segi+1)%segnum]
			var out = { 
				'pos': seg.pos + (seg.vec) * subpc,
				'rot': (seg.dir * (1-subpc) + nexts.dir * subpc).angle()
			}
			if not nexts.continuous:
				out.rot = seg.dir.angle()
			return out
		crt += seg.length
		segi += 1
	return null

func _process(delta):
	pass
