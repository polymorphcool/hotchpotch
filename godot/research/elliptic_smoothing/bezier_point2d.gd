extends Node2D

signal point_moved

export (Color) var color = Color(0,1,1)
export (Color) var hover_color = Color(1,1,1)
export (Color) var anchor0_color = Color(0,1,0)
export (Color) var anchor1_color = Color(1,0,1)
export (bool) var align_anchors = true

var angle = 0
var a0 = Vector2(30,0)
var a1 = Vector2(-30,0)
var initialised = false
var inside = null
var mouse_offset = null

func mouse_in():
	$rect.color = hover_color
	inside = $rect
func mouse_in_a0():
	$anchor0.color = hover_color
	inside = $anchor0
func mouse_in_a1():
	$anchor1.color = hover_color
	inside = $anchor1
func mouse_out():
	$rect.color = color
	$anchor0.color = anchor0_color
	$anchor1.color = anchor1_color
	inside = null

func get_data():
	return {
		'back': $anchor0.rect_position + $anchor0.rect_size * 0.5,
		'pos': global_position,
		'front': $anchor1.rect_position + $anchor1.rect_size * 0.5,
		'continuous': align_anchors
	}

func update_anchors():
	if align_anchors and inside != null:
		var other_a = $anchor0
		if inside == $anchor0:
			other_a = $anchor1
		var l = (other_a.rect_position + other_a.rect_size * 0.5).length()
		var dir = (inside.rect_position + inside.rect_size * 0.5) * -1
		other_a.rect_position = dir.normalized() * l - other_a.rect_size * 0.5
	$line.clear_points()
	$line.add_point( $anchor0.rect_position + $anchor0.rect_size * 0.5 )
	$line.add_point( Vector2(0,0) )
	$line.add_point( $anchor1.rect_position + $anchor1.rect_size * 0.5 )

func _input(event):
	if inside == null:
		return
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		if event.pressed:
			if inside == $rect:
				mouse_offset = event.position - global_position
			else:
				mouse_offset = event.position - (global_position+inside.rect_position)
		else:
			mouse_offset = null
	elif event is InputEventMouseMotion and mouse_offset != null:
		emit_signal( "point_moved" )

func _ready():
	
	# applying rotation
	a0 = a0.rotated( rotation )
	a1 = a1.rotated( rotation )
	rotation = 0
	
	$rect.color = color
	if not align_anchors:
		$rect.rect_rotation = PI * 0.25
	$anchor0.color = anchor0_color
	$anchor1.color = anchor1_color
	$anchor0.rect_position = a0 - $anchor0.rect_size * 0.5
	$anchor1.rect_position = a1 - $anchor1.rect_size * 0.5
	$rect.connect("mouse_entered",self,"mouse_in")
	$rect.connect("mouse_exited",self,"mouse_out")
	$anchor0.connect("mouse_entered",self,"mouse_in_a0")
	$anchor0.connect("mouse_exited",self,"mouse_out")
	$anchor1.connect("mouse_entered",self,"mouse_in_a1")
	$anchor1.connect("mouse_exited",self,"mouse_out")
	update_anchors()

func _process(delta):
	if mouse_offset != null:
		var mp = get_viewport().get_mouse_position() - mouse_offset
		if inside == $rect:
			global_position = mp
		else:
			inside.rect_position = mp - global_position
			update_anchors()
