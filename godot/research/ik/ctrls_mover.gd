tool

extends MeshInstance

export (bool) var _animate_children : bool = false setget animate_children
export (float,0,10) var speed : float = 1
export (float,0,10) var x_offset : float = 0
export (float,0,10) var x_mult : float = 1
export (float,0,10) var y_mult : float = 1

var angle = 0

func animate_children( b : bool ):
	_animate_children = b
	if !b: 
		for c in get_children():
			c.translation.y = 0
		angle = 0

func _process(delta):
	
	if _animate_children:
		angle += delta * speed
		for c in get_children():
			var a = angle + x_offset + ( c.translation.x + c.translation.z ) * x_mult
			var s = sin(a)
			if s >= 0:
				c.translation.y = s * y_mult
