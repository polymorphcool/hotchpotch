tool
extends Spatial

export (NodePath) var src : NodePath = ""
export (int,0,2) var axis : int = 1

var source = null

func straighten_basis( src_basis : Basis, axis : int ):
	
	var x = Vector3(1,0,0)
	var y = Vector3(0,1,0)
	var z = Vector3(0,0,1)
	var plane = Plane()
	match axis:
		0:
			plane.x = src_basis[0].x
			plane.y = src_basis[0].y
			plane.z = src_basis[0].z
		1:
			plane.x = src_basis[1].x
			plane.y = src_basis[1].y
			plane.z = src_basis[1].z
		_:
			plane.x = src_basis[2].x
			plane.y = src_basis[2].y
			plane.z = src_basis[2].z
	
	var dotx = x.dot( src_basis[0] )
	var doty = y.dot( src_basis[1] )
	var dotz = z.dot( src_basis[2] )
	
	match axis:
		
		1:
			if abs(doty) > 1e-5:
				# standard case for a Z projection: Y axis is not perpendicular to world Y
				# meaning we can cast world Z onto XZ plane
				z = plane.intersects_ray( Vector3(0,0,1), Vector3(0,1,0) )
				if z == null:
					z = plane.intersects_ray( Vector3(0,0,1), Vector3(0,-1,0) )
				if z != null:
					# if we reprocess X & Z
					z = z.normalized()
					# never negative
					if z.dot( src_basis[2] ) < 0:
						z *= -1
					x = src_basis[1].cross(z)
					y = z.cross(x).normalized()
					x = y.cross(z).normalized()
			else:
				# a solution exists in the YZ world plane
				z = plane.intersects_ray( Vector3(0,1,0), Vector3(0,0,1) )
				if z == null:
					z = plane.intersects_ray( Vector3(0,1,0), Vector3(0,0,-1) )
				# if Y is pointing in opposite direction
				if z != null:
					# might be tricky: z could be inverted!
					if z.dot( src_basis[2] ) < 0:
						z *= -1
					# if we reprocess X & Z
					z = z.normalized()
					x = src_basis[1].cross(z)
					y = z.cross(x).normalized()
					x = y.cross(z).normalized()
	
	return Basis(x,y,z)

func get_delta( from : Basis, to : Basis, axis : int ):
	match axis:
		1:
			var r = acos( from[2].dot( to[2] ) )
			if to[0].dot( Vector3.FORWARD ) < 0:
				r += PI * .5
			if from[0].dot( to[2] ) < 0:
				r *= -1
			return r * 180 / PI
		_:
			return 0.0

func _process(delta):
	
	if source == null and src != null and src != "":
		source = get_node(src)
	if source == null:
		return
#	var src_basis : Basis = source.global_transform.basis
#	var x = Vector3(1,0,0)
#	var y = Vector3(0,1,0)
#	var z = Vector3(0,0,1)
#	var plane = Plane()
#	plane.x = src_basis[1].x
#	plane.y = src_basis[1].y
#	plane.z = src_basis[1].z
#	var dotx = x.dot( src_basis[0] )
#	var doty = y.dot( src_basis[1] )
#	var dotz = z.dot( src_basis[2] )
#	if abs(doty) > 1e-5:
#		# standard case for a Z projection: Y axis is not perpendicular to world Y
#		# meaning we can cast world Z onto XZ plane
#		z = plane.intersects_ray( Vector3(0,0,1), Vector3(0,1,0) )
#		if z == null:
#			z = plane.intersects_ray( Vector3(0,0,1), Vector3(0,-1,0) )
#		if z != null:
#			# if we reprocess X & Z
#			z = z.normalized()
#			# never negative
#			if z.dot( src_basis[2] ) < 0:
#				z *= -1
#			x = src_basis[1].cross(z)
#			y = z.cross(x).normalized()
#			x = y.cross(z).normalized()
#	else:
#		# a solution exists in the YZ world plane
#		z = plane.intersects_ray( Vector3(0,1,0), Vector3(0,0,1) )
#		if z == null:
#			z = plane.intersects_ray( Vector3(0,1,0), Vector3(0,0,-1) )
#		# if Y is pointing in opposite direction
#		if z != null:
#			# might be tricky: z could be inverted!
#			if z.dot( src_basis[2] ) < 0:
#				z *= -1
#			# if we reprocess X & Z
#			z = z.normalized()
#			x = src_basis[1].cross(z)
#			y = z.cross(x).normalized()
#			x = y.cross(z).normalized()
	
	# analysis
	var src_basis : Basis = source.global_transform.basis
	var straightb = straighten_basis( src_basis, axis )
	match axis:
		1:
			print( straightb[2].dot( src_basis[2] ) )
			print( get_delta( straightb, src_basis, axis ) )
	self.global_transform.basis = straightb
