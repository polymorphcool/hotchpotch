# Cyclic Coordinate Descent Inverse Kinematics (CCDIK) implementation
# main object: manages chains, lookats and constraints
# author: frankiezafe

tool

extends Node

export (bool) var _play : bool = true setget play
export (bool) var _force_update : bool = true
export (bool) var _root_translation : bool = true
export (NodePath) var _skeleton : NodePath setget skeleton
export (int,1,100) var _iterations = 10
export (int,1,10) var _tolerance = 5 setget tolerance
export (bool) var _debug_iterations : bool = true setget debug_iterations
export (float,0,100) var _interpolation_speed : float = 10

func play( b : bool ):
	_play = b
	if not _play:
		reset_poses()

func skeleton( np = null ):
	_skeleton = np
	if get_node( _skeleton ) == null:
		sk = null
		refresh = true
		initialised = false
		return
	sk = get_node( _skeleton )
	if not sk is Skeleton:
		sk = null
		refresh = true
		initialised = false

func tolerance(i):
	_tolerance = i
	epsilon = pow( 10, -_tolerance )

func debug_iterations( b: bool ):
	_debug_iterations = b
	if replicat != null:
		replicat.visible = _debug_iterations
	refresh = true

var chain_script = load( "res://research/ik/ccdik_chain.gd")
#var lookat_script = load( "res://research/ik/ccdik_lookat.gd")
#var constraint_script = load( "res://research/ik/ccdik_constraint.gd")

var sk : Skeleton = null
# original object transform
var refresh : bool = true
var initialised : bool = false
var epsilon = 1e-4 # controls precision of computation (tolerance)
# local copy of bones data
var skeleton_hierarchy : Array = []
# ccdik nodes
var ccdik_chains : Array = []
var ccdik_lookats : Array = []
var ccdik_constraints : Array = []
var childnum : int = 0
var chainnum : int = 0
var replicat : ImmediateGeometry = null

func clear():
	childnum = get_child_count()
	skeleton_hierarchy = []
	ccdik_chains = []
	ccdik_lookats = []
	ccdik_constraints = []

func reset_poses():
	
	if not initialised:
		return
	
	for b in skeleton_hierarchy:
		b.pose = b.init_pose
		sk.set_bone_pose( b.id, b.pose )

func validate_chain( chain: Node ):
	# reseting chain
	chain.validate(false)
	# no target => invalid chain
	if chain.get_target_node() == null:
		return
	var bid = sk.find_bone( chain.get_bone_name() )
	if bid > -1:
		chain.validate(true)
		chain.add_id( bid, 1.0 )
		skeleton_hierarchy[bid].total_weight +=1
	# validation of depth
	var dcheck = 0
# warning-ignore:unused_variable
	for i in range( 0, chain.get_depth() ):
		bid = skeleton_hierarchy[ bid ].parent
		if bid == -1:
			# end of site!
			break
		# adding bone id
		chain.add_id( bid, 1.0 )
		# adding weight
		skeleton_hierarchy[bid].total_weight +=1
		dcheck += 1
	if dcheck != chain.get_depth():
		chain.depth( dcheck )

func collect_chains( root : Node = null ):
	if root == null:
		ccdik_chains = []
		print( chain_script )
		collect_chains( self )
	else:
		for c in root.get_children():
			var s = c.get_script()
			if s != null and s == chain_script:
				# just to be certain
				c.disconnect( "ccdik_chain_exit", self, "initialise" )
				c.disconnect( "ccdik_chain_t2b", self, "target_to_bone" )
				c.disconnect( "ccdik_chain_refresh", self, "initialise" )
				# verifying that everything is ok
				validate_chain( c )
				# linking exit signals to self 
				c.connect( "ccdik_chain_exit", self, "initialise" )
				c.connect( "ccdik_chain_t2b", self, "target_to_bone" )
				c.connect( "ccdik_chain_refresh", self, "initialise" )
				ccdik_chains.append( c )
			collect_chains(c)

func collect_lookats( root : Node = null ):
	if root == null:
		ccdik_lookats = []
		collect_lookats( self )
	else:
		for c in root.get_children():
			var s = c.get_script()
			if s != null and s.has_script_signal( "ccdik_lookat_exit" ):
				ccdik_lookats.append( c )
			collect_lookats( c )

func validate_constraint( constraint: Node ):
	# reseting chain
	constraint.validate(false)
	var bid = sk.find_bone( constraint.get_bone_name() )
	if bid > -1:
		constraint.validate(true)
		skeleton_hierarchy[bid].constraint = constraint

func collect_constraints( root : Node = null ):
	if root == null:
		# removing all registered constrints
		for b in skeleton_hierarchy:
			b.constraint = null
		ccdik_constraints = []
		collect_constraints( self )
	else:
		for c in root.get_children():
			var s = c.get_script()
			if s != null and s.has_script_signal( "ccdik_constraint_exit" ):
				c.disconnect( "ccdik_constraint_exit", self, "collect_constraints" )
				c.disconnect( "ccdik_constraint_refresh", self, "collect_constraints" )
				# linking exit signals to self 
				validate_constraint( c )
				c.connect( "ccdik_constraint_exit", self, "collect_constraints" )
				c.connect( "ccdik_constraint_refresh", self, "collect_constraints" )
				ccdik_constraints.append( c )
			collect_constraints( c )

func hierarchy_add( bid : int ):
	var b = {
		# invariants
		'id': bid, # allows simple for loops on skeleton_hierarchy
		'name' : sk.get_bone_name( bid ),
		'parent' : sk.get_bone_parent( bid ),
		# configuration
		'locks': [false,false,false],
		'total_weight': 0.0,
		# from skeleton
		'glob': null,
		'rest': null,
		'custom': null,
		'init_pose': sk.get_bone_pose( bid ),
		'pose': null,
		# constraint object
		'constraint': null,
		# chains managing this bone
		'chains' : [],
		# computation matrix
		'built': null, # used by chains
		'pass': 0 # used by chains
	}
	skeleton_hierarchy.append( b )
	hierarchy_update( bid )

func hierarchy_update( bid : int ):
	var b = skeleton_hierarchy[bid]
	b.glob = sk.get_bone_global_pose( bid )
	b.rest = sk.get_bone_rest( bid )
	b.custom = sk.get_bone_custom_pose( bid )
	b.pose = sk.get_bone_pose( bid )

func initialise():
	
	if replicat == null and _debug_iterations:
		# removing all ImmediateGeometry in chilrden
		var igs = []
		for c in get_children():
			if c is ImmediateGeometry:
				igs.append( c )
		for ig in igs:
			remove_child( ig )
		igs = []
		replicat = ImmediateGeometry.new()
		replicat.visible = true
		replicat.material_override = SpatialMaterial.new()
		replicat.material_override.flags_unshaded = true
		replicat.material_override.flags_no_depth_test = true
		replicat.material_override.flags_transparent = true
		replicat.material_override.albedo_color = Color(0,1,1,0.5)
		add_child( replicat )
	
	refresh = false
	skeleton( _skeleton )
	if sk == null:
		initialised = false
		return
	clear()
	var bnum = sk.get_bone_count()
	for i in range( 0, bnum ):
		hierarchy_add( i )
	
	collect_chains()
	collect_lookats()
	collect_constraints()
	
	chainnum = ccdik_chains.size()
	
	# rendering chain weights
	for c in ccdik_chains:
		for i in range( 0, c.size ):
			var b = skeleton_hierarchy[ c.ids[i] ]
			c.weights[i] /= b.total_weight
			b.chains.append( c )
			
	
	initialised = true
	
	# summary
	print( "####### skeleton_hierarchy #######" )
	for b in skeleton_hierarchy:
		print( "name ", b.name )
		print( "\tid ", b.id )
		if b.parent > -1:
			print( "\tparent ", sk.get_bone_name( b.parent ) )
		else:
			print( "\tparent ", sk.name )
		print( "\ttotal_weight ", b.total_weight )
#		print( "\tglob ", b.glob )
#		print( "\trest ", b.rest )
#		print( "\tcustom ", b.custom )
#		print( "\tpose ", b.pose )
		print( "\tconstraint ", b.constraint )
	print( "####### ccdik_chains #######" )
	for c in ccdik_chains:
		print( c.name )
		print( "\tplay: ", c._play )
		print( "\tdepth: ", c._depth )
		print( "\tvalid: ", c.valid )
		print( "\tids: ", c.ids )
		print( "\tweights: ", c.weights )
	print( "####### ccdik_lookats #######" )
	print( ccdik_lookats )
	print( "####### ccdik_constraints #######" )
	print( ccdik_constraints )

func target_to_bone( chain : Node ):
	var id = chain.ids[chain._depth]
	var target = chain.get_target_node()
	target.global_transform = sk.global_transform * skeleton_hierarchy[id].glob

func update( delta : float ):
	
	for b in skeleton_hierarchy:
		if _force_update:
			hierarchy_update( b.id )
		# reseting global transforms
		b.built = b.glob
		b.pose = Transform()
	
	var interpolation = delta * _interpolation_speed
	if interpolation > 1:
		interpolation = 1
	
	if replicat != null:
		replicat.clear()
		if _debug_iterations:
			replicat.begin( Mesh.PRIMITIVE_LINES )
	
	var sk_glob = sk.global_transform
	
	for it in range( 0, _iterations ):
		
		# used to render root translations
		var root_pos : Vector3 = Vector3(0,0,0)
		var active_chains = 0
		
		for chain in ccdik_chains:
			if chain.is_active():
				chain.compute( delta, sk_glob, skeleton_hierarchy, epsilon )
				if _root_translation:
					var offset = chain.get_offset( sk_glob, skeleton_hierarchy )
					if offset != null:
						root_pos += offset
					active_chains += 1
		
		if active_chains > 0 and _root_translation:
				var root = skeleton_hierarchy[0]
				root.pass += 1
				root.pose.origin += root.built.xform_inv(root_pos) / active_chains
		
		# updating of all builts
		var all_pass_0 = true
		for b in skeleton_hierarchy:
			var pmat = Transform()
			if b.parent > -1:
				pmat = skeleton_hierarchy[ b.parent ].built
			b.built = pmat * b.rest * b.custom * b.pose
			if b.pass != 0:
				all_pass_0 = false
			if replicat != null and _debug_iterations:
				if b.parent != -1:
					replicat.add_vertex( (sk.global_transform * skeleton_hierarchy[ b.parent ].built).origin ) 
				else:
					replicat.add_vertex( sk.global_transform.origin )
				replicat.add_vertex( (sk.global_transform * b.built).origin )
		
		if all_pass_0:
			# no need to continue!
			break
	
	if replicat != null and _debug_iterations:
		replicat.end()
	
	# aligining tips
	for chain in ccdik_chains:
		if chain._align_tip:
			chain.align_tip( delta, sk_glob, skeleton_hierarchy, epsilon )
	
	# applying computed poses
	for b in skeleton_hierarchy:
		
		var skp = sk.get_bone_pose( b.id )
		var ikp = b.pose
		
		# rendering slerp
		if not b.chains.empty():
			var w = 0
			for c in b.chains:
				w += c.get_slerp_for( b.id )
			w /= b.total_weight
			if w == 0:
				ikp = Transform()
			elif w != 1:
				ikp = Transform().interpolate_with( ikp, w )
		
		sk.set_bone_pose( b.id, skp.interpolate_with( ikp, interpolation ) )
		
		if b.constraint != null:
			var parentb = null
			if b.parent != -1:
				parentb = skeleton_hierarchy[b.parent]
			b.constraint.move( sk_glob, parentb, skeleton_hierarchy[b.id] )

func _enter_tree():
	refresh = true
	initialised = false

# warning-ignore:unused_argument
func _process(delta):
	
	# this check only runs inside the editor
	# call "initialise" if you create chains, 
	# constraints or lookat programmatically
	if Engine.is_editor_hint() and childnum != get_child_count():
		initialise()
	
	if _play:
		if refresh:
			initialise()
		elif initialised:
			update( delta )
	elif initialised:
		reset_poses()
		clear()
		initialised = false
		refresh = true
