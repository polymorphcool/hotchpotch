tool

extends Spatial

export (bool) var _rotate_x = false setget rx
export (bool) var _rotate_y = false setget ry
export (bool) var _rotate_z = false setget rz
export (float,-1,1) var _speed = .5

func rx( b : bool ):
	_rotate_x = b
	if !b:
		self.rotation.x = 0
func ry( b : bool ):
	_rotate_y = b
	if !b:
		self.rotation.y = 0
func rz( b : bool ):
	_rotate_z = b
	if !b:
		self.rotation.z = 0

var rot = Vector3(0,0,0)

func _process(delta):
	if _rotate_x:
		rot.x += delta * _speed
	else:
		rot.x = 0
	if _rotate_y:
		rot.y += delta * _speed
	else:
		rot.y = 0
	if _rotate_z:
		rot.z += delta * _speed
	else:
		rot.z = 0
	if _rotate_x or _rotate_y or _rotate_z:
		self.rotation = rot
