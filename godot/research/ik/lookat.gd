tool

extends Node

enum FACING {
	X, Y, Z, Xi, Yi, Zi
}

export (bool) var _play = true
export (NodePath) var _skeleton setget skeleton
export (String) var _bone = "" setget bone
export (NodePath) var _target setget target
export (float,0,1) var _slerp = 0.5
export (FACING) var _facing = FACING.Zi setget facing_axis
export (bool) var lock_x = false
export (bool) var lock_y = false
export (bool) var lock_z = false

func skeleton( np ):
	_skeleton = np
	if get_node( _skeleton ) == null:
		sk = null
		refresh = true
		initialised = false
		return
	sk = get_node( _skeleton )
	if not sk is Skeleton:
		sk = null
		refresh = true
		initialised = false

func bone( s ):
	_bone = s
	refresh = true

func target( np ):
	_target = np
	if get_node( _target ) == null:
		trgt = null
		refresh = true
		initialised = false
		return
	trgt = get_node( _target )
	if not trgt is Spatial:
		trgt = null
		refresh = true
		initialised = false

func facing_axis( a ):
	_facing = a
	refresh = true

var sk = null
var trgt = null
var refresh = true
var initialised = false
var pid = null
var bid = null
var gmat = null
var rest = null
var custom = null
var unit_transform = Transform(Basis(Vector3(1,0,0),Vector3(0,1,0),Vector3(0,0,1)),Vector3(0,0,0))

func clear():
	# reseting the skeleton
	for i in range(0, sk.get_bone_count() ):
		sk.set_bone_pose( i, unit_transform )

func initialise():
	
	refresh = false
	skeleton( _skeleton )
	target( _target )
	
	if sk == null or trgt == null or sk.find_bone( _bone ) == -1:
		initialised = false
		return
	
	clear()
	bid = sk.find_bone( _bone )
	pid = sk.get_bone_parent( bid )
	gmat = sk.get_bone_global_pose( bid )
	rest = sk.get_bone_rest( bid )
	custom = sk.get_bone_custom_pose( bid )
	initialised = true

func render_axis( v3, bup, bud ):
	
	var dt_z = v3.normalized()
	if abs( dt_z.dot( bup ) ) == 1:
		return null
	var up = Vector3(0,1,0)
	if bud < 0:
		up *= -1
	var dt_x = up.cross(dt_z).normalized()
	if bud == 0:
		dt_x *= -1
	var dt_y = dt_z.cross(dt_x).normalized()
	
	match _facing:
		FACING.X:
			dt_x *= -1
			dt_z *= -1
			return Basis(dt_z,dt_x,dt_y)
		FACING.Xi:
			return Basis(dt_z,dt_x,dt_y)
		FACING.Y:
			dt_z *= -1
			return Basis(dt_x,dt_z,dt_y)
		FACING.Yi:
			dt_y *= -1
			dt_z *= -1
			return Basis(dt_x,dt_z,dt_y)
		FACING.Z:
			dt_z *= -1
			dt_y *= -1
			return Basis(dt_x,dt_y,dt_z)
		FACING.Zi:
			return Basis(dt_x,dt_y,dt_z)
	return null

func update():
	
	if lock_x and lock_y and lock_z:
		sk.set_bone_pose( bid, unit_transform )
		return
	var mat = sk.get_bone_global_pose( pid ) * rest * custom
	var target = sk.global_transform.xform_inv( trgt.global_transform.origin )
	# computing rotations in skeleton space
	var up = Vector3(0,1,0)
	var bup = gmat.basis.xform( Vector3(0,1,0) ).normalized()
	# y axis polarity compared to skeleton space
	# will be used to invert axis
	var bud = bup.dot( up )
	var bt = render_axis( mat.origin - target, bup, bud )
	if bt != null:
		var b = ( mat.basis.inverse() * bt ).orthonormalized()
		if lock_x or lock_y or lock_z: 
			if lock_x:
				b *= Basis( b[0], -b.get_euler().x )
			if lock_y:
				b *= Basis( b[1], -b.get_euler().y )
			if lock_x:
				b *= Basis( b[2], -b.get_euler().z )
		b = Basis().slerp( b, _slerp )
		sk.set_bone_pose( bid, b )

func _process(delta):
	if _play:
		if refresh:
			initialise()
		elif initialised:
			update()
	elif initialised:
		clear()
		initialised = false
		refresh = true
