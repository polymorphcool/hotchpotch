# Cyclic Coordinate Descent Inverse Kinematics (CCDIK) implementation
# constraint: removes or limits rotation axis of a basis
# author: frankiezafe

tool

extends Node

signal ccdik_constraint_exit
signal ccdik_constraint_refresh

export var _play : bool = true setget play
export var _debug : bool = true setget debug
export var _bone_name : String = "" setget bone_name

export var lock_x : bool = false
export var lock_y : bool = false
export var lock_z : bool = false

export var limit_x : bool = false
export (float,-180,180) var limit_x_min : float = -180
export (float,-180,180) var limit_x_max : float = 180
export var limit_y : bool = false
export (float,-180,180) var limit_y_min : float = -180
export (float,-180,180) var limit_y_max : float = 180
export var limit_z : bool = false
export (float,-180,180) var limit_z_min : float = -180
export (float,-180,180) var limit_z_max : float = 180

func play( b : bool ):
	_play = b
	for c in get_children():
		c.visible = _play

func debug( b : bool ):
	_debug = b
	if debug_axis != null:
		debug_axis.visible = _debug
		debug_limits.visible = _debug

func bone_name( s : String ):
	_bone_name = s
	emit_signal("ccdik_constraint_refresh" )

func get_bone_name():
	return _bone_name

func validate( b : bool ):
	valid = b

var valid : bool = false # set by solver!
var debug_axis : Spatial = null
var debug_limits : Spatial = null

func update_limits():
	update_limit(0, limit_x, limit_x_min, limit_x_max)
	update_limit(1, limit_y, limit_y_min, limit_y_max)
	update_limit(2, limit_z, limit_z_min, limit_z_max)

func update_limit( i : int, enabled : bool, amin : float, amax : float ):
	var dl = debug_limits.get_child( i )
	var gap = TAU / 32
	dl.clear()
	dl.begin( Mesh.PRIMITIVE_LINES )
	if not enabled:
		for j in range( 1, 32 + 1 ):
			var k = j - 1
			var ch = cos( gap * k )
			var sh = sin( gap * k )
			var ci = cos( gap * j )
			var si = sin( gap * j )
			match i:
				0:
					dl.add_vertex( Vector3(0,ch,sh) * 0.5 )
					dl.add_vertex( Vector3(0,ci,si) * 0.5 )
				1:
					dl.add_vertex( Vector3(ch,0,sh) * 0.5 )
					dl.add_vertex( Vector3(ci,0,si) * 0.5 )
				_:
					dl.add_vertex( Vector3(ch,sh,0) * 0.5 )
					dl.add_vertex( Vector3(ci,si,0) * 0.5 )
	else:
		amin *= PI / 180
		amax *= PI / 180 
		var dist = amax - amin
		var steps : int = ceil( abs(dist) / gap )
		var g = dist / steps
		for ii in range( 0, steps ):
			var a = amin + ii * g
			var b = amin + (ii+1) * g
			var ca = cos( a )
			var sa = sin( a )
			var cb = cos( b )
			var sb = sin( b )
			if ii == 0 or ii == steps -1:
				dl.add_vertex( Vector3(0,0,0) )
			match i:
				0:
					if ii == 0:
						dl.add_vertex( Vector3(0,ca,sa) * 0.5 )
					if ii == steps -1:
						dl.add_vertex( Vector3(0,cb,sb) * 0.5 )
					dl.add_vertex( Vector3(0,ca,sa) * 0.5 )
					dl.add_vertex( Vector3(0,cb,sb) * 0.5 )
				1:
					if ii == 0:
						dl.add_vertex( Vector3(ca,0,sa) * 0.5 )
					if ii == steps -1:
						dl.add_vertex( Vector3(cb,0,sb) * 0.5 )
					dl.add_vertex( Vector3(ca,0,sa) * 0.5 )
					dl.add_vertex( Vector3(cb,0,sb) * 0.5 )
				_:
					if ii == 0:
						dl.add_vertex( Vector3(ca,sa,0) * 0.5 )
					if ii == steps -1:
						dl.add_vertex( Vector3(cb,sb,0) * 0.5 )
					dl.add_vertex( Vector3(ca,sa,0) * 0.5 )
					dl.add_vertex( Vector3(cb,sb,0) * 0.5 )
	dl.end()

func update_rotations( basis : Basis ):
	var eulers = basis.get_euler()
	var gap = TAU / 32
	for i in range(3,6):
		var im = debug_limits.get_child( i )
		var start = 0
		var end = eulers[i-3]
		if end == 0:
			continue
		# rendering steps
		var steps : int = ceil( abs(end) / gap )
		var g = end / steps
		var dl = debug_limits.get_child( i )
		dl.clear()
		dl.begin( Mesh.PRIMITIVE_TRIANGLES )
		for ii in range( 0, steps ):
			var a = ii * g
			var b = (ii+1) * g
			var ca = cos( a )
			var sa = sin( a )
			var cb = cos( b )
			var sb = sin( b )
			dl.add_vertex( Vector3(0,0,0) )
			match i:
				3:
					dl.add_vertex( Vector3(0,ca,sa) * 0.5 )
					dl.add_vertex( Vector3(0,cb,sb) * 0.5 )
				4:
					dl.add_vertex( Vector3(ca,0,sa) * 0.5 )
					dl.add_vertex( Vector3(cb,0,sb) * 0.5 )
				_:
					dl.add_vertex( Vector3(ca,sa,0) * 0.5 )
					dl.add_vertex( Vector3(cb,sb,0) * 0.5 )
		dl.end()

func _init():
	
	while get_child_count() > 0:
		remove_child( get_child(0) )
	
	var mats = []
	for i in range(0,6):
		var m = SpatialMaterial.new()
		m.flags_unshaded = true
		m.flags_no_depth_test = true
		m.flags_transparent = true
		m.set_cull_mode( SpatialMaterial.CULL_DISABLED )
		match i:
			0:
				m.albedo_color = Color(1,0,0,0.6)
			1:
				m.albedo_color = Color(0,1,0,0.6)
			2:
				m.albedo_color = Color(0,0,1,0.6)
			3:
				m.albedo_color = Color(1,0,0,0.3)
			4:
				m.albedo_color = Color(0,1,0,0.3)
			_:
				m.albedo_color = Color(0,0,1,0.3)
		mats.append(m)
	
	debug_axis = Spatial.new()
	add_child( debug_axis )
	
	var im : ImmediateGeometry = null
	
	for i in range(0,3):
		im = ImmediateGeometry.new()
		im.material_override = mats[i]
		im.begin( Mesh.PRIMITIVE_LINES )
		im.add_vertex( Vector3(0,0,0) )
		match i:
			0:
				im.add_vertex( Vector3.RIGHT )
			1:
				im.add_vertex( Vector3.UP )
			_:
				im.add_vertex( Vector3.BACK )
		im.end()
		debug_axis.add_child( im )
	
	debug_limits = Spatial.new()
	add_child( debug_limits )
	
	for i in range(0,6):
		im = ImmediateGeometry.new()
		im.material_override = mats[i]
		debug_limits.add_child(im)
	for i in range(0,3):
		im = debug_axis.get_child(i).duplicate()
		debug_limits.add_child(im)
	
	debug_axis.visible = _debug
	debug_limits.visible = _debug
	
	update_limits()

func move( obj : Transform, parent : Dictionary, bone : Dictionary ):
	if !valid or !_play:
		return
	var ptrans = Transform()
	if parent != null:
		ptrans = parent.built
	debug_limits.global_transform = obj * bone.built
	debug_axis.global_transform = obj * bone.built
	update_limits()
	update_rotations( bone.pose.basis )

func _exit_tree():
	emit_signal("ccdik_constraint_exit")

func apply_v3( current: Vector3, target: Vector3 ):
	
	if !valid or !_play:
		return target
	if not lock_x and not lock_y and not lock_z and not limit_x and not limit_y and not limit_z:
		# nothing to do
		return target
	if lock_x and lock_y and lock_z:
		return current
	var diff = target - current
	if lock_x:
		diff.x = 0
	if lock_y:
		diff.y = 0
	if lock_z:
		diff.z = 0
	return current + diff

func apply_basis( ba : Basis ):
	
	if !valid or !_play:
		return ba
	if not lock_x and not lock_y and not lock_z and not limit_x and not limit_y and not limit_z:
		# nothing to do
		return ba
	if lock_x and lock_y and lock_z:
		return Basis()
	
	if lock_x:
		ba *= Basis( ba[0].normalized(), -ba.get_euler().x )
	if lock_y:
		ba *= Basis( ba[1].normalized(), -ba.get_euler().y )
	if lock_z:
		ba *= Basis( ba[2].normalized(), -ba.get_euler().z )
	
	return ba
