tool

extends Node

export var _source : NodePath setget source
export var _target : NodePath setget target
export (bool) var limit_x = false
export (bool) var limit_y = false
export (bool) var limit_z = false

export (float, -180,180) var x_min = -180
export (float, -180,180) var x_max = 180
export (float, -180,180) var y_min = -180
export (float, -180,180) var y_max = 180
export (float, -180,180) var z_min = -180
export (float, -180,180) var z_max = 180

var src : Spatial = null
var trgt : Spatial = null
var materials : Array = []

func source( np : NodePath ):
	_source = np
	reload()

func target( np : NodePath ):
	_target = np
	trgt = get_node( _target )

func reload():
	
	while get_child_count() > 0:
		remove_child( get_child(0) )
	materials = []
	
	if _source == null:
		src = null
		return
	src = get_node( _source )
	
	materials = []
	for i in range(0,6):
		var m = SpatialMaterial.new()
		m.flags_unshaded = true
		m.flags_no_depth_test = true
		if i > 2:
			m.flags_transparent = true
			m.params_cull_mode = SpatialMaterial.CULL_DISABLED
		match i:
			0:
				m.albedo_color = Color( 1,0,0, 1)
			1:
				m.albedo_color = Color( 0,1,0, 1)
			2:
				m.albedo_color = Color( 0,0,1, 1)
			3:
				m.albedo_color = Color( 1,0.2,0.2, 0.5)
			4:
				m.albedo_color = Color( 0.2,1,0.2, 0.5)
			_:
				m.albedo_color = Color( 0.2,0.2,1, 0.5)
		materials.append(m)
	
	# creating 2 axis system
	var size = 0.5
	for i in range(0,2):
		var axis = Spatial.new()
		add_child( axis )
		for j in range(0,3):
			var a = ImmediateGeometry.new()
			a.begin( Mesh.PRIMITIVE_LINES )
			a.add_vertex(Vector3())
			match j:
				0:
					a.add_vertex(Vector3(1,0,0)*size)
				1:
					a.add_vertex(Vector3(0,1,0)*size)
				_:
					a.add_vertex(Vector3(0,0,1)*size)
			a.end()
			a.material_override = materials[j]
			axis.add_child( a )
		size = 1
	
	# creating 3 axis constraints visualisation
	for i in range(0,3):
		var limit = ImmediateGeometry.new()
		limit.material_override = materials[3+i]
		add_child( limit )

func straighten( basis : Basis, axis : int ):
	var x = Vector3(1,0,0)
	var y = Vector3(0,1,0)
	var z = Vector3(0,0,1)
	match axis:
		0:
			# rendering Y and Z aligned with the world
			var yydot = Vector3(0,1,0).dot(basis[1])
			var xydot = Vector3(1,0,0).dot(basis[1])
			y = ( Vector3(0,1,0) * yydot + Vector3(1,0,0) * xydot ).normalized()
			var zzdot = Vector3(0,0,1).dot(basis[2])
			var xzdot = Vector3(1,0,0).dot(basis[2])
			z = ( Vector3(0,0,1) * zzdot + Vector3(1,0,0) * xzdot ).normalized()
			x = y.cross(z).normalized()
		1:
			# rendering X and Z aligned with the world
			var xxdot = Vector3(1,0,0).dot(basis[0])
			var yxdot = Vector3(0,1,0).dot(basis[0])
			x = ( Vector3(1,0,0) * xxdot + Vector3(0,1,0) * yxdot ).normalized()
			var zzdot = Vector3(0,0,1).dot(basis[2])
			var yzdot = Vector3(0,1,0).dot(basis[2])
			z = ( Vector3(0,0,1) * zzdot + Vector3(0,1,0) * yzdot ).normalized()
			y = z.cross(x).normalized()
		_:
			# rendering X and Y aligned with the world
			var xxdot = Vector3(1,0,0).dot(basis[0])
			var zxdot = Vector3(0,0,1).dot(basis[0])
			x = ( Vector3(1,0,0) * xxdot + Vector3(0,0,1) * zxdot ).normalized()
			var yydot = Vector3(0,1,0).dot(basis[1])
			var zydot = Vector3(0,0,1).dot(basis[1])
			y = ( Vector3(0,1,0) * yydot + Vector3(0,0,1) * zydot ).normalized()
			if y.dot(Vector3(0,1,0)) < 0:
				x *= -1
				y *= -1
			z = x.cross(y).normalized()
	var out = Basis(x,y,z)
	return out

func limit_add_vertex( im : ImmediateGeometry, axis : int, x : float, y : float ):
	match axis:
		0:
			im.add_vertex( Vector3( 0,x,y ) )
		1:
			im.add_vertex( Vector3( y,0,x ) )
		_:
			im.add_vertex( Vector3( x,y,0 ) )

func render_limit( axis : int, rad : float ):
	var im : ImmediateGeometry = null
	var min_max = [0,0]
	var d2r = PI / 180
	match axis:
		0:
			im = get_child(2)
			min_max = [ x_min * d2r, x_max * d2r ]
		1:
			im = get_child(3)
			min_max = [ y_min * d2r, y_max * d2r ]
		_:
			im = get_child(4)
			min_max = [ z_min * d2r, z_max * d2r ]
	var def = TAU / 32
	# rendering limit
	var steps : int = ceil( ( min_max[1]-min_max[0] ) / def )
	var gap = ( min_max[1]-min_max[0] ) / steps
	im.clear()
	im.begin( Mesh.PRIMITIVE_LINES )
	var preva = min_max[0]
	match axis:
		2:
			preva += PI * 0.5
	var prevcs = [ cos(preva)*.5, sin(preva)*.5 ]
	im.add_vertex( Vector3(0,0,0) )
	limit_add_vertex( im, axis, prevcs[0],prevcs[1] )
	for i in range( 1, steps + 1 ):
		var a = preva + gap
		var cosa = cos(a)*.5
		var sina = sin(a)*.5
		limit_add_vertex( im, axis, prevcs[0],prevcs[1] )
		limit_add_vertex( im, axis, cosa,sina )
		preva = a
		prevcs[0] = cosa
		prevcs[1] = sina
	limit_add_vertex( im, axis, prevcs[0],prevcs[1] )
	im.add_vertex( Vector3(0,0,0) )
	im.end()
	# rendering current axis
	steps = ceil( abs(rad) / def )
	if steps == 0:
		return
	gap = rad / steps
	im.begin( Mesh.PRIMITIVE_TRIANGLES )
	preva = 0
	match axis:
		2:
			preva += PI * 0.5
	prevcs = [ cos(preva)*.5, sin(preva)*.5 ]
	for i in range( 1, steps + 1 ):
		var a = preva + gap
		var cosa = cos(a)*.5
		var sina = sin(a)*.5
		im.add_vertex( Vector3(0,0,0) )
		limit_add_vertex( im, axis, prevcs[0],prevcs[1] )
		limit_add_vertex( im, axis, cosa,sina )
		preva = a
		prevcs[0] = cosa
		prevcs[1] = sina
	im.end()

func _process(delta):
	
	if src == null:
		reload()
	if src == null:
		return
	
	var srcb = src.global_transform.basis
	if limit_x:
		var strb = straighten( srcb, 0 )
		var dotz = srcb[2].dot( strb[2] )
		# this renders only an HALF circle rotation (0>PI)
		var ax = acos( dotz ) * 180.0 / PI
		# to detect the rotation X sign,
		# we project the up axis on forward:
		# - if < 0: going up
		# - if > 0: going down
		var ay = srcb[1].dot( strb[2] )
		if ay < 0:
			ax *= -1
		if ax > x_max:
			srcb = strb.rotated( strb[0].normalized(), x_max * PI / 180 )
			ax = x_max
		elif ax < x_min:
			srcb = strb.rotated( strb[0].normalized(), x_min * PI / 180 )
			ax = x_min
		# visualisation of limit
		get_child(2).global_transform.basis = Basis()
		render_limit(0, ax * PI / 180)
	if limit_y:
		var strb = straighten( srcb, 1 )
		var dotz = srcb[2].dot( strb[2] )
		var ay = -acos( dotz ) * 180.0 / PI
		var ax = srcb[0].dot( strb[2] )
		if ax < 0:
			ay *= -1
		if ay > y_max:
			srcb = strb.rotated( strb[1].normalized(), y_max * PI / 180 )
			ay = y_max
		elif ay < y_min:
			srcb = strb.rotated( strb[1].normalized(), y_min * PI / 180 )
			ay = y_min
		get_child(3).global_transform.basis = Basis()
		render_limit(1, ay * PI / 180)
	if limit_z:
		var strb = straighten( srcb, 2 )
		var dotz = srcb[1].dot( strb[1] )
		var az = acos( dotz ) * 180.0 / PI
		var ax = srcb[0].dot( strb[1] )
		if ax < 0:
			az *= -1
		if az > z_max:
			srcb = strb.rotated( strb[2].normalized(), z_max * PI / 180 )
			az = z_max
		elif az < z_min:
			srcb = strb.rotated( strb[2].normalized(), z_min * PI / 180 )
			az = z_min
		get_child(4).global_transform.basis = Basis()
		render_limit(2, az * PI / 180)
	
	get_child(0).global_transform.basis = src.global_transform.basis
	get_child(1).global_transform.basis = srcb
	
	if trgt != null:
		trgt.global_transform.basis = srcb
		trgt.global_transform.origin = self.global_transform.origin
