# Cyclic Coordinate Descent Inverse Kinematics (CCDIK) implementation
# chains: tries to reach target by rotating a hierarchy of bones
# author: frankiezafe

tool

extends Node

signal ccdik_chain_exit
signal ccdik_chain_refresh
signal ccdik_chain_t2b

export (bool) var _play = true setget play
export (String) var _bone_name = "" setget bone_name
export (int,1,100) var _depth = 2 setget depth
export (NodePath) var _target setget target
export (bool) var _align_tip = true
export (float,0,1) var _slerp = 1 setget slerp
export (float,0,1) var _weight_decrease_factor = 0.01
export (bool) var _target2bone = false setget target2bone

func play( b : bool ):
	_play = b

func bone_name( s : String ):
	_bone_name = s
	emit_signal("ccdik_chain_refresh" )

func get_bone_name():
	return _bone_name

func depth( f : float ):
	_depth = f
	emit_signal("ccdik_chain_refresh" )

func get_depth():
	return _depth

func target( np : NodePath ):
	_target = np
	target_node = get_node( _target )
	emit_signal("ccdik_chain_refresh" )

func get_target_node():
	return target_node

func slerp( f : float ):
	_slerp = f

func get_slerp_for( bid : int ):
	
	var w = 0
	if !is_active():
		return w
	for i in range(0, size):
		if ids[i] == bid:
			w = weights[1]
	return w * _slerp

func target2bone( b : bool = true ):
	_target2bone = false
	if b and target_node != null:
		emit_signal("ccdik_chain_t2b", self )

func validate( b : bool ):
	valid = b
	if !valid:
		clear()

func is_active():
	if !valid or !_play or _slerp == 0:
		return false
	return true

var target_node : Spatial = null
var ids : Array = [] # DO NOT SET
var weights : Array = [] # DO NOT SET
var size : int = 0 # DO NOT SET
var valid : bool = false # set by solver!

func _exit_tree():
	emit_signal("ccdik_chain_exit")

func _ready():
	target( _target )

func clear():
	ids = []
	weights = []
	size = 0
	valid = false

func add_id( bid : int, weight = 1.0 ):
	if !valid:
		return
	ids.push_front( bid )
	weights.push_front( weight )
	size += 1

func get_offset( mat : Transform, hierarchy : Array ):
	
	if !is_active():
		return null
	
	var aim = mat.xform_inv( target_node.global_transform.origin )
	var tip = hierarchy[ids[_depth]]
	return ( aim - tip.built.origin ) * _slerp

func close_enough( aim, tip, epsilon ):
	return ( aim - tip.built.origin ).length() <= epsilon

func render_pose( b, current, target, epsilon ):
	var nc = current.normalized()
	var nt = target.normalized()
	if b.constraint != null:
		nt = b.constraint.apply_v3( nc, nt ).normalized()
	var d = nt.dot( nc )
	# no rotation needed!
	if d > 1 - epsilon:
		return null
	elif d < -epsilon:
		d = -epsilon
	var axis = nt.cross( nc ).normalized()
	var newb = Basis().rotated( axis, -acos(d) )
#	if b.constraint != null:
#		b.pose.basis = b.constraint.apply_basis( b.pose.basis )
#		newb = b.constraint.apply_basis( newb )
	return b.pose.basis * newb

func compute( delta : float, mat : Transform, hierarchy : Array, epsilon: float ):
	
	if !is_active():
		return true
	
	var aim = mat.xform_inv( target_node.global_transform.origin )
	var tip = hierarchy[ids[_depth]]
	
	if not close_enough( aim, tip, epsilon ):
	
		# from tip to root!
		for i in range( 1, _depth ):
			var ii = _depth - i
			# getting bone id and weight
			var bid = ids[ii]
			var w = weights[ii] - ( i / ( _depth - 1.0 ) ) * _weight_decrease_factor
			# now accessing the hierarchy
			var b = hierarchy[bid]
			var cur = b.built.xform_inv( tip.built.origin )
			var tgt = b.built.xform_inv( aim )
			var newb = render_pose( b, cur, tgt, epsilon )
			# nothing new -> let's stop here
			if newb == null:
				break
			if w != -1 and b.pass > 0:
				b.pose.basis = b.pose.basis.slerp( newb, w )
			else:
				b.pose.basis = newb
			b.pass += 1
			# reconstruction of global transform
			b.built = hierarchy[ids[ii-1]].built * b.rest * b.custom * b.pose
			# keeping Y straight
			for j in range( ii+1, _depth+1 ):
				var bj = hierarchy[ids[j]]
				hierarchy[ids[j]].built = hierarchy[ids[j-1]].built * bj.rest * bj.custom * bj.pose
	
	return close_enough( aim, tip, epsilon )

func align_tip( delta : float, mat : Transform, hierarchy : Array, epsilon: float ):
	
	if !is_active():
		return true
	
	# rendering tip orientation depending on target orientation
	var b = hierarchy[ids[_depth]]
	var p = hierarchy[ids[_depth-1]]
	var tipb = (p.built * b.rest * b.custom).basis
	var tgtb = (mat.basis.inverse() * target_node.global_transform.basis).orthonormalized()
	# avoiding unnecessary computation
	if weights[_depth] != 1:
		tgtb = Basis().slerp( tgtb, weights[_depth] )
	b.pose.basis = tipb.inverse() * tgtb
	if _slerp != 1:
		b.pose.basis = Basis().slerp( b.pose.basis, _slerp )
	if b.constraint != null:
		b.pose.basis = b.constraint.apply_basis( b.pose.basis )
	b.pass += 1
