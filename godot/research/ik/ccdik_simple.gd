# Cyclic Coordinate Descent Inverse Kinematics (CCDIK) implementation
# basis computation inspired by https://github.com/sunilnayak133/ikccd/blob/master/Assets/Scripts/iksolver.cs
# a quick explanation of CCDIK: http://www.ryanjuckett.com/programming/cyclic-coordinate-descent-in-2d/
# author: frankiezafe

tool

extends Node

export (bool) var _play = true
export (NodePath) var _skeleton setget skeleton
export (String) var _bone = "" setget bone
export (int,0,100) var _depth = 0 setget depth
export (int,1,100) var _iterations = 10
export (int,1,10) var _tolerance = 5 setget tolerance
export (NodePath) var _target setget target
export (float,0,1) var _slerp = 0.5
export (Vector3) var _front = Vector3(0,0,-1) setget front
export (bool) var _debug = true setget debug
export (float,0,3) var _debug_length = 1 setget debug_length
export (bool) var _target2bone = false setget target2bone
export (Material) var x_mat = null
export (Material) var y_mat = null
export (Material) var z_mat = null
export (Material) var connect_mat = null
export (Material) var hierarchy_mat = null

func skeleton( np ):
	_skeleton = np
	if get_node( _skeleton ) == null:
		sk = null
		refresh = true
		initialised = false
		return
	sk = get_node( _skeleton )
	if not sk is Skeleton:
		sk = null
		refresh = true
		initialised = false

func target( np ):
	_target = np
	if get_node( _target ) == null:
		trgt = null
		refresh = true
		initialised = false
		return
	trgt = get_node( _target )
	if not trgt is Spatial:
		trgt = null
		refresh = true
		initialised = false

func bone( s ):
	_bone = s
	refresh = true

func depth( i ):
	if i < 1:
		i = 1
	if _depth != i:
		_depth = i
		refresh = true

func tolerance(i):
	_tolerance = i
	epsilon = pow( 10, -_tolerance )

func front( v3 ):
	_front = v3.normalized()
	refresh = true

func debug( b ):
	_debug = b
	refresh = true

func debug_length( f ):
	_debug_length = f
	refresh = true

func target2bone( b ):
	_target2bone = false
	if b and initialised and hierarchy[_depth].glob != null:
		trgt.global_transform.origin = sk.global_transform.xform( hierarchy[_depth].glob.origin )

var sk = null
var trgt = null
var hierarchy = null
var refresh = true
var initialised = false
var axis_display = [null,null,null]
var connector = null
var replicat = null
var epsilon = 1e-4
var unit_transform = Transform(Basis(Vector3(1,0,0),Vector3(0,1,0),Vector3(0,0,1)),Vector3(0,0,0))

func add_bone( d, bid ):
	hierarchy[ d ] = {
		'id': bid,
		'name': sk.get_bone_name( bid ),
		'glob': sk.get_bone_global_pose( bid ),
		'pose': sk.get_bone_pose( bid ),
		'custom': sk.get_bone_custom_pose( bid ),
		'locks': [false,false,false],
		'rest': sk.get_bone_rest( bid ),
		'built': null,
	}
	return hierarchy[ d ]

func clear():
	# reseting the skeleton
	for i in range(0, sk.get_bone_count() ):
		sk.set_bone_pose( i, unit_transform )
	# cleaning debugs
	while get_child_count() > 0:
		remove_child( get_child(0) )

func initialise():
	
	refresh = false
	skeleton( _skeleton )
	target( _target )
	
	if sk == null or trgt == null:
		initialised = false
		return
	clear()
	# building hierarchy
	hierarchy = {}
	var bid = sk.find_bone( _bone )
	var crrtd = _depth
	if bid > -1:
		add_bone( crrtd, bid )
		crrtd -= 1
	else:
		initialised = false
		return
# warning-ignore:unused_variable
	for i in range( 0, _depth ):
		bid = sk.get_bone_parent( bid )
		if bid > -1:
			add_bone( crrtd, bid )
			crrtd -= 1
		else:
			print( "no parent found for: ", bid, "!!!!!!!!" )
			break
	# print
	for i in range( 0, _depth + 1 ):
		for kk in hierarchy[i]:
			print( i, ':', kk, ' = ', hierarchy[i][kk] )
	if _debug:
		for i in range(0,3):
			axis_display[i] = ImmediateGeometry.new()
			match i:
				0:
					axis_display[i].material_override = x_mat
				1:
					axis_display[i].material_override = y_mat
				_:
					axis_display[i].material_override = z_mat
			add_child(axis_display[i])
		# other debuggers
		connector = ImmediateGeometry.new()
		connector.material_override = connect_mat
		add_child(connector)
		replicat = ImmediateGeometry.new()
		replicat.material_override = hierarchy_mat
		add_child(replicat)
	# and that's it!
	initialised = true

func render_pose( basis, current, target ):
	var nc = current.normalized()
	var nt = target.normalized()
	var d = nt.dot( nc )
	# no rotation needed!
	if d > 1 - epsilon:
		return null
	elif d < -epsilon:
		d = -epsilon
	var axis = nt.cross( nc ).normalized()
	var newb = Basis().rotated( axis, -acos(d) )
	newb = newb.rotated(newb[2], -newb.get_euler().z)
	return basis * newb

func update():
	
	# reseting all poses
	for i in range( 0, _depth + 1 ):
		hierarchy[i].built = hierarchy[i].glob
		hierarchy[i].pose = unit_transform
	
	var target = sk.global_transform.xform_inv( trgt.global_transform.origin )
	var tip = hierarchy[_depth]
	
	if _debug:
		for a in range(0,3):
			axis_display[a].clear()
			axis_display[a].begin( Mesh.PRIMITIVE_LINES )
			for i in range( 0, _depth ):
				var mat = sk.global_transform * hierarchy[i].built
				var start = mat.origin
				axis_display[a].add_vertex( start ) 
				match a:
					0:
						axis_display[a].add_vertex( start + mat.basis.xform(Vector3.RIGHT * _debug_length) ) 
					1:
						axis_display[a].add_vertex( start + mat.basis.xform(Vector3.UP * _debug_length) ) 
					_:
						axis_display[a].add_vertex( start + mat.basis.xform(Vector3.BACK * _debug_length) )
		replicat.clear()
		replicat.begin( Mesh.PRIMITIVE_LINES )
	
# warning-ignore:unused_variable
	for it in range(0,_iterations):
		
		var keepon = true
		
		# from tip to root!
		for i in range( 1, _depth ):
			var ii = _depth - i
			var b = hierarchy[ii]
			var cur = b.built.xform_inv( tip.built.origin )
			var tgt = b.built.xform_inv( target )
			var newb = render_pose( b.pose.basis, cur, tgt )
			# nothing new -> let's stop here
			if newb == null:
				keepon = false
				break
			b.pose.basis = newb
			b.built = hierarchy[ii-1].built * b.rest * b.custom * b.pose
			# keeping Y straight
			for j in range( ii+1, _depth+1 ):
				b = hierarchy[j]
				hierarchy[j].built = hierarchy[j-1].built * b.rest * b.custom * b.pose
				# locking rotation axis
#				var a_up = hierarchy[j].glob.basis.xform( Vector3.UP )
#				var b_up = hierarchy[j].built.basis.xform( Vector3.UP )
#				# putting Y up
#				var d = b_up.dot( a_up )
#				if d < 1-epsilon:
#					var mb = hierarchy[j].built.basis
#					var corrb = mb.rotated( mb[2], acos(d) ).orthonormalized()
#					corrb = (hierarchy[j-1].built * b.rest * b.custom).basis.inverse() * corrb
#					b.pose.basis *= corrb
#					hierarchy[j].built = hierarchy[j-1].built * b.rest * b.custom * b.pose
		
		if _debug: 
			for i in range( 0, _depth ):
				for a in range(0,3):
					var mat = sk.global_transform * hierarchy[i].built
					var g = sk.global_transform * hierarchy[i].glob
					var start = mat.origin
					axis_display[a].add_vertex( start ) 
					match a:
						0:
							axis_display[a].add_vertex( start + mat.basis.xform(Vector3.RIGHT * _debug_length) ) 
						1:
							axis_display[a].add_vertex( start + mat.basis.xform(Vector3.UP * _debug_length) )
							axis_display[a].add_vertex( start )  
							axis_display[a].add_vertex( start + g.basis.xform(Vector3.UP * _debug_length) )
						_:
							axis_display[a].add_vertex( start + mat.basis.xform(Vector3.BACK * _debug_length) )
				replicat.add_vertex( (sk.global_transform * hierarchy[i].built).origin ) 
				replicat.add_vertex( (sk.global_transform * hierarchy[i+1].built).origin ) 
		
		# avoiding unnecessary processing
		if not keepon or ( target - tip.built.origin ).length() < epsilon:
			break
	
	if _debug:
		for i in range(0,3):
			axis_display[i].end()
		replicat.end()
	
	# rendering tip orientation depending on target orientation
	var tipb = (hierarchy[_depth-1].built * hierarchy[_depth].rest * hierarchy[_depth].custom).basis
	var init = sk.global_transform.basis * hierarchy[_depth].glob.basis
	var tgtb = (sk.global_transform.basis.inverse() * trgt.global_transform.basis).orthonormalized()
	hierarchy[_depth].pose.basis = tipb.inverse() * tgtb * init

	for i in range( 0, _depth + 1 ):
		sk.set_bone_pose( hierarchy[i].id, Transform( Basis().slerp( hierarchy[i].pose.basis, _slerp ), Vector3(0,0,0) ) )

	if _debug:
		replicat.begin( Mesh.PRIMITIVE_LINES )
		for i in range( 0, _depth ):
			var bi = sk.global_transform * hierarchy[i].built
			var bj = sk.global_transform * hierarchy[i+1].built
			replicat.add_vertex( bi.origin + sk.global_transform.xform( Vector3.UP ) * 0.02 ) 
			replicat.add_vertex( bj.origin + sk.global_transform.xform( Vector3.UP ) * 0.02 ) 
			replicat.add_vertex( bi.origin ) 
			replicat.add_vertex( bj.origin ) 
		replicat.end()

# warning-ignore:unused_argument
func _process(delta):
	if _play:
		if refresh:
			initialise()
		elif initialised:
			update()
	elif initialised:
		clear()
		initialised = false
		refresh = true
