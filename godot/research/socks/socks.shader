shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;
uniform vec4 albedo_main : hint_color;
uniform vec4 albedo_under : hint_color;
uniform float roughness_main : hint_range(0,1);
uniform float roughness_under : hint_range(0,1);
uniform sampler2D texture_albedo : hint_albedo;
uniform sampler2D texture_normal : hint_normal;
uniform float specular;
uniform float metallic;
uniform float point_size : hint_range(0,128);
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform vec3 uv2_scale;
uniform vec3 uv2_offset;
uniform float depth;
uniform float silk_depth;
uniform float silk_power;

void vertex() {
	UV=UV*uv1_scale.xy+uv1_offset.xy;
}

vec4 extract_normal( vec4 color ) {
	return (color * 2.0) - vec4(1.0);
}

void fragment() {
	vec2 base_uv = UV;
	vec4 normmap_c = texture(texture_normal,base_uv);
	vec3 add_norm = ( PROJECTION_MATRIX * extract_normal(normmap_c) ).xyz;
	NORMALMAP = normmap_c.xyz;
	NORMALMAP_DEPTH = depth;
	float ndot = ( 1.0 + dot( normalize( NORMAL + add_norm*silk_depth ), vec3(0,0,1) ) ) * 0.5; //+add_norm*depth
	float mult = pow( ndot, silk_power );
	vec4 albedo_tex = texture(texture_albedo,base_uv);
	ALBEDO = mix( albedo_under.rgb, albedo_main.rgb, mult ) * albedo_tex.rgb;
	METALLIC = metallic;
	ROUGHNESS = mix( roughness_under, roughness_main, mult );
	SPECULAR = specular;
}
