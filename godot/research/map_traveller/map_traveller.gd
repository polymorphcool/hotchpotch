tool

extends Node2D

export (float,0,0.1) var move_speed = 0.1
export (float,0,100) var steer_speed = 0.5
export (bool) var debug = false
export (float,0,0.1) var seek_distance = 0.05
export (float,0,1.6) var seek_angle = 0.5
export (float,1,20) var slope_power = 3

var initialised = false
var map = null
var points = null
var dir = Vector2(0,1)

func initialise():
	map = {
		'size': $map.texture.get_size(),
		'pixels': $map.texture.get_data()
	}
	points = {
		'locations': {
			'current': Vector2(0.2, 0.2),
			'targets': [Vector3(),Vector3(),Vector3()]
		},
		'previous': null,
		'colors': {
			'current': Vector3(),
			'targets': [Vector3(),Vector3(),Vector3()]
		},
		'display': {
			'location': $location,
			'targets': [ $target0, $target1, $target2 ]
		}
	}
	map.pixels.lock()
	initialised = true

func c2vec( c ):
	if c == null:
		return Vector3(0,0,0)
	return Vector3( c.r, c.g, c.b )

func vec2c( v, a = 1 ):
	if v == null:
		return Color(0,0,0,1)
	return Color( v.x, v.y, v.z, a )

func bound( v2 ):
	var out = v2
	while out.x < 0:
		out.x +=1
	while out.x > 1:
		out.x -=1
	while out.y < 0:
		out.y +=1
	while out.y > 1:
		out.y -=1
	return out

func get_color( pos ):
	var pixid = map.size * bound(pos)
	var weight = Vector2( pixid.x - floor(pixid.x), pixid.y - floor(pixid.y) )
	var kernel = [
		[floor(pixid.x), 1 - weight.x],
		[floor(pixid.y), 1 - weight.y],
		[int(ceil(pixid.x)) % int(map.size.x), weight.x],
		[int(ceil(pixid.y)) % int(map.size.y), weight.y]
	]
	var colors = [
		[c2vec(map.pixels.get_pixel( kernel[0][0], kernel[1][0] )), kernel[0][1]+kernel[1][1]],
		[c2vec(map.pixels.get_pixel( kernel[0][0], kernel[3][0] )), kernel[0][1]+kernel[3][1]],
		[c2vec(map.pixels.get_pixel( kernel[2][0], kernel[1][0] )), kernel[2][1]+kernel[1][1]],
		[c2vec(map.pixels.get_pixel( kernel[2][0], kernel[3][0] )), kernel[2][1]+kernel[3][1]]
	]
	var mixed = Vector3(0,0,0)
	var totalw = 0
	for mx in colors:
		mixed += mx[0] * mx[1]
		totalw += mx[1]
	mixed /= totalw
	return mixed

func update_points( delta ):
	if not initialised:
		initialise()
	if points.locations.current == points.previous:
		return
	points.colors.current = get_color( points.locations.current )
	for i in range(0,3):
		points.colors.targets[i] = get_color( points.locations.targets[i] )
	points.previous = points.locations.current
	# seeking best new dir
	var new_dirs = []
	var totalw = 0
	var smin = null
	var smax = null
	for i in range(0,3):
		var d = points.locations.targets[i] - points.locations.current
		var slope = 1 + (points.colors.targets[i].x - points.colors.current.x)
		if smin == null or smin > slope:
			smin = slope
		if smax == null or smax < slope:
			smax = slope
		new_dirs.append( [d.normalized(), slope] )
		totalw += slope
	if totalw == 0 or smin == smax:
		# no need to steer, no best solution
		return
	# slopes normalisation
	totalw = 0
	for i in range(0,3):
		new_dirs[i][1] = pow((new_dirs[i][1] - smin) / (smax-smin), slope_power)
		totalw += new_dirs[i][1]
	var new_dir = Vector2()
	for i in range(0,3):
		new_dir += new_dirs[i][0] * (1 - new_dirs[i][1]/totalw )
	dir = ( dir + new_dir.normalized() * steer_speed * delta ).normalized()

func update_display():
	points.display.location.position = points.locations.current * map.size
	points.display.location.rotation = dir.angle() - PI * 0.5
	for i in range(0,3):
		points.display.targets[i].position = points.locations.targets[i] * map.size
	if debug:
		$color.color = vec2c(points.colors.current)
		$color1.color = vec2c(points.colors.targets[0])
		$color2.color = vec2c(points.colors.targets[1])
		$color3.color = vec2c(points.colors.targets[2])
		$debug/lbl.text = "x: " + str( points.locations.current.x ) + "\n"
		$debug/lbl.text += "y: " + str( points.locations.current.y ) + "\n"
		$debug/lbl.text += str( points.colors.current.x ) + "\n"
		$debug/lbl.text += str( points.colors.current.y ) + "\n"
		$debug/lbl.text += str( points.colors.current.z )

func move( delta ):
	points.locations.current = bound( points.locations.current + dir * move_speed * delta )
	for i in range(0,3):
		var a = 0
		match i:
			0:
				a = seek_angle
			2:
				a = -seek_angle
		var aim = dir.rotated( a )
		points.locations.targets[i] = points.locations.current + aim * seek_distance

func _ready():
	initialise()

func _process(delta):
	if not initialised:
		initialise()
	move( delta )
	update_points( delta )
	update_display()
