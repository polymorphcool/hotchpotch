tool

extends Node2D

export (bool) var _generate : bool = false setget generate
export (float,0,100) var _normal_boost : float = 1
export (float,0,10) var _dot_power : float = 2
export (int,0,20) var _smooth_passes : int = 2
export (Basis) var kernel : Basis = Basis( Vector3( 0.5,1.0,0.5 ), Vector3( 1.0,2.0,1.0 ), Vector3( 0.5,1.0,0.5 ) )

var im : Image = null
var ims : Vector2 = Vector2()
var vec_buf0 : Array = []
var vec_buf1 : Array = []

func constraint( v : Vector2 ):
	if v.x < 0:
		v.x *= -1
	if v.y < 0:
		v.y *= -1
	if v.x >= ims.x:
		v.x = ims.x - (v.x-(ims.x-1))
	if v.y >= ims.y:
		v.y = ims.y - (v.y-(ims.y-1))
	return v

func c2v( var c : Color ):
	return Vector3( c.r, c.g, c.b )

func v2c( var v : Vector3 ):
	return Color(v.x,v.y,v.z)

func px( x : float, y : float ):
	return c2v(im.get_pixel( int(x), int(y) ))

func average( v : Vector2 ):
	var before : Vector2 = constraint( v - Vector2(1,1) )
	var after : Vector2 = constraint( v + Vector2(1,1) )
	var around : Array = [
		# top line, left 2 right
		px( before.x, before.y ),
		px( v.x, before.y ),
		px( after.x, before.y ),
		# center line, left 2 right
		px( before.x, v.y ),
		px( v.x, v.y ),
		px( after.x, v.y ),
		# bottom line, left 2 right
		px( before.x, after.y ),
		px( v.x, after.y ),
		px( after.x, after.y )
	]
	var out = Vector3()
	var mult : float = 0
	var total  : float = 0
	for i in range(0,9):
		match i:
			0:
				mult = kernel.x.x
			1:
				mult = kernel.x.y
			2:
				mult = kernel.x.z
			3:
				mult = kernel.y.x
			4:
				mult = kernel.y.y
			5:
				mult = kernel.y.z
			6:
				mult = kernel.z.x
			7:
				mult = kernel.z.y
			_:
				mult = kernel.z.z
		out += around[i] * mult
		total += mult
	return out / total

func get_surrounding( v : Vector2 ) :
	var before : Vector2 = constraint( v - Vector2(1,1) )
	var after : Vector2 = constraint( v + Vector2(1,1) )
#	return [
#		v2c( average( Vector2( v.x, before.y ) ) ), # top
#		v2c( average( Vector2( after.x, v.y ) ) ), # right
#		v2c( average( Vector2( v.x, after.y ) ) ), # bottom
#		v2c( average( Vector2( before.x, v.y ) ) ) # left
#	]
	return [
		v2c( px( v.x, before.y ) ), # top
		v2c( px( after.x, v.y ) ), # right
		v2c( px( v.x, after.y ) ), # bottom
		v2c( px( before.x, v.y ) ) # left
	]

func smooth_normal( v : Vector2, src : Array ):
	var before : Vector2 = constraint( v - Vector2(1,1) )
	var after : Vector2 = constraint( v + Vector2(1,1) )
	var around : Array = [
		# top line, left 2 right
		src[before.y][before.x],
		src[before.y][v.x],
		src[before.y][after.x],
		# center line, left 2 right
		src[v.y][before.x],
		src[v.y][v.x],
		src[v.y][after.x],
		# bottom line, left 2 right
		src[after.y][before.x],
		src[after.y][v.x],
		src[after.y][after.x],
	]
	var k : Basis = Basis( kernel.x, kernel.y, kernel.z )
	if _dot_power > 0:
		for i in range(0,9):
			if i == 4:
				continue
			var d = pow(abs( around[4].dot( around[i] )), _dot_power )
			match i:
				0:
					k.x.x *= d
				1:
					k.x.y *= d
				2:
					k.x.z *= d
				3:
					k.y.x *= d
				5:
					k.y.z *= d
				6:
					k.z.x *= d
				7:
					k.z.y *= d
				8:
					k.z.z *= d
	var out = Vector3()
	var mult : float = 0
	var total  : float = 0
	for i in range(0,9):
		match i:
			0:
				mult = k.x.x
			1:
				mult = k.x.y
			2:
				mult = k.x.z
			3:
				mult = k.y.x
			4:
				mult = k.y.y
			5:
				mult = k.y.z
			6:
				mult = k.z.x
			7:
				mult = k.z.y
			_:
				mult = k.z.z
		out += around[i] * mult
		total += mult
	return out / total

func generate( b : bool ):
	
	_generate = false
	
	if b:
		
		var now = OS.get_ticks_msec()
		
		im = $heigh.texture.get_data()
		if im.is_compressed():
# warning-ignore:return_value_discarded
			im.decompress()
		ims = im.get_size()
		print( "normal map pass 0 done : source locked" )
		
		var gnormal : Image = Image.new()
		gnormal.create( int(ims.x), int(ims.y), false, im.get_format() )
		gnormal.blend_rect(im, Rect2(Vector2(),ims), Vector2())
		gnormal.bumpmap_to_normalmap(_normal_boost)
		var gtex : ImageTexture = ImageTexture.new()
		gtex.create_from_image( gnormal )
		$normal_godot.texture = gtex
		
		# SMOOTHING PIXELS
#		var normal : Image = Image.new()
##		normal.blend_rect(im, Rect2(Vector2(),ims), Vector2())
##		normal.bumpmap_to_normalmap(_normal_boost)
#		normal.create( int(ims.x), int(ims.y), false, Image.FORMAT_RGBF )
#		normal.lock()
#		for y in range(0,ims.y):
#			for x in range(0,ims.x):
##				var c : Color = im.get_pixel( x, y )
#				var around : Array = get_surrounding( Vector2(x,y) )
#				var dx = (around[1].r - around[3].r) * _normal_boost
#				var dy = (around[0].r - around[2].r) * _normal_boost
#				var norm : Vector3 = Vector3( -dx, -dy, 2.0 ).normalized()
#				normal.set_pixel( x, y, Color( (1+norm.x) * 0.5, (1+norm.y) * 0.5, norm.z ) )
#		normal.unlock()

		# SMOOTHING VECTORS
		im.lock()
		vec_buf0 = []
		vec_buf1 = []
		vec_buf0.resize( ims.y )
		vec_buf1.resize( ims.y )
		for y in range(0,ims.y):
			vec_buf0[y] = []
			vec_buf0[y].resize( ims.x )
			vec_buf1[y] = []
			vec_buf1[y].resize( ims.x )
			for x in range(0,ims.x):
				var around : Array = get_surrounding( Vector2(x,y) )
				var dx = (around[1].r - around[3].r) * _normal_boost
				var dy = (around[0].r - around[2].r) * _normal_boost
				vec_buf0[y][x] = Vector3( -dx, -dy, 2.0 ).normalized()
				vec_buf1[y][x] = vec_buf0[y][x]
		im.unlock()
		print( "normal map pass 1 done : all normals generated (" + str( ims.x*ims.y ) + ")" )
		
		var bsrc = vec_buf0
		var bdst = vec_buf1
		
		for i in range( 0,_smooth_passes ):
			for y in range(0,ims.y):
				for x in range(0,ims.x):
					bdst[y][x] = smooth_normal(Vector2(x,y), bsrc)
			var tmp = bdst
			bdst = bsrc
			bsrc = tmp
			print( "normal map smooth pass " + str(i) )
		print( "normal map pass 2 done : smoothing applied" )
		
		var normal : Image = Image.new()
		normal.create( int(ims.x), int(ims.y), false, Image.FORMAT_RGBF )
		normal.lock()
		for y in range(0,ims.y):
			for x in range(0,ims.x):
				var norm = bdst[y][x]
				normal.set_pixel( x, y, Color( (1+norm.x) * 0.5, (1+norm.y) * 0.5, norm.z ) )
		normal.unlock()
		print( "normal map pass 3 done : image generated" )
		
		var tex : ImageTexture = ImageTexture.new()
		tex.create_from_image( normal )
		$normal.texture = tex
		if $plane.material_override is ShaderMaterial:
			$plane.material_override.set_shader_param("texture_normal",tex)
		elif $plane.material_override is SpatialMaterial:
			$plane.material_override.normal_texture = tex
		
		if $plane_godot.material_override is ShaderMaterial:
			$plane_godot.material_override.set_shader_param("texture_normal",gtex)
		elif $plane_godot.material_override is SpatialMaterial:
			$plane_godot.material_override.normal_texture = gtex
		
		var delta = OS.get_ticks_msec() - now
		print( "normal map generation time " + str( delta / 1000.0 ) + "'" )
