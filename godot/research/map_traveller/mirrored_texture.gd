tool

extends Node2D

export (float, 0, 200) var speed : float = .2
export (bool) var _reset : bool = false setget reset
export (bool) var update_3d : bool = false

var tolerance : float = 1e-5
var im : Image = null
var ims : Vector2 = Vector2()
var trav : Node2D = null
var dir : Vector2 = Vector2(1,0)
var absolute_position : Vector2 = Vector2()
var hid = 0

func reset(var b:bool):
	_reset = false
	if b and trav != null:
		im = null
		dir = Vector2(1,0)

func project( var v:Vector2, var mirror = false ):
	var flipx : bool = false
	var flipy : bool = false
	while v.x < 0:
		v.x += ims.x
		flipx = !flipx
	while v.x >= ims.x:
		v.x -= ims.x
		flipx = !flipx
	while v.y < 0:
		v.y += ims.y
		flipy = !flipy
	while v.y >= ims.y:
		v.y -= ims.y
		flipy = !flipy
	var real_dir = Vector2(1,1)
	if flipx:
		real_dir.x *= -1
		if mirror:
			v.x = ims.x - v.x
	if flipy:
		real_dir.y *= -1
		if mirror:
			v.y = ims.y - v.y
	return [v, real_dir]

func pixel( var v : Vector2 ):
	return c2v(im.get_pixel( int(v.x), int(v.y) ))

func c2v( var c : Color ):
	return Vector3( c.r, c.g, c.b )

func v2c( var v : Vector3 ):
	return Color(v.x,v.y,v.z)

func get_color( var v:Vector2, var mirror = false ):
	
	var minv : Vector2 = Vector2( floor(v.x), floor(v.y) )
	minv = project( minv, mirror )[0]
	if minv.x == ims.x:
		minv.x = 0
	if minv.y == ims.y:
		minv.y = 0
	
	var diff : Vector2 = Vector2(-0.5,-0.5) + ( v - minv ) # constraints in [-0.5,0.5] range
	var pixels : Array = [ 
		minv, # 0: current 
		minv, # 1: horizontal neighbour
		minv, # 2: vertical neighbour
		minv  # 3: opposite (corner)
	]
	
	$debug/lbl.text += "diff: " + str(diff.x) + ", " + str(diff.y) + "\n"
	
	# getting the right quadran
	if (diff-Vector2(0.5,0.5)).length_squared() < tolerance:
		# perfect match!
		return im.get_pixel( int(minv.x), int(minv.y) )
	else:
		# horizontal neighbour
		# on left
		if diff.x < 0:
			pixels[1].x -= 1
			pixels[3].x -= 1
			# inversion of direction
			diff.x *= -1
		# on right
		else:
			pixels[1].x += 1
			pixels[3].x += 1
		# vertical neighbour
		# on top
		if diff.y < 0:
			pixels[2].y -= 1
			pixels[3].y -= 1
			# inversion of direction
			diff.y *= -1
		# on bottom
		else:
			pixels[2].y += 1
			pixels[3].y += 1
	
	# constraining neighbours in texture
	for i in range(1,4):
		var p = pixels[i]
		p = project( p, mirror )[0]
		if p.x == ims.x:
			if mirror:
				p.x = ims.x - 1 
			else:
				p.x = 0
		if p.y == ims.y:
			if mirror:
				p.y = ims.y - 1
			else:
				p.y = 0
		pixels[i] = p
	
	$debug/lbl.text += "pixels:\n\t" + str(minv.x) + ", " + str(minv.y) + "\n"
	$debug/lbl.text += "\t" + str(pixels[1].x) + ", " + str(pixels[1].y) + "\n"
	$debug/lbl.text += "\t" + str(pixels[2].x) + ", " + str(pixels[2].y) + "\n"
	$debug/lbl.text += "\t" + str(pixels[3].x) + ", " + str(pixels[3].y) + "\n"
	
	# rendering horizontal gradients:
	# current to horizontal neighbour
	var hgrad0 = pixel( pixels[0] ) * (1-diff.x)
	hgrad0 += pixel( pixels[1] ) * diff.x
	# vertical neighbour to opposite
	var hgrad1 = pixel( pixels[2] ) * (1-diff.x)
	hgrad1 += pixel( pixels[3] ) * diff.x
	# and rendering vertical gradient and return color
	return v2c( hgrad0 * (1-diff.y) + hgrad1 * diff.y )

func _process(delta):
	
	$debug/lbl.text = ""
	
	if im == null:
		im = $map.texture.get_data()
		if im.is_compressed():
# warning-ignore:return_value_discarded
			im.decompress()
		im.lock()
		ims = im.get_size()
		trav = $trav
		trav.position = ims * .5
		absolute_position = trav.position
		hid = 0
		for i in range(0,$histo.points.size()):
			var x : float = i * ims.x / ($histo.points.size()-1)
			print( i, " > ", x )
			$histo.set_point_position(i, Vector2(x,0))
		$submap.texture = $map.texture
		$sidemap.texture = $map.texture
	
	dir = dir.rotated( rand_range( -1, 1 ) * delta ).normalized()
	absolute_position += dir * speed * delta
	var res = project( absolute_position, true )
	trav.position = res[0]
	trav.rotation = (dir*res[1]).angle()
	
	var c : Color = get_color( res[0], true )
	
	$submap.position = trav.position
	$submap.scale = res[1]
	$submap.texture_offset = absolute_position
	$sidemap.scale = res[1]
	$sidemap.texture_offset = absolute_position
	$sidemap/trav.rotation = trav.rotation
	$color.color = c
	
	$debug/lbl.text += "\nabs: " + str(absolute_position.x) + "," + str(absolute_position.y)
	$debug/lbl.text += "\nrel: " + str(trav.position.x) + "," + str(trav.position.y)
	$debug/lbl.text += "\nflip: " + str(res[1].x) + "," + str(res[1].y)
	
	if hid < $histo.points.size():
		var p = $histo.get_point_position(hid)
		p.y = -c.r * ims.y
		$histo.set_point_position(hid, p)
		hid += 1
	else:
		var vs = PoolVector2Array()
		vs.resize( $histo.points.size() )
		for i in range(0,$histo.points.size()):
			var p = $histo.points[i]
			if i < $histo.points.size()-1:
				p.y = $histo.points[i+1].y
			else:
				p.y = -c.r * ims.y
			vs.set( i, p )
		$histo.points = vs
	
	# positioning 3d ball on map
	if update_3d:
		var plane : PlaneMesh = $w3d/terrain.mesh
		var smallest_step = 1 / (plane.subdivide_width + 1)
		$w3d/terrain.material_override.set_shader_param("uv1_offset", Vector2(-0.5,-0.5) + absolute_position/ims )
		var tscale : Vector3 = $w3d/terrain.global_transform.basis.get_scale()
		var origin : Vector3 = Vector3(0, c.r*tscale.y*$w3d/terrain.material_override.get_shader_param("height"), 0)
		$w3d/trav.global_transform.origin = origin

#		var rot : Vector3 = Vector3(0,dir.angle()-PI*0.5*(res[1].x*res[1].y),0)
#		var mix : float = 5*delta
#		if mix > 1:
#			mix = 1
#		$w3d/trav.rotation = (rot*mix) + $w3d/trav.rotation*(1-mix)

