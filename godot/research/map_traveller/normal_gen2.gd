tool

extends Spatial

export (Texture) var _input_height : Texture = null
export (float,0,1000) var _height : float = 1
export (bool) var _repeat_border : bool = false
export (bool) var _repeat_texture : bool = false
export (bool) var _normal_generate : bool = false setget normal_generate
export (bool) var _normal_clear : bool = false setget normal_clear

var normal_im : Image = null
var normal_ims : Vector2 = Vector2()
var normal_vertices : Array = []

func normal_clear( b : bool ):
	_normal_clear = false
	if b:
		$plane.mesh = null
		var matn : SpatialMaterial = $flat_normal.material_override
		matn.albedo_texture = null
		var mats : ShaderMaterial = $plane_shadows.material_override
		mats.set_shader_param("texture_normal", null)
		var matc : SpatialMaterial = $flat_control.material_override
		matc.albedo_texture = null

func c2v( var c : Color ):
	return Vector3( c.r, c.g, c.b )

func v2c( var v : Vector3 ):
	return Color(v.x,v.y,v.z)

func normal_generate( b : bool ):
	
	_normal_generate = false
	
	if b and _input_height != null:
		
		var now = OS.get_ticks_msec()
		
		normal_im = _input_height.get_data()
		if normal_im.is_compressed():
# warning-ignore:return_value_discarded
			normal_im.decompress()
		normal_im.lock()
		normal_ims = normal_im.get_size()
		# adding a border to the grid
		var gridx : int = int(normal_ims.x+2)
		var gridy : int = int(normal_ims.y+2)
		normal_vertices.clear()
		normal_vertices.resize( gridx*gridy )
		print( "normal_vertices: ", normal_vertices.size() )
		# computing vertices
		for y in range(0,normal_ims.y):
			for x in range(0,normal_ims.x):
				var v = Vector3( (x/normal_ims.x)-0.5, c2v(normal_im.get_pixel( int(x), int(y))).x*_height, (y/normal_ims.y)-0.5 )
				# generating ids in the normal grid, not in image
				var id = (x+1)+(y+1)*gridx
				normal_vertices[id] = v
		normal_im.unlock()
		# filling the gaps
		for y in range(0,gridy):
			for x in range(0,gridx):
				var id = x+y*gridx
				if normal_vertices[id] != null:
					continue
				## top left corner
				if x == 0 and y == 0:
					# position:
					var ref : Vector3 = normal_vertices[1+1*gridx]
					if _repeat_border:
						# bottom right pîxel
						ref.y += normal_vertices[(gridx-2)+(gridy-2)*gridx].y
					normal_vertices[id] = ref + Vector3( -1/normal_ims.x, 0, -1/normal_ims.y )
				## top right corner
				elif x == gridx-1 and y == 0:
					var ref : Vector3 = normal_vertices[(gridx-2)+1*gridx]
					if _repeat_border:
						# bottom left pîxel
						ref.y = normal_vertices[1+(gridy-2)*gridx].y
					normal_vertices[id] = ref + Vector3( 1/normal_ims.x, 0, -1/normal_ims.y )
				## bottom right corner
				elif x == gridx-1 and y == gridy-1:
					var ref : Vector3 = normal_vertices[(gridx-2)+(gridy-2)*gridx]
					if _repeat_border:
						# top left pîxel
						ref.y = normal_vertices[1+gridx].y
					normal_vertices[id] = ref + Vector3( 1/normal_ims.x, 0, 1/normal_ims.y )
				## bottom left corner
				elif x == 0 and y == gridy-1:
					var ref : Vector3 = normal_vertices[1+(gridy-2)*gridx]
					if _repeat_border:
						# top right pîxel
						ref.y = normal_vertices[(gridx-2)+gridx].y
					normal_vertices[id] = ref + Vector3( -1/normal_ims.x, 0, 1/normal_ims.y )
				# all corners are solved, we have to process the edges now
				## top edge
				elif y == 0:
					var ref : Vector3 = normal_vertices[x+gridx]
					if _repeat_border:
						# bottom edge
						ref.y = normal_vertices[x+(gridy-2)*gridx].y
					normal_vertices[id] = ref + Vector3( 0, 0, -1/normal_ims.y )
				## bottom edge
				elif y == gridy-1:
					var ref : Vector3 = normal_vertices[x+(gridy-2)*gridx]
					if _repeat_border:
						# top edge
						ref.y = normal_vertices[x+gridx].y
					normal_vertices[id] = ref + Vector3( 0, 0, 1/normal_ims.y )
				## left edge
				elif x == 0:
					var ref : Vector3 = normal_vertices[1+y*gridx]
					if _repeat_border:
						# right edge
						ref.y = normal_vertices[(gridx-2)+y*gridx].y
					normal_vertices[id] = ref + Vector3( -1/normal_ims.x, 0, 0 )
				## right edge
				elif x == gridx-1:
					var ref : Vector3 = normal_vertices[(gridx-2)+y*gridx]
					if _repeat_border:
						# left edge
						ref.y = normal_vertices[1+y*gridx].y
					normal_vertices[id] = ref + Vector3( 1/normal_ims.x, 0, 0 )
				## should NEVER comes here
				else:
					print( "inconsistency in normal_generate, await messy results" )
					normal_vertices[id] = Vector3()
		
		# adding standard vertices
		var surf : SurfaceTool = SurfaceTool.new()
		surf.begin(Mesh.PRIMITIVE_TRIANGLES)
		surf.add_smooth_group(true)
		for y in range(0,gridy):
			for x in range(0,gridx):
				var id : int = int(x+y*gridx)
				surf.add_uv( Vector2(normal_vertices[id].x,normal_vertices[id].z) + Vector2(0.5,0.5) )
				surf.add_vertex( normal_vertices[id] * Vector3(10,1,10) )
		
		# registering standard faces
		for y in range(0,gridy-1):
			for x in range(0,gridx-1):
				var id0 : int = int(x+y*gridx)
				var id1 : int = int((x+1)+y*gridx)
				var id2 : int = int((x+1)+(y+1)*gridx)
				var id3 : int = int(x+(y+1)*gridx)
				surf.add_index(id0)
				surf.add_index(id1)
				surf.add_index(id2)
				surf.add_index(id2)
				surf.add_index(id3)
				surf.add_index(id0)
		
		surf.generate_normals()
		surf.generate_tangents()
		var mesh : Mesh = Mesh.new()
# warning-ignore:return_value_discarded
		surf.commit( mesh )
		surf.clear()
		surf.unreference()
		var arr : Array = mesh.surface_get_arrays(0)
		var norms : PoolVector3Array = arr[Mesh.ARRAY_NORMAL]
		print( "Mesh.ARRAY_NORMAL: ", norms.size() )
		# encoding normals in a map
		var normal : Image = Image.new()
		if _repeat_texture:
			# we will create a 2x2 version of the map with flipped normals
			normal.create( int(normal_ims.x*2), int(normal_ims.y*2), false, Image.FORMAT_RGBF )
			normal.lock()
			for y in range(0,normal_ims.y):
				for x in range(0,normal_ims.x):
					var id : int = int((x+1)+(y+2)*gridx)
					var norm : Vector3 = norms[id]
					var n : Vector3 = Vector3( (1+norm.x)*.5, 1 - (1+norm.z)*0.5, norm.y )
					# standard normal
					normal.set_pixel( int(x), int(y), Color(n.x,n.y,n.z) )
					# x flipped
					normal.set_pixel( int((normal_ims.x*2-1)-x), int(y), Color(1.0-n.x,n.y,n.z) )
					# y flipped
					normal.set_pixel( int(x), int((normal_ims.y*2-1)-y), Color(n.x,1.0-n.y,n.z) )
					# x & y flipped
					normal.set_pixel( int((normal_ims.x*2-1)-x), int((normal_ims.y*2-1)-y), Color(1.0-n.x,1.0-n.y,n.z) )
			normal.unlock()
		else:
			normal.create( int(normal_ims.x), int(normal_ims.y), false, Image.FORMAT_RGBF )
			normal.lock()
			for y in range(0,normal_ims.y):
				for x in range(0,normal_ims.x):
					var id : int = int((x+1)+(y+1)*gridx)
					var norm : Vector3 = norms[id]
					var n : Vector3 = Vector3( (1+norm.x)*.5, 1 - (1+norm.z)*0.5, norm.y )
					normal.set_pixel( x, y, Color(n.x,n.y,n.z) )
			normal.unlock()
		
		$plane.mesh = mesh
		
		var tex : ImageTexture = ImageTexture.new()
		tex.create_from_image( normal )
		var math : SpatialMaterial = $flat_height.material_override
		math.albedo_texture = _input_height
		var matn : SpatialMaterial = $flat_normal.material_override
		matn.albedo_texture = tex
		var mats : ShaderMaterial = $plane_shadows.material_override
		mats.set_shader_param("texture_normal", tex)
		mats.set_shader_param("normal_scale", _height)
		
		var control : Image = Image.new()
		control.create( int(normal_ims.x), int(normal_ims.y), false, normal_im.get_format() )
		control.blend_rect(normal_im, Rect2(Vector2(),normal_ims), Vector2())
		control.bumpmap_to_normalmap(_height)
		var texc : ImageTexture = ImageTexture.new()
		texc.create_from_image( control )
		var matc : SpatialMaterial = $flat_control.material_override
		matc.albedo_texture = texc
		
		var delta = OS.get_ticks_msec() - now
		print( "normal map generation time " + str( delta / 1000.0 ) + "'" )
