tool

extends Node2D

func c2v( var c : Color ):
	return Vector3( c.r, c.g, c.b )

func v2c( var v : Vector3 ):
	return Color(v.x,v.y,v.z)

func _process(delta):
	
	var d2c = ($cell0.position + Vector2(50,50)) - $trav.position
	d2c.x = abs( d2c.x )
	d2c.y = abs( d2c.y )
	d2c /= 100
	
	var colors : Array = [ 
		c2v( $cell0.color ),
		c2v( $cell1.color ),
		c2v( $cell2.color ),
		c2v( $cell3.color ) ]
	
	var mix0 = colors[0] * (1-d2c.x) + colors[1] * d2c.x
	var mix1 = colors[3] * (1-d2c.x) + colors[2] * d2c.x

	$result.color = v2c( mix0 * (1-d2c.y) + mix1 * d2c.y )
	
	$debug.text = "distance: " + str( d2c.x ) + ", " + str( d2c.y ) + "\n"	
