# This script creates a procedural weather map for clouds.

tool
extends Node

signal cycle_finished # When the weather map is finished blending to the next weather

enum {COPY_BUFFER, WEATHER_BUFFER, BLEND_BUFFER}
const TEXTURE_SIZE = 512

export var blend_time = 60.0 # The time it takes to blend from the previous weather map to the next (in seconds barring lag)

var _time = 1.0 # The current time inbetween transitioning
var blend_mat : ShaderMaterial = null
var disable_renderer = false

func get_vp(ID):
	return get_child(ID).get_child(0)

func get_mat(ID):
	return get_vp(ID).get_child(0).material

func _ready():
	
	get_vp(COPY_BUFFER).size = Vector2(TEXTURE_SIZE, TEXTURE_SIZE)
	get_vp(WEATHER_BUFFER).size = Vector2(TEXTURE_SIZE, TEXTURE_SIZE)
	get_vp(BLEND_BUFFER).size = Vector2(TEXTURE_SIZE, TEXTURE_SIZE)
	
	# setting viewports
	# copy texture
	var cp_mat = get_mat(COPY_BUFFER)
	cp_mat.set_shader_param("tex", get_vp(WEATHER_BUFFER).get_texture())
	# blend textures
	blend_mat = get_mat(BLEND_BUFFER)
	blend_mat.set_shader_param("tex_1", get_vp(COPY_BUFFER).get_texture())
	blend_mat.set_shader_param("tex_2", get_vp(WEATHER_BUFFER).get_texture())
	
	var next_random = Vector3(rand_range(-1000,1000), rand_range(-1000,1000), randf() * 1.5 - 0.2)
	var weather_mat = get_mat(WEATHER_BUFFER)
	weather_mat.set_shader_param("randomness", next_random)
	
	get_vp(COPY_BUFFER).render_target_update_mode = Viewport.UPDATE_ONCE
	yield(get_tree(), "idle_frame")
	get_vp(COPY_BUFFER).render_target_update_mode = Viewport.UPDATE_DISABLED
	
	get_vp(WEATHER_BUFFER).render_target_update_mode = Viewport.UPDATE_ALWAYS
	yield(get_tree(), "idle_frame")
	get_vp(WEATHER_BUFFER).render_target_update_mode = Viewport.UPDATE_DISABLED

func _process(delta: float):
	
	if _time >= 1.0: # If the blending is complete,
		_time = 0.0 # Reset the time,
		emit_signal("cycle_finished")
		
		# Should be the same code as in the _ready function
		var next_random := Vector3(rand_range(-1000,1000), rand_range(-1000,1000), randf() * 1.5 - 0.2)
		var weather_mat: ShaderMaterial = get_mat(WEATHER_BUFFER)
		weather_mat.set_shader_param("randomness", next_random)
		
		get_vp(COPY_BUFFER).render_target_update_mode = Viewport.UPDATE_ONCE
		yield(get_tree(), "idle_frame")
		get_vp(COPY_BUFFER).render_target_update_mode = Viewport.UPDATE_DISABLED
		
		get_vp(WEATHER_BUFFER).render_target_update_mode = Viewport.UPDATE_ONCE
		yield(get_tree(), "idle_frame")
		get_vp(WEATHER_BUFFER).render_target_update_mode = Viewport.UPDATE_DISABLED
		disable_renderer = true
	
	blend_mat.set_shader_param("blend", _smoothstep(_time))
	if blend_time > 0.0:
		_time += delta / blend_time

func get_texture() -> ViewportTexture:
	var weather_map: ViewportTexture = get_vp(BLEND_BUFFER).get_texture()
#	var weather_map: ViewportTexture = get_vp(WEATHER_BUFFER).get_texture()
	weather_map.flags = Texture.FLAGS_DEFAULT
	return weather_map

func _smoothstep(x: float) -> float:
	var c_x := clamp(x, 0.0, 1.0)
	return c_x * c_x * (3.0 - 2.0 * c_x)
