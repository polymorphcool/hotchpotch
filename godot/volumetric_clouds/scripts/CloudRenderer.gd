tool
extends MeshInstance

export (Vector2) var _wind = Vector2(0,0) setget wind
export (float,0,1000) var _blend_time = 10.0 setget blend_time
export (bool) var _new_worley = false setget new_worley

func wind( v2 ):
	_wind = v2

func blend_time(f):
	_blend_time = f
	if $weather != null:
		$weather.blend_time = _blend_time

func new_worley( b = true ):
	if b and $vpc_worley/worley_texture != null:
		$vpc_worley/worley_texture.make_random_points()

var mat = null
var _weather : Node
var wind_accum = Vector2(0,0)

func _ready():
	$weather.blend_time = _blend_time
	# seeting render textures
	mat = get_surface_material(0)
	mat.set_shader_param("weather_map", $weather.get_texture())
	mat.set_shader_param("worley", $vpc_worley/worley_texture.get_texture())

func set_sun_energy(new_energy: float):
	mat.set_shader_param("sun_energy", new_energy)

func set_sun_direction(new_direction: Vector3):
	mat.set_shader_param("sun_direction", new_direction)

func _process( delta ):
	wind_accum += _wind
	mat.set_shader_param("wind", wind_accum)
