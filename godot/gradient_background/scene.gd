extends Node2D

export (float,0,10) var speed = 1
export (float,0,1) var cube_alpha = 0.3 setget set_cube_alpha

export(float,0,1) var _noise_amount = null setget noise_amount
export(Shader) var _gradient_shader = null setget gradient_shader

func noise_amount( f = null ):
	if f == null:
		return $vp0/bg.material.get_shader_param( "noise_amount" )
	_noise_amount = f
	$vp0/bg.material.set_shader_param( "noise_amount", f )

func gradient_shader( s = null ):
	if s == null:
		return _gradient_shader
	_gradient_shader = s
	$vp0/bg.material.shader = s

onready var blank = load( "res://assets/textures/blank_8x8.png" )

var vps = null
var configs = []
var angl = 0
var angl_offset = PI * -0.5
var mix_color = Color(0.93,0.93,0.93,0)

var c_current = 0
var c_next = 1
var use_solid = false
var mp = null
var mouse_enabled = false
var orbit = Vector3(0,0,0)
var orbit_target = Vector3(0,0,0)

func set_cube_alpha( a ):
	cube_alpha = a
	mix_color.a = cube_alpha

func _ready():
	
	get_viewport().connect( "size_changed", self, "vp_size" )
	vp_size()
	for c in $configs.get_children():
		configs.append( c.get_config() )
	toggle_solid()
	
	if _gradient_shader == null:
		_gradient_shader = $vp0/bg.material.shader
	
	# listening UI
	glob.connect( "HP_menu_open", self, "menu_open" )
	glob.connect( "HP_menu_close", self, "menu_close" )

func menu_open():
	Input.set_custom_mouse_cursor(null)
	mouse_enabled = false

func menu_close():
	Input.set_custom_mouse_cursor(blank)
	mouse_enabled = true

func toggle_solid():
	use_solid = !use_solid
	if use_solid:
		$root_3d/cube.change_mesh( $meshes/solid.mesh )
		$root_3d/cube.change_mat( $meshes/solid.material_override )
	else:
		$root_3d/cube.change_mesh( $meshes/wireframe.mesh )
		$root_3d/cube.change_mat( $meshes/wireframe.material_override )

func vp_size():
	if vps == get_viewport().size:
		return
	vps = get_viewport().size
	$vp0.size = vps
	$vp0/bg.scale = vps

func _input(event):
	
	if event is InputEventMouseButton:
		if mouse_enabled and event.is_pressed() and event.button_index == BUTTON_LEFT:
			$root_3d/cube.cancel_rotation()

func _process(delta):
	
	var mp_delta = Vector2(0,0)
	var mpn = get_viewport().get_mouse_position() / get_viewport().size
	if mp != mpn:
		if mp != null and mouse_enabled:
			mp_delta = mpn - mp
		mp = mpn
		
	orbit_target += Vector3( mp_delta.y, mp_delta.x, 0 )
	orbit += ( orbit_target - orbit ) * delta
	$root_3d/cube.add_rotation( ( orbit_target - orbit ) * 0.01, 1.0001 )
	
	
	angl += delta * speed
	while( angl > PI ):
		angl -= PI
		var l = len( configs )
		c_current = (c_current + 1) % l
		c_next = (c_current + 1) % l
		if c_current == 0:
			toggle_solid()
	var mix = ( 1 + sin(angl + angl_offset) ) * 0.5
	var xim = 1 - mix
	var cmix = {}
	for k in configs[c_current]:
		cmix[k] = configs[c_current][k] * xim + configs[c_next][k] * mix
	$vp0/bg.set_config( cmix )
	if not use_solid:
		$root_3d/cube.material_override.albedo_color = cmix.color_halo * mix_color
