extends MeshInstance

onready var speed = Vector3( rand_range(-0.4,0.38), rand_range(-0.049,0.05), rand_range(-0.01,0.011) )
var speed_variation = 0.08
var init_rot = Vector3(0,5.6,0)
var target_rot = null
var rot_to_0 = false

func _ready():
	rotation_degrees = init_rot
	scale = Vector3(1,1,1) * 1.075
	pass

func set_target_rot( r ):
	target_rot = r
	for c in get_children():
		c.set_target_rot( r )

func change_mesh( m ):
	mesh = m
	for c in get_children():
		c.change_mesh( m )

func change_mat( m ):
	material_override = m
	for c in get_children():
		c.change_mat( m )

func add_rotation( rads, decay ):
	rotation += rads
	var r = rads * decay
	for c in get_children():
		c.add_rotation( r, decay )

func cancel_rotation():
	rot_to_0 = true
	for c in get_children():
		c.cancel_rotation()

func _process(delta):
	
	if target_rot != null:
		speed = ( target_rot -  rotation_degrees ) * delta
		rotation_degrees += speed
	else:
		rotation_degrees += delta * speed
	
	if rot_to_0:
		rotation_degrees -= rotation_degrees * 6 * delta
		if abs(rotation_degrees.x) < 1e-2 and abs(rotation_degrees.y) < 1e-2 and abs(rotation_degrees.z) < 1e-2:
			rot_to_0 = false
	
	speed += Vector3( 
			rand_range(-1,1),
			rand_range(-1,1),
			rand_range(-1,1) 
			) * speed_variation * rand_range(-1,1) * delta
