tool

extends Polygon2D

export(Color) var color_border = Color(1,0,0,1) setget _color_border
export(Color) var color_center = Color(0,1,0,1) setget _color_center
export(Color) var color_halo = Color(0,0,0,0) setget _color_halo
export(float, -1, 2) var center = 0.5 setget _center
export(float, 0, 1) var center_thickness = 0 setget _center_thickness
export(float, 0.001, 1.5) var gradient = 0.25 setget _gradient
export(float, 0.001, 2) var power = 0.2 setget _power
export(float, -1, 2) var halo_center = 0.5 setget _halo_center
export(float, 0, 2) var halo_size = 0.2 setget _halo_size
export(float, 0, 50) var halo_power = 1 setget _halo_power
export(float, -1, 1) var halo_stretch = 0.7 setget _halo_stretch
export(float, 0, 1) var halo_strength = 1.0 setget _halo_strength
export(float, 0, 1) var uv_offset_range = 0 setget _uv_offset_range

func get_config():
	return {
		"color_border": color_border,
		"color_center": color_center,
		"color_halo": color_halo,
		"center": center,
		"center_thickness": center_thickness,
		"gradient": gradient,
		"power": power,
		"halo_center": halo_center,
		"halo_size": halo_size,
		"halo_power": halo_power,
		"halo_stretch": halo_stretch,
		"halo_strength": halo_strength,
		"uv_offset_range": uv_offset_range
	}

func set_config( conf ):
	for k in conf:
		if k == "color_border":
			_color_border(conf[k])
		elif k == "color_center":
			_color_center(conf[k])
		elif k == "color_halo":
			_color_halo(conf[k])
		elif k == "center":
			_center(conf[k])
		elif k == "center_thickness":
			_center_thickness(conf[k])
		elif k == "gradient":
			_gradient(conf[k])
		elif k == "power":
			_power(conf[k])
		elif k == "halo_center":
			_halo_center(conf[k])
		elif k == "halo_size":
			_halo_size(conf[k])
		elif k == "halo_power":
			_halo_power(conf[k])
		elif k == "halo_stretch":
			_halo_stretch(conf[k])
		elif k == "halo_strength":
			_halo_strength(conf[k])
		elif k == "uv_offset_range":
			_uv_offset_range(conf[k])

func _color_border( c ):
	color_border = c
	material.set_shader_param( "color_border", color_border )

func _color_center( c ):
	color_center = c
	material.set_shader_param( "color_center", color_center )

func _color_halo( c ):
	color_halo = c
	material.set_shader_param( "color_halo", color_halo )

func _center( f ):
	center = f
	material.set_shader_param( "center", center )

func _center_thickness( f ):
	center_thickness = f
	material.set_shader_param( "center_thickness", center_thickness )

func _gradient( f ):
	gradient = f
	material.set_shader_param( "gradient", gradient )

func _power( f ):
	power = f
	material.set_shader_param( "power", power )

func _halo_center( f ):
	halo_center = f
	material.set_shader_param( "halo_center", halo_center )

func _halo_size( f ):
	halo_size = f
	material.set_shader_param( "halo_size", halo_size )

func _halo_power( f ):
	halo_power = f
	material.set_shader_param( "halo_power", halo_power )

func _halo_stretch( f ):
	halo_stretch = f
	material.set_shader_param( "halo_stretch", halo_stretch )

func _halo_strength( f ):
	halo_strength = f
	material.set_shader_param( "halo_strength", halo_strength )

func _uv_offset_range(f):
	uv_offset_range = f

func apply():
	material.set_shader_param( "color_border", color_border )
	material.set_shader_param( "color_center", color_center )
	material.set_shader_param( "color_halo", color_halo )
	material.set_shader_param( "center", center )
	material.set_shader_param( "center_thickness", center_thickness )
	material.set_shader_param( "gradient", gradient )
	material.set_shader_param( "power", power )
	material.set_shader_param( "halo_center", halo_center )
	material.set_shader_param( "halo_size", halo_size )
	material.set_shader_param( "halo_power", halo_power )
	material.set_shader_param( "halo_stretch", halo_stretch )
	material.set_shader_param( "halo_strength", halo_strength )
	
func _ready():
	apply()

#warning-ignore:unused_argument
func _process(delta):
	material.set_shader_param( "uv_offset", Vector2( rand_range(0,uv_offset_range), rand_range(0,uv_offset_range) ) )
