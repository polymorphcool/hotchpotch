extends MeshInstance

func _process(delta):
	rotate_x( delta * 0.1 )
	rotate_y( delta * 0.5 )
