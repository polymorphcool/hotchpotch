extends Node2D

signal splash_close

onready var logo_fadeout_duration = 2
onready var logo_fadeout = null

var vps = null
var scroll_size = null
var scroll_size_target = null
var buttons = null

func _ready():
	# center on screen
	vp_size()
	get_viewport().connect( "size_changed", self, "vp_size" )
	# starting logo fadeout
	logo_fadeout = logo_fadeout_duration
	# adding credits
	glob.load_credits( $welcome/columns/scroll/lyt )
	# linking buttons
	sort_buttons()

func sort_buttons():
	if buttons == null:
		buttons = []
		for c in $welcome/columns/btns.get_children():
			if c.name == "exit":
				c.connect( "pressed", self, "exit_pressed" )
			else:
				c.connect( "pressed", self, "close_pressed" )
			buttons.append( c )
	else:
		var bts = buttons.duplicate()
		for b in buttons:
			$welcome/columns/btns.remove_child( b )
		while bts.size() > 0:
			var bid = int( rand_range( 0, bts.size() ) )
			if bid > bts.size():
				bid = 0
			var b = bts[ bid ]
			bts.remove( bid )
			$welcome/columns/btns.add_child( b )

func open():
	self.visible = true
	scroll_size = null
	vp_size()
	sort_buttons()

func exit_pressed():
	get_tree().quit()

func close_pressed():
	emit_signal( "splash_close" )
	self.visible = false

func vp_size():
	vps = get_viewport().size
	position = vps * 0.5
	$welcome.rect_position = Vector2( -$welcome/columns/scroll.rect_size.x * 0.5, vps.y * 0.5 )
	if scroll_size == null:
		$welcome/columns/scroll.rect_size.y = 0
		scroll_size = $welcome/columns/scroll.rect_size
	scroll_size_target = Vector2( $welcome/columns/scroll.rect_size.x, vps.y * 2 / 3 )

func _process(delta):

	if logo_fadeout != null:
		logo_fadeout -= delta
		if logo_fadeout < 0:
			logo_fadeout = 0
		$logo.material.set_shader_param( "alpha", logo_fadeout / logo_fadeout_duration )
		if logo_fadeout == 0:
			logo_fadeout = null
	
	if scroll_size != scroll_size_target and delta < 0.1:
		var diff = glob.int_speed( ( scroll_size_target - scroll_size ) * 5 * delta )
		scroll_size += diff
		$welcome.rect_position.y = vps.y * 0.5 - scroll_size.y
		$welcome/columns/scroll.rect_size = scroll_size
		
	
