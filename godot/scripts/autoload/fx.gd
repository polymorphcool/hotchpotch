extends Node

enum FX_TYPE {
	_undefined,
	_real_linear,
	_real_slingshot
}

func fx_real( real, damping = 5 ):
	return {
		'pause': false,
		'init': real,
		'current': real,
		'target': real,
		'speed': 0,
		'damping': damping,
		'delta': 1e-3,
		'fx': []
	}

func fx_send( data, val ):
	data.pause = false
	data.current = val
	data.speed = 0

func fx_restart( data ):
	data.current = data.init
	data.target = data.target
	data.dir = 0
	data.speed = 0

func fx_add( data, type ):
	data.fx.append( type )

func __real_slingshot( d, delta ):
	if d.current != d.target:
		d.speed += delta
		if d.speed > 1:
			d.speed = 1
		d.current += (d.target-d.current) * d.speed * d.damping * delta
		if abs( d.current - d.target ) < d.delta:
			d.current = d.target
			d.speed = 0
		return true
	return false

func __real_linear( d, delta ):
	if d.current != d.target:
		d.current += (d.target-d.current) * d.damping * delta
		if abs( d.current - d.target ) < d.delta:
			print( "__real_linear stopped" )
			d.current = d.target
		return true
	return false

func update( data, delta ):
	if data.pause:
		return false
	var ret = false
	for t in data.fx:
		match(t):
			FX_TYPE._real_linear:
				ret = ret || __real_linear( data, delta )
			FX_TYPE._real_slingshot:
				ret = ret || __real_slingshot( data, delta )
			_:
				print( "unknow fx...", t )
	return ret
