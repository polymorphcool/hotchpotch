extends Node

onready var fx = get_node("/root/fx")

enum HP_TYPE {
	_undefined,
	_bool,
	_int,
	_real,
	_color,
	_shader,
	_texture,
	_group,
	_label,
	_func,
	_list,
	_vec3,
	_vec2
}

enum HP_LEVEL {
	_undefined,
	_wip,
	_debug,
	_release
}

###############
# SCREEN DATA #
###############

# move credits and configuration in root node of each scene
onready var credits = [
	{
		'text': 'HOTCHPOTCH\na collection of interactive and/or configurable screens doing useless yet cool things',
		'url': 'https://gitlab.com/polymorphcool/hotchpotch',
		'license': 'LGPL + CC0'
	},
	{
		'text': 'published by polymorph.cool',
		'url': 'http://polymorph.cool',
	},
	{
		'type': 'tool',
		'text': 'made with Godot Engine',
		'url': 'http://godotengine.org',
		'license': 'MIT'
	},
	{ 
		'type': 'graphic',
		'text': 'Polymorph logo', 
		'url': 'https://fontlibrary.org/en/font/fifteentwenty',
		'license': 'all rights reserved'
	},
	{ 
		'type': 'font',
		'text': 'Belgika by OSP', 
		'url': 'https://fontlibrary.org/en/font/belgica-belgika',
		'license': 'OFL'
	},
	{ 
		'type': 'font',
		'text': 'FifteenTwenty by Stewart C. Russell', 
		'url': 'https://fontlibrary.org/en/font/fifteentwenty',
		'license': 'CC-0'
	},
	{ 
		'type': 'texture',
		'text': 'Spruit Sunrise by Greg Zaal', 
		'url': 'https://hdrihaven.com/hdri/?h=spruit_sunrise',
		'license': 'CC-0'
	},
	{ 
		'type': 'texture',
		'text': 'Royal Esplanade by Greg Zaal', 
		'url': 'https://hdrihaven.com/hdri/?h=royal_esplanade',
		'license': 'CC-0'
	},
	{ 
		'type': 'texture',
		'text': 'heightmap by Alvaro Martin', 
		'url': 'http://www.alvaromartin.net/images/surfaceclipmaps/heightmap.jpg',
	},
	{ 
		'type': 'texture',
		'text': 'terrain.htmlcd_Image18 by Advanced Distributed Learning (ADL)', 
		'url': 'https://sandboxdocs.readthedocs.io/en/latest/tutorials/images/terrain/terrain.htmlcd_Image18.png',
	},
	{ 
		'type': 'texture',
		'text': 'Brushed Metal With Rusty Spots', 
		'url': 'https://everytexture.com/everytexture-com-stock-metal-texture-00001/',
	},
	{ 
		'type': 'texture',
		'text': 'White-Marble-01-Normal - Seamless by Good Textures', 
		'url': 'https://www.goodtextures.com/image/19661/white-marble-01-normal-seamless',
	},
	{ 
		'type': 'texture',
		'text': 'Seamless Red Marble + (Maps) by Seme Design Lab', 
		'url': 'http://www.texturise.club/2013/08/seamless-red-marble-maps.html',
	},
	{ 
		'type': '3d model',
		'text': 'Fish Perch by holmen', 
		'url': 'https://www.blendswap.com/blend/8888',
		'license': 'CC-0'
	},
	{ 
		'type': 'texture',
		'text': 'Iris of the Eye by Filter Forge', 
		'url': 'https://www.flickr.com/photos/filterforge/8776654163',
		'license': 'CC by 2.0'
	}
]
onready var screen_list = [
	{
		'ID': -1, # will be set by script
		'level': HP_LEVEL._undefined,
		'name': 'raymarching',
		'scene': 'research/worley-shader/Main.tscn',
		'thumb': 'research/worley-shader/icon.png',
		'type': 'shader',
		'date': '2020.04.03',
		'authors': 'frankiezafe',
		'interactive': false,
		'description': 'raymarching',
		'configuration': {
			'raymarch shader': { 
				'type': HP_TYPE._shader,
				'getter': 'raymarch', 
				'setter': 'raymarch'
			}
		}
	},
	{
		'ID': -1, # will be set by script
		'level': HP_LEVEL._debug,
		'name': 'wireframer',
		'scene': 'wireframe/wireframe.tscn',
		'thumb': 'wireframe/screenshot.jpg',
		'type': 'geometry',
		'date': '2020.04.24',
		'authors': 'frankiezafe',
		'interactive': false,
		'description': 'a tool to generate wireframe version of meshes',
		'configuration': {
			'controls': {
				'type': HP_TYPE._group, 
				'data': {
					'commands and display': {
						'type': HP_TYPE._group, 
						'data': {
							'commands': {
								'type': HP_TYPE._list,
								'columns': 3,
								'getter': 'get_commands',
								'setter': 'load_command',
							},
							'select mesh': {
								'type': HP_TYPE._list,
								'columns': 3,
								'getter': 'get_mesh_list',
								'setter': 'load_mesh_name',
							},
							'adjust display': {
								'type': HP_TYPE._label
							},
							'show original': { 
								'type': HP_TYPE._bool,
								'getter': 'show_original',
								'setter': 'show_original'
							},
							'show floor': { 
								'type': HP_TYPE._bool,
								'getter': 'show_floor',
								'setter': 'show_floor'
							}
						}
					},
					'algorithm': {
						'type': HP_TYPE._group, 
						'data': {
							'optimise': { 
								'type': HP_TYPE._bool,
								'getter': 'optimise',
								'setter': 'optimise'
							},
							'stroke': { 
								'type': HP_TYPE._real, 
								'range': [0,0.1],
								'range_auto': true,
								'getter': 'stroke',
								'setter': 'stroke'
							},
							'stroke multiply': { 
								'type': HP_TYPE._vec3, 
								'range': [-1,1],
								'range_auto': true,
								'getter': 'stroke_multiply',
								'setter': 'stroke_multiply'
							},
							'drop': { 
								'type': HP_TYPE._real, 
								'range': [0,1],
								'range_auto': false,
								'getter': 'drop',
								'setter': 'drop'
							},
							'overflow': { 
								'type': HP_TYPE._real, 
								'range': [-1,1],
								'range_auto': true,
								'getter': 'overflow',
								'setter': 'overflow'
							},
							'gap': { 
								'type': HP_TYPE._real, 
								'range': [0,1],
								'range_auto': false,
								'getter': 'gap',
								'setter': 'gap'
							},
							'grow': { 
								'type': HP_TYPE._real, 
								'range': [0,1],
								'range_auto': true,
								'getter': 'grow',
								'setter': 'grow'
							},
							'solidify': { 
								'type': HP_TYPE._bool,
								'getter': 'solidify',
								'setter': 'solidify'
							},
							'caps': { 
								'type': HP_TYPE._bool,
								'getter': 'caps',
								'setter': 'caps'
							}
						}
					}
				}
			}
		}
	},
	{
		'ID': -1, # will be set by script
		'level': HP_LEVEL._release,
		'name': 'metal fish',
		'scene': 'research/curvature/simple.tscn',
		'thumb': 'research/curvature/chrome.jpg',
		'type': 'shader',
		'date': '2020.04.22',
		'authors': 'frankiezafe',
		'interactive': false,
		'description': 'tech demo of curvature shader developped for pink ocean\nX and Y curvature are configurable, as well as inflate ratio',
		'configuration': {
			'parameters': {
				'type': HP_TYPE._group, 
				'data': {
					'view': {
						'type': HP_TYPE._group, 
						'data': {
							'rotation': { 
								'type': HP_TYPE._real, 
								'range': [-3.14,3.14],
								'range_auto': false,
								'getter': 'cam_rot',
								'setter': 'cam_rot',
								'fx': fx.FX_TYPE._real_slingshot 
							},
							'rotor': { 
								'type': HP_TYPE._real, 
								'range': [-1,1],
								'range_auto': false,
								'getter': 'cam_rotor',
								'setter': 'cam_rotor',
								'fx': fx.FX_TYPE._real_slingshot 
							},
							'camera': {
								'type': HP_TYPE._label
							},
							'distance': { 
								'type': HP_TYPE._real, 
								'range': [5,30],
								'range_auto': false,
								'getter': 'cam_dist',
								'setter': 'cam_dist'
							},
							'fov': { 
								'type': HP_TYPE._real, 
								'default': 25, 
								'range': [1,179],
								'range_auto': false,
								'getter': 'cam_fov',
								'setter': 'cam_fov'
							}
						}
					},
					'fish': {
						'type': HP_TYPE._group, 
						'data': {
							'curvature x': { 
								'type': HP_TYPE._real, 
								'default': 0, 
								'range': [-3,3],
								'range_auto': true,
								'getter': 'curvature_x',
								'setter': 'curvature_x' },
							'curvature y': { 
								'type': HP_TYPE._real, 
								'default': 0, 
								'range': [-3,3],
								'range_auto': true,
								'getter': 'curvature_y',
								'setter': 'curvature_y' },
							'accel x': { 
								'type': HP_TYPE._real, 
								'default': 0, 
								'range': [-4,4],
								'range_auto': true,
								'getter': 'accel_x',
								'setter': 'accel_x' },
							'accel y': { 
								'type': HP_TYPE._real, 
								'default': 0, 
								'range': [-4,4],
								'range_auto': true,
								'getter': 'accel_y',
								'setter': 'accel_y' },
							'inflate': { 
								'type': HP_TYPE._real, 
								'default': 0, 
								'range': [0,1],
								'range_auto': true,
								'getter': 'inflate',
								'setter': 'inflate' },
							'scratches': { 
								'type': HP_TYPE._real, 
								'range': [-2,2],
								'range_auto': true,
								'getter': 'scratches',
								'setter': 'scratches' }
						}
					}
				}
			},
			'curvature shader': { 
				'type': HP_TYPE._shader,
				'getter': 'fish_shader', 
				'setter': 'fish_shader' }
		}
	},
	{
		'ID': -1, # will be set by script
		'level': HP_LEVEL._wip,
		'name': 'pink ocean',
		'scene': 'fish/scene.tscn',
		'thumb': 'fish/screenshot.jpg',
		'type': 'geometry',
		'date': '2018.01.01',
		'authors': 'frankiezafe',
		'interactive': true,
		'description': 'a lazy fish gently swimming along with whales and other mammals'
	},
	{
		'ID': -1, # will be set by script
		'level': HP_LEVEL._undefined,
		'name': 'ultra slow',
		'scene': 'heightmap/ultra_slow.tscn',
		'thumb': 'heightmap/ultra_slow.jpg',
		'type': 'heightmap',
		'date': '2020.04.03',
		'authors': 'frankiezafe',
		'interactive': false,
		'description': 'a\n landscape\n that\n leaves\n trails\n in\n the\n sky\n while wandering above it',
	},
	{
		'ID': -1, # will be set by script
		'level': HP_LEVEL._wip,
		'name': 'speedster',
		'scene': 'heightmap/speedster.tscn',
		'thumb': 'heightmap/speedster.jpg',
		'type': 'heightmap',
		'date': '2020.04.03',
		'authors': 'frankiezafe',
		'interactive': false,
		'description': 'flying in a speedster in a icy desert, soon you will be able to drive it',
		'configuration': {
			'landscape': {
				'type': HP_TYPE._group, 
				'data': {
					'speed': { 
						'type': HP_TYPE._real, 
						'default': -0.15, 
						'range': [-2,2],
						'range_auto': true,
						'getter': 'speed',
						'setter': 'speed' },
					'rgb_speed': { 
						'type': HP_TYPE._real, 
						'default': 0.012, 
						'range': [-2,2], 
						'range_auto': true,
						'getter': 'rgb_speed', 
						'setter': 'rgb_speed' },
					'height_speed': { 
						'type': HP_TYPE._real, 
						'default': 0.232, 
						'range': [-2,2], 
						'range_auto': false,
						'getter': 'height_speed', 
						'setter': 'height_speed' },
					'height_max 0': { 
						'type': HP_TYPE._real, 
						'default': 0.4, 
						'range': [-1,1], 
						'range_auto': true,
						'getter': 'height_max0', 
						'setter': 'height_max0' },
					'height_max 1': { 
						'type': HP_TYPE._real, 
						'default': 0.2, 
						'range': [-1,1], 
						'range_auto': true,
						'getter': 'height_max1', 
						'setter': 'height_max1' }
				}
			},
			'landscape shader': { 
				'type': HP_TYPE._shader,
				'getter': 'shader', 
				'setter': 'shader' }
		}
	},
	{
		'ID': -1, # will be set by script
		'level': HP_LEVEL._debug,
		'name': 'gradiator',
		'scene': 'gradient_background/scene.tscn',
		'thumb': 'gradient_background/screenshot.jpg',
		'type': 'gradient',
		'date': '2020.04.03',
		'authors': 'frankiezafe',
		'interactive': true,
		'description': 'slow gradient variations disturbed by rotating objects',
		'configuration': {
			'background': {
				'type': HP_TYPE._group, 
				'data': {
					'noise amount': { 
						'type': HP_TYPE._real, 
						'range': [0,2],
						'range_auto': true,
						'getter': 'noise_amount',
						'setter': 'noise_amount' }
				}
			},
			'gradient shader': { 
				'type': HP_TYPE._shader,
				'getter': 'gradient_shader', 
				'setter': 'gradient_shader' }
		}
	}
]

var screen_active_id = null
var screen_active_scene = null

# configuration validation and connections

func connect_param( param ):
	if param.type == glob.HP_TYPE._group:
		for k in param.data:
			connect_param( param.data[k] )
		return
	param.get = null
	param.set = null
	if 'getter' in param and param.getter != null:
		param.get = funcref( glob.screen_active_scene, param.getter )
	if 'setter' in param and  param.setter != null:
		param.set = funcref( glob.screen_active_scene, param.setter )
	if param.get != null and not 'default' in param:
		param.default = param.get.call_func()

func connect_screen():
	if screen_active_scene == null:
		return
	var conf = screen_list[ screen_active_id ].configuration
	if conf == null:
		return
	for k in conf:
		connect_param( conf[k] )

func validate_configuration( field ):
	
	if not 'type' in field:
		return null
	
	match (field.type):
		HP_TYPE._label:
			return field
		HP_TYPE._group:
			if not 'data' in field or not field.data is Dictionary:
				print( "glob.validate_configuration :: Invalid configuration: ", field )
				return null
			# generation of an empty group
			var output = {
				'type': HP_TYPE._group,
				'data': {}
			}
			for k in field.data:
				var valid = validate_configuration( field.data[k] )
				if valid != null:
					output.data[k] = valid
			return output
		HP_TYPE._bool:
			if not ( 'getter' in field or 'setter' in field ):
				print( "glob.validate_configuration :: Missing getter/setter: ", field )
				return null
			return field
		HP_TYPE._int:
			if not ( 'getter' in field or 'setter' in field ):
				print( "glob.validate_configuration :: Missing getter/setter: ", field )
				return null
			if not 'range' in field:
				field.range = [0,1]
			if not 'range_auto' in field:
				field.range_auto = true
			return field
		HP_TYPE._real:
			if not ( 'getter' in field or 'setter' in field ):
				print( "glob.validate_configuration :: Missing getter/setter: ", field )
				return null
			if not 'range' in field:
				field.range = [0,1]
			if not 'range_auto' in field:
				field.range_auto = true
			return field
		HP_TYPE._vec2:
			if not ( 'getter' in field or 'setter' in field ):
				print( "glob.validate_configuration :: Missing getter/setter: ", field )
				return null
			if not 'range' in field:
				field.range = [0,1]
			if not 'range_auto' in field:
				field.range_auto = true
			return field
		HP_TYPE._vec3:
			if not ( 'getter' in field or 'setter' in field ):
				print( "glob.validate_configuration :: Missing getter/setter: ", field )
				return null
			if not 'range' in field:
				field.range = [0,1]
			if not 'range_auto' in field:
				field.range_auto = true
			return field
		HP_TYPE._shader:
			if not ( 'getter' in field or 'setter' in field ):
				print( "glob.validate_configuration :: Missing getter/setter: ", field )
				return null
			return field
		HP_TYPE._func:
			if not ( 'setter' in field ):
				print( "glob.validate_configuration :: Missing setter: ", field )
				return null
			return field
		HP_TYPE._list:
			if not ( 'getter' in field or 'setter' in field ):
				print( "glob.validate_configuration :: Missing getter/setter: ", field )
				return null
			if not 'columns' in field:
				field.columns = 1
			return field
		_:
			print( "glob.validate_configuration :: unsupported type! ", field )
			return null

func _ready():
	
	# screens validation
	var validated_list = []
	for sc in screen_list:
		# adapting path
		var cp = sc.duplicate( true )
		if not cp.scene.begins_with( 'res://' ):
			cp.scene = 'res://' + cp.scene
		if not cp.thumb.begins_with( 'res://' ):
			cp.thumb = 'res://' + cp.thumb
		# verification of resources
		if not cp.scene.ends_with( ".tscn" ) or not ResourceLoader.exists( cp.scene ):
			print( "scene path is wrong, invalid scene! ", cp )
			continue
		if not ResourceLoader.exists( cp.thumb ):
			print( "thumb path is wrong, invalid scene! ", cp )
			continue
# warning-ignore:return_value_discarded
		load( cp.thumb )
		cp.ID = len(validated_list)
		# checking if configration is there
		if not 'configuration' in cp:
			cp.configuration = null
		else:
			# validation of configuration
			var valid_conf = {}
			for k in cp.configuration:
				var valid = validate_configuration( cp.configuration[k] )
				if valid != null:
					valid_conf[k] = valid
			cp.configuration = valid_conf
		validated_list.append( cp )
	# overwriting screen_list
	screen_list = validated_list

func filter_screens( lvl ):
	if lvl == HP_LEVEL._undefined:
		return
	var validated_list = []
	for sc in screen_list:
		if sc.level >= lvl:
			validated_list.append( sc )
	screen_list = validated_list

########################
# UI templates & funcs #
########################

onready var button_focus_style = load( "res://ui/themes/5020_button_focus.stylebox" )
onready var button_font_bold = load( "res://ui/themes/5020_font_bold.tres" )
onready var url_normal_stylebox = load( "res://ui/themes/url_normal.stylebox" )
onready var url_hover_stylebox = load( "res://ui/themes/url_hover.stylebox" )

onready var all_templates = load( "res://ui/templates/templates.tscn" ).instance()
onready var tmpl_button = all_templates.get_node( "btns_frame/lyt/btn" )
onready var tmpl_label = all_templates.get_node( "btns_frame/lyt/lbl" )
onready var tmpl_url = all_templates.get_node( "btns_frame/lyt/lbl_url" )
onready var tmpl_notification = all_templates.get_node( "notification" )
onready var fields = {
	HP_TYPE._real : all_templates.get_node( "params_frame/lyt/real" ),
	HP_TYPE._int : all_templates.get_node( "params_frame/lyt/int" ),
	HP_TYPE._bool : all_templates.get_node( "params_frame/lyt/bool" ),
	HP_TYPE._func : all_templates.get_node( "params_frame/lyt/func" ),
	HP_TYPE._shader : all_templates.get_node( "params_frame/lyt/shader" ),
	HP_TYPE._vec3 : all_templates.get_node( "params_frame/lyt/vec3" ),
	HP_TYPE._list : all_templates.get_node( "params_frame/lyt/list" )
}

func button_highlight( btn ):
	btn.set( "custom_styles/normal", button_focus_style )

func button_focus( btn ):
	btn.set( "custom_styles/normal", button_focus_style )
	btn.set( "custom_fonts/font", button_font_bold )

func button_unfocus( btn ):
	btn.set( "custom_styles/normal", null )
	btn.set( "custom_fonts/font", null )

func int_speed( v ):
	if not v is Vector2:
		return null
	if v.x > 0 and v.x < 1:
		v.x = 1
	elif v.x < 0 and v.x > -1:
		v.x = -1
	if v.y > 0 and v.y < 1:
		v.y = 1
	elif v.y < 0 and v.y > -1:
		v.y = -1
	return Vector2( int( v.x ), int( v.y ) )

func load_credits( container ):
	for c in credits:
		if not 'text' in c:
			continue
		var txt
		if 'url' in c:
			txt = tmpl_url.duplicate()
			txt.url = c.url
		else:
			txt = tmpl_label.duplicate()
		txt.text = c.text
		if 'type' in c or 'license' in c:
			txt.text += " ["
		if 'type' in c:
			txt.text += c.type
			if 'license' in c:
				txt.text += ', '
		if 'license' in c:
			txt.text += c.license
		if 'type' in c or 'license' in c:
			txt.text += "]"
		container.add_child( txt )
		txt.visible = true

###########
# SIGNALS #
###########

# custom signals system to interact with UI

# warning-ignore:unused_signal
signal HP_menu_open
# warning-ignore:unused_signal
signal HP_menu_close
# warning-ignore:unused_signal
signal HP_notification

func emit( sig, data = null ):
	if data == null:
		emit_signal( sig )
	else:
		emit_signal( sig, data )

var DEFAULT_NOTICATION_TTL = 3
var NOTIFICATION_ID = 0
func new_notification():
	var out = {
		'name': "",
		'text': "",
		'ttl': DEFAULT_NOTICATION_TTL,
		'ID': NOTIFICATION_ID
	}
	NOTIFICATION_ID += 1
	return out
