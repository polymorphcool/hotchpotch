extends Spatial

onready var glob = get_node("/root/glob")

export (glob.HP_LEVEL) var release_level = glob.HP_LEVEL._undefined

var rloader = null
var current_notification = null

func _ready():
	glob.filter_screens( release_level )
	glob.screen_active_scene = null
# warning-ignore:return_value_discarded
	$ui.connect( "screen_selected", self, "load_screen" )
	$ui.connect( "open_splash", self, "open_splash" )
	$ui.load_screens()
	$splash.connect("splash_close", self, "open_menu")

func open_menu():
	$ui.visible = true
	$notifications.visible = true

func open_splash():
	$ui.visible = false
	$notifications.visible = false
	$splash.open()

func load_screen( id ):
	# setting scene in global space
	glob.screen_active_id = id
	var screen_data = glob.screen_list[ id ]
	if glob.screen_active_scene != null:
		remove_child( glob.screen_active_scene )
		glob.screen_active_scene.queue_free()
		glob.screen_active_scene = null
	current_notification = glob.new_notification()
	current_notification.name = screen_data.name
	current_notification.text = screen_data.name + " :: loading"
	glob.emit( "HP_notification", current_notification )
	rloader = ResourceLoader.load_interactive( screen_data.scene )
	# starting _process
	set_process(true)

# warning-ignore:unused_argument
func _input(event):
	pass

# warning-ignore:unused_argument
func _process(delta):
	
	if rloader == null:
		return
	
	var err = rloader.poll()
	if err == ERR_FILE_EOF: # load finished
		
		current_notification.text = current_notification.name+" :: "+str(rloader.get_stage_count())+'/'+str(rloader.get_stage_count())+" loaded"
		glob.emit( "HP_notification", current_notification )
		glob.emit( "HP_notification", current_notification.name + " :: loaded" )
		
		glob.screen_active_scene = rloader.get_resource().instance()
		add_child( glob.screen_active_scene )
		# connection of functions
		glob.connect_screen()
		# loading configuration in edit panel
		$ui.load_configuration()
		rloader = null
		# stopping _process
		set_process(false)
	elif err == OK:
		current_notification.text = current_notification.name+" :: "+str(rloader.get_stage())+'/'+str(rloader.get_stage_count())+" loaded"
		glob.emit( "HP_notification", current_notification )
	else:
		current_notification.text = current_notification.name + " :: failed to load"
		glob.emit( "HP_notification", current_notification )
		rloader = null
		
