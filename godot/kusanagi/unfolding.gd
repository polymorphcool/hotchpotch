tool

extends Spatial

export (bool) var _generate setget generate
export (float) var _norm_offset = 0 setget norm_offset

func norm_offset( f ):
	_norm_offset = f
	var i = 0
	for c in $generated.get_children():
		c.scale = Vector3(1,1,1) + Vector3(1,1,1) * (_norm_offset * i)
		i += 1

func generate( b ):
	
	if b:
		
		$generated.rotation = Vector3()
		while $generated.get_child_count() > 0:
			$generated.remove_child( $generated.get_child(0) )
		var mat0 = $tmpl.material_override
		var mat1 = $tmpl.material_override.next_pass
		
		var parts = []
		for i in range(0,11):
			var tpath = "res://kusanagi/textures/part"
			if i < 10:
				tpath += "0"
			tpath += str(i) + ".png"
			parts.append({ 'texture': tpath, 'invert': false })
			if i < 7:
				parts.append({ 'texture': tpath, 'invert': true })
		
		for t in parts:
			var tex = load( t.texture )
			var inst = $tmpl.duplicate()
			inst.visible = true
			inst.material_override = mat0.duplicate()
			inst.material_override.next_pass = mat1.duplicate()
			var col = Color(0,1,0,1)
			col.h = $generated.get_child_count() * 1.0 / len( parts )
			inst.material_override.set_shader_param( "albedo", col )
			col.s = 0.8;
			inst.material_override.next_pass.set_shader_param( "albedo", col )
			inst.material_override.set_shader_param( "texture_albedo", tex )
			inst.material_override.next_pass.set_shader_param( "texture_albedo", tex )
			if t.invert:
				var uv = Vector2(-1,1)
				inst.material_override.set_shader_param( "uv1_scale", uv )
				inst.material_override.next_pass.set_shader_param( "uv1_scale", uv )
			$generated.add_child( inst )
			inst.set_owner(get_tree().get_edited_scene_root())
		norm_offset( _norm_offset )

func _ready():
	pass # Replace with function body.

func _process(delta):
	#$generated.rotate_y( delta * 0.2 )
	var i = 0
	for c in $generated.get_children():
		c.rotate_y( delta * i )
		c.rotate_x( delta * 0.5 * i )
		i += 0.01
